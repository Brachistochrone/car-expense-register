# ![CarExpenseRegister App using Java and Spring](app-logo.png)

# Description

This application's main purpose is to store information about the car's maintenance history.
It allows users to create the car's profile and then add information about its refuels and repairs whenever they take place.

It also makes it possible to obtain the following information:
- a total volume of gas consumed by a car during a specified period of time;
- an amount of money spent on spare parts for a car during a specified year;
- a total amount of money spent on spare parts for a car;
- a full list of spare parts replaced in a car during its operational history;
- a complete list of cars and their detailed information.

All the information about spent money may be stored and acquired in different currencies (UAH, USD, EUR, RUB) according to current exchange rates.

# Used technologies

* Spring Core
* Spring Boot Web 
* Spring Boot Security
* Spring Boot Data JPA
* Spring Boot Test
* JWT
* MapStruct
* Lombok
* Flyway
* Apache Commons Validator
* PostgreSQL
* Maven

# How it works

- The application follows MVC structure and has three distinguished layers (Web, Service, and Dao).
- Each layer has its own POJO objects to represent data, which are converted into one another by mappers, based on MapStruct.
- Every user input is validated by validation layer using Apache Commons Validator.
- Up-to-date currency exchange rates are obtained from privatbank.ua API.
- Both Web and Service layers are completely covered with unit and integration tests.

The code is organized as follows:

1. `web` is the web layer implemented by Spring Boot Web. It consists of REST controllers, DTOs, etc.
2. `service` is the service layer including business logic classes, validators, and mappers. All of them are Spring Beans.
3. `dao` is the persistence layer for working with a database. It consists of entities, query results and repositories, and utilizes Spring Boot Data JPA.
4. `model`  contains all the POJO classes used to represent data throughout application.
5. `exceptions` contains all the custom exceptions thrown in application and handled by Exception Handler.
6. `enums` includes ENUMs used in application.
7. `configuration` holds configuration classes.

# Security

Application security is based on Spring Boot Security. 
It supports user authentication, authorization, and uses JWT to generate tokens.
Furthermore, the application maintains two roles (USER and ADMIN) and enables global method security.

The secret token key is stored in `application.properties`.

# Database

It uses a PostgreSQL database. However, it can be changed easily in the `application.properties` for any other database.

# Getting started

In order to be able to use this application one needs to have Java 11 and PostgreSQL installed.
Also, it might be necessary to change DB credentials in `application.properties`.

All endpoints are located on `http://localhost:8081/car-expense-register-app/v1`.  

# API Specification

### Registration:
`POST /users`

Creates new user. No authentication required.

Request body example:
```JSON
{
  "email": "example@gmail.com",
  "password": "qwerty"
}
```
Response body example:
```JSON
{
  "id": 1,
  "email": "example@gmail.com"
}
```
### Authentication:
`POST /login`

Authenticates user, returns token. No authentication required.

Request body example:
```JSON
{
  "email": "example@gmail.com",
  "password": "qwerty"
}
```
Response header example:

`Key: Authorization; Value: soMeENorMouSTokEn`

### Adding new expense category:
`POST /expense_categories`

Creates new expense category. Requires authentication and role ADMIN.

Request body example:
```JSON
{
  "expenseType": "PARKING"
}
```
Response body example:
```JSON
{
  "id": 5,
  "expenseType": "PARKING"
}
```

### Getting list of available expense categories:
`GET /expense_categories`

Returns full list of available expense categories. Requires authentication.

No request body or parameters required.

Example response body:
```JSON
{
    "expenseCategories": [
        {
            "id": 1,
            "expenseType": "REFUEL"
        },
        {
            "id": 2,
            "expenseType": "REPAIR"
        }
    ]
}
```
### Creating new car profile:
`POST /cars`

Creates new car profile. Requires authentication.

Request body example:
```JSON
{
  "carInfo": {
    "vinCode": "123456",
    "model": "Chevrolet Bel Air",
    "color": "Blue",
    "body": "Sedan",
    "year": "1957"
  }
}
```
Response body example:
```JSON
{
    "id": 5,
    "carInfo": {
        "vinCode": "123456",
        "model": "Chevrolet Bel Air",
        "color": "Blue",
        "body": "Sedan",
        "year": "1957"
    }
}
```

### Getting list of cars:
`GET /cars`

Returns the list of available cars divided into pages. Requires authentication.

Query parameters example:

`?page=0&size=2&sort=carInfo.model`

where `page` selects a number of a page, `size` specifies a number of cars per page, and `sort` sets sorting options.

Response body example:
```JSON
{
    "content": [
        {
            "id": 2,
            "carInfo": {
                "vinCode": "987654321",
                "model": "Audi S3",
                "color": "Red",
                "body": "Hatchback",
                "year": "2014"
            }
        },
        {
            "id": 5,
            "carInfo": {
                "vinCode": "123456",
                "model": "Chevrolet Bel Air",
                "color": "Blue",
                "body": "Sedan",
                "year": "1957"
            }
        }
    ],
    "pageable": {
        "sort": {
            "sorted": true,
            "unsorted": false,
            "empty": false
        },
        "offset": 0,
        "pageSize": 2,
        "pageNumber": 0,
        "unpaged": false,
        "paged": true
    },
    "totalElements": 5,
    "last": false,
    "totalPages": 3,
    "size": 2,
    "number": 0,
    "sort": {
        "sorted": true,
        "unsorted": false,
        "empty": false
    },
    "numberOfElements": 2,
    "first": true,
    "empty": false
}
```
### Adding new refuel to a car:
`POST /refuels`

Adds new refuel to the car's refuel history. Requires authentication.

Request body example:
```JSON
{
  "volume":50.0,
  "refuelInfo":{
      "expenseCategory":{
          "expenseType":"REFUEL"
      },
      "date":"2020-09-25",
      "sum":1500,
      "currency":"UAH",
      "companyName":"OKKO",
      "companyAddress":"Some address",
      "description":"Gasoline type A-95"
  },
  "carId":5
}
```
Response body example:
```JSON
{
    "volume": 50.0,
    "refuelInfo": {
        "expenseCategory": {
            "id": 1,
            "expenseType": "REFUEL"
        },
        "date": "2020-09-25T00:00:00.000+0000",
        "sum": 1500.0,
        "currency": "UAH",
        "companyName": "OKKO",
        "companyAddress": "Some address",
        "description": "Gasoline type A-95"
    },
    "carId": 5
}
```

### Getting volume of gas consumed by a car during a specified period:
`GET /refuels`

Returns volume of gas consumed by a car during a specified period. Requires authentication.

Query parameters example:

`?car_id=1&start_date=2020-05-01&end_date=2020-07-31`

where `car_id` specifies a car, `start_date` and `end_date` set a desired period.

Response body example:
```JSON
{
    "volume": 610.0
}
```
### Adding new repair to a car:
`POST /replaced_parts`

Adds new repair to the car's repair history. Requires authentication.

Request body example:
```JSON
{
  "serialNumber":"HENGST FILTER E1002L",
  "name":"Air filter",
  "replacedPartInfo":{
      "expenseCategory":{
          "expenseType":"REPAIR"
      },
      "date":"2020-09-02",
      "sum":625.93,
      "currency":"UAH",
      "companyName":"Some company",
      "companyAddress":"Some company address",
      "description":"Air filter replacement"
  },
  "carId":5
}
```
Response body example:
```JSON
{
    "serialNumber": "HENGST FILTER E1002L",
    "name": "Air filter",
    "replacedPartInfo": {
        "expenseCategory": {
            "id": 2,
            "expenseType": "REPAIR"
        },
        "date": "2020-09-02T00:00:00.000+0000",
        "sum": 625.93,
        "currency": "UAH",
        "companyName": "Some company",
        "companyAddress": "Some company address",
        "description": "Air filter replacement"
    },
    "carId": 5
}
```

### Getting an amount of money spent on spare parts during a specified year:
`GET /replaced_parts/spent_money`

Returns amount of money spent on spare parts for a car during a specified year. Requires authentication.

Query parameters example:

`?car_id=1&year=2020&currency=USD`

where `car_id` specifies a car, `year` sets a desired year, `currency` selects a currency of response.

Response body example:
```JSON
{
    "spentMoney": 177.63,
    "currency": "USD"
}
```
### Getting total amount of money spent on spare parts:
`GET /replaced_parts/spent_money`

Returns total amount of money spent on spare parts for a car during its operation. Requires authentication.

Query parameters example:

`car_id=1&year=&currency=USD`

where `car_id` specifies a car, `currency` selects a currency of response, empty `year` means the whole period.

Response body example:
```JSON
{
    "spentMoney": 177.63,
    "currency": "USD"
}
```
### Getting a list of replaced parts:
`GET /replaced_parts/{car_id}`

where `car_id` specifies a car.

Query parameters example:

`?page=0&size=2&sort=`

where `page` selects a number of a page, `size` specifies a number of cars per page, and `sort` sets sorting options.

Returns a list of car's replaced parts divided into pages. Requires authentication.

Response body example:
```JSON
{
    "content": [
        {
            "serialNumber": "MEYLE 35143220002",
            "name": "Oil filter",
            "replacedPartInfo": {
                "expenseCategory": {
                    "id": 2,
                    "expenseType": "REPAIR"
                },
                "date": "2020-05-11T21:00:00.000+0000",
                "sum": 276.89,
                "currency": "UAH",
                "companyName": "Some company",
                "companyAddress": "Some company address",
                "description": "Oil filter replacement"
            }
        },
        {
            "serialNumber": "ELSTOCK 831615",
            "name": "Stopping support",
            "replacedPartInfo": {
                "expenseCategory": {
                    "id": 2,
                    "expenseType": "REPAIR"
                },
                "date": "2020-04-22T21:00:00.000+0000",
                "sum": 3954.32,
                "currency": "UAH",
                "companyName": "Some company",
                "companyAddress": "Some company address",
                "description": "Stopping support replacement"
            }
        }
    ],
    "pageable": {
        "sort": {
            "sorted": false,
            "unsorted": true,
            "empty": true
        },
        "offset": 0,
        "pageSize": 2,
        "pageNumber": 0,
        "unpaged": false,
        "paged": true
    },
    "totalElements": 3,
    "last": false,
    "totalPages": 2,
    "size": 2,
    "number": 0,
    "sort": {
        "sorted": false,
        "unsorted": true,
        "empty": true
    },
    "numberOfElements": 2,
    "first": true,
    "empty": false
}
```