package com.likhomanov;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarExpenseRegisterApp {

    public static void main(String[] args) {
        SpringApplication.run(CarExpenseRegisterApp.class, args);
    }
}
