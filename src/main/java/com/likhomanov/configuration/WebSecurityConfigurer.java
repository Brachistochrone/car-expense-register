package com.likhomanov.configuration;

import com.likhomanov.services.UserService;
import com.likhomanov.services.mappers.UserMapper;
import com.likhomanov.services.mappers.UserRoleMapper;
import com.likhomanov.services.validators.AuthenticatedUserValidator;
import com.likhomanov.services.validators.LoginUserValidator;
import com.likhomanov.web.filters.JWTAuthenticationFilter;
import com.likhomanov.web.filters.JWTAuthorizationFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import static org.springframework.http.HttpMethod.POST;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
@PropertySource("classpath:/application.properties")
@ComponentScan({"com.likhomanov.services", "com.likhomanov.dao"})
public class WebSecurityConfigurer extends WebSecurityConfigurerAdapter {

    private final UserService userService;
    private final LoginUserValidator loginUserValidator;
    private final AuthenticatedUserValidator authenticatedUserValidator;
    private final UserMapper userMapper;
    private final UserRoleMapper roleMapper;
    private final String secret;

    public WebSecurityConfigurer(@Lazy UserService userService,
                                 LoginUserValidator loginUserValidator,
                                 AuthenticatedUserValidator authenticatedUserValidator,
                                 @Lazy UserMapper userMapper,
                                 UserRoleMapper roleMapper,
                                 @Value("&{token.secret}") String secret) {
        this.userService = userService;
        this.loginUserValidator = loginUserValidator;
        this.authenticatedUserValidator = authenticatedUserValidator;
        this.userMapper = userMapper;
        this.roleMapper = roleMapper;
        this.secret = secret;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors()
            .and()
            .csrf()
            .disable()
            .authorizeRequests()
            .antMatchers(POST, "/users")
            .permitAll()
            .anyRequest()
            .authenticated()
            .and()
            .addFilter(new JWTAuthenticationFilter(authenticationManager(),
                                                   loginUserValidator,
                                                   authenticatedUserValidator,
                                                   userMapper,
                                                   secret))
            .addFilter(new JWTAuthorizationFilter(authenticationManager(),
                                                  roleMapper,
                                                  authenticatedUserValidator,
                                                  secret))
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(bCryptPasswordEncoder());
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        UrlBasedCorsConfigurationSource corsConfiguration = new UrlBasedCorsConfigurationSource();
        corsConfiguration.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        return corsConfiguration;
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
