package com.likhomanov.dao.entities;

import com.likhomanov.model.Car;
import com.likhomanov.model.CarInfo;
import com.likhomanov.model.Refuel;
import com.likhomanov.model.ReplacedPart;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "cars")
@Setter
public class CarEntity implements Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne(  cascade = CascadeType.ALL,
                orphanRemoval = true,
                fetch = FetchType.LAZY)
    @JoinColumn(name = "car_info_id")
    private CarInfoEntity carInfo;
    @OneToMany( mappedBy = "car",
                cascade = CascadeType.ALL,
                orphanRemoval = true,
                fetch = FetchType.LAZY)
    private Set<ReplacedPartEntity> replacedParts = new HashSet<>();
    @OneToMany( mappedBy = "car",
                cascade = CascadeType.ALL,
                orphanRemoval = true,
                fetch = FetchType.LAZY)
    private Set<RefuelEntity> refuels = new HashSet<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public CarInfo getCarInfo() {
        return carInfo;
    }

    @Override
    public Set<? extends ReplacedPart> getReplacedParts() {
        return replacedParts;
    }

    @Override
    public Set<? extends Refuel> getRefuels() {
        return refuels;
    }

    public void setReplacedParts(Set<ReplacedPartEntity> replacedParts) {
        replacedParts.forEach(this::addReplacedPart);
    }

    public void setRefuels(Set<RefuelEntity> refuels) {
        refuels.forEach(this::addRefuel);
    }

    public void addReplacedPart(ReplacedPartEntity replacedPart) {
        replacedPart.setCar(this);
        replacedParts.add(replacedPart);
    }

    public void removeReplacedPart(ReplacedPartEntity replacedPart) {
        replacedParts.remove(replacedPart);
        replacedPart.setCar(null);
    }

    public void addRefuel(RefuelEntity refuel) {
        refuel.setCar(this);
        refuels.add(refuel);
    }

    public void removeRefuel(RefuelEntity refuel) {
        refuels.remove(refuel);
        refuel.setCar(null);
    }
}
