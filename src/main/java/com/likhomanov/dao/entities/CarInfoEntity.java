package com.likhomanov.dao.entities;

import com.likhomanov.model.CarInfo;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;

@Entity
@Table(name = "car_info")
@Setter
public class CarInfoEntity implements CarInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NaturalId
    @Column(name = "vin_code")
    private String vinCode;
    @Column(name = "model")
    private String model;
    @Column(name = "color")
    private String color;
    @Column(name = "body")
    private String body;
    @Column(name = "year")
    private String year;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getVinCode() {
        return vinCode;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public String getBody() {
        return body;
    }

    @Override
    public String getYear() {
        return year;
    }
}
