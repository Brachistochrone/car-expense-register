package com.likhomanov.dao.entities;

import com.likhomanov.model.ExpenseCategory;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "expense_categories")
@Setter
public class ExpenseCategoryEntity implements ExpenseCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "expense_type")
    private String expenseType;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getExpenseType() {
        return expenseType;
    }
}
