package com.likhomanov.dao.entities;

import com.likhomanov.enums.Currency;
import com.likhomanov.model.ExpenseCategory;
import com.likhomanov.model.ExpenseInfo;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "expense_info")
@Setter
public class ExpenseInfoEntity implements ExpenseInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne( cascade = { CascadeType.PERSIST,
                            CascadeType.MERGE },
                fetch = FetchType.EAGER)
    @JoinColumn(name = "expense_category_id")
    private ExpenseCategoryEntity expenseCategory;
    @Column(name = "date")
    private Date date;
    @Column(name = "sum")
    private Double sum;
    @Enumerated(value = EnumType.STRING)
    @Column(name = "currency")
    private Currency currency;
    @Column(name = "company_name")
    private String companyName;
    @Column(name = "company_address")
    private String companyAddress;
    @Column(name = "description")
    private String description;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public ExpenseCategory getExpenseCategory() {
        return expenseCategory;
    }

    @Override
    public Date getDate() {
        return date;
    }

    @Override
    public Double getSum() {
        return sum;
    }

    @Override
    public Currency getCurrency() {
        return currency;
    }

    @Override
    public String getCompanyName() {
        return companyName;
    }

    @Override
    public String getCompanyAddress() {
        return companyAddress;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
