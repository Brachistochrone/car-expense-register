package com.likhomanov.dao.entities;

import com.likhomanov.model.Car;
import com.likhomanov.model.ExpenseInfo;
import com.likhomanov.model.Refuel;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "refuels")
@Setter
public class RefuelEntity implements Refuel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "volume")
    private Double volume;
    @OneToOne(  cascade = CascadeType.ALL,
                orphanRemoval = true,
                fetch = FetchType.LAZY)
    @JoinColumn(name = "expense_info_id")
    private ExpenseInfoEntity expenseInfo;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "car_id", referencedColumnName = "id")
    private CarEntity car;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Double getVolume() {
        return volume;
    }

    @Override
    public ExpenseInfo getExpenseInfo() {
        return expenseInfo;
    }

    @Override
    public Car getCar() {
        return car;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RefuelEntity that = (RefuelEntity) o;
        return id != null && id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
