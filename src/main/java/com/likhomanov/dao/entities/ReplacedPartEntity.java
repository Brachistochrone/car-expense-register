package com.likhomanov.dao.entities;

import com.likhomanov.model.Car;
import com.likhomanov.model.ExpenseInfo;
import com.likhomanov.model.ReplacedPart;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "replaced_parts")
@Setter
public class ReplacedPartEntity implements ReplacedPart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NaturalId
    @Column(name = "serial_number")
    private String serialNumber;
    @Column(name = "name")
    private String name;
    @OneToOne(  cascade = CascadeType.ALL,
                orphanRemoval = true,
                fetch = FetchType.LAZY)
    @JoinColumn(name = "expense_info_id")
    private ExpenseInfoEntity expenseInfo;
    @ManyToOne(fetch = FetchType.LAZY)
    private CarEntity car;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getSerialNumber() {
        return serialNumber;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public ExpenseInfo getExpenseInfo() {
        return expenseInfo;
    }

    @Override
    public Car getCar() {
        return car;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReplacedPartEntity that = (ReplacedPartEntity) o;
        return getSerialNumber().equals(that.getSerialNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSerialNumber());
    }
}
