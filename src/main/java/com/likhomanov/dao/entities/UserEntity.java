package com.likhomanov.dao.entities;

import com.likhomanov.model.User;
import com.likhomanov.model.UserRole;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "users")
@Setter
public class UserEntity implements User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NaturalId
    @Column(name = "email")
    private String email;
    @Column(name = "password")
    private String password;
    @ManyToMany(fetch = FetchType.EAGER,
                cascade = { CascadeType.PERSIST,
                            CascadeType.MERGE })
    @JoinTable( name = "users_roles_mapping",
                joinColumns = @JoinColumn(name = "user_id"),
                inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<UserRoleEntity> roles = new HashSet<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public Set<? extends UserRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<UserRoleEntity> roles) {
        roles.forEach(this::addRole);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity that = (UserEntity) o;
        return getEmail().equals(that.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEmail());
    }

    public void addRole(UserRoleEntity userRole) {
        userRole.addUser(this);
        roles.add(userRole);
    }

    public void removeRole(UserRoleEntity userRole) {
        userRole.removeUser(this);
        roles.remove(userRole);
    }
}
