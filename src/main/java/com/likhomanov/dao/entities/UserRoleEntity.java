package com.likhomanov.dao.entities;

import com.likhomanov.enums.Role;
import com.likhomanov.model.User;
import com.likhomanov.model.UserRole;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "user_roles")
@Setter
public class UserRoleEntity implements UserRole {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NaturalId
    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private Role role;
    @ManyToMany(mappedBy = "roles",
                fetch = FetchType.LAZY)
    private Set<UserEntity> users = new HashSet<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Role getRole() {
        return role;
    }

    @Override
    public Set<? extends User> getUsers() {
        return users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserRoleEntity that = (UserRoleEntity) o;
        return role.name().equals(that.role.name());
    }

    @Override
    public int hashCode() {
        return Objects.hash(role.name());
    }

    public void addUser(UserEntity user) {
        users.add(user);
    }

    public void removeUser(UserEntity user) {
        users.remove(user);
    }
}
