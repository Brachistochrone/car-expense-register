package com.likhomanov.dao.queryresults;

import com.likhomanov.enums.Currency;

public interface MoneyByCurrencyResult {

    Currency getCurrency();

    Double getDough();
}
