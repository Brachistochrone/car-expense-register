package com.likhomanov.dao.repositories;

import com.likhomanov.dao.entities.CarEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarDao extends JpaRepository<CarEntity, Long>, ExpenseToCarAttacher {

}
