package com.likhomanov.dao.repositories;

import com.likhomanov.dao.entities.CarInfoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CarInfoDao extends JpaRepository<CarInfoEntity, Long> {

    @Query(nativeQuery = true,
           value = "SELECT EXISTS (SELECT * FROM car_info WHERE vin_code = :vinCode)")
    boolean existsByVinCode(@Param("vinCode") String vinCode);
}
