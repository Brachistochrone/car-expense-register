package com.likhomanov.dao.repositories;

import com.likhomanov.dao.entities.ExpenseCategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ExpenseCategoryDao extends JpaRepository<ExpenseCategoryEntity, Long> {

    @Query("select e.id from ExpenseCategoryEntity e where e.expenseType = :expenseType")
    Optional<Long> findIdByExpenseType(@Param("expenseType") String expenseType);

    @Query("select e from ExpenseCategoryEntity e where e.expenseType = :expenseType")
    Optional<ExpenseCategoryEntity> findByExpenseType(@Param("expenseType") String expenseType);

    @Query(nativeQuery = true,
           value = "SELECT EXISTS (SELECT * FROM expense_categories WHERE expense_type = :expenseType)")
    boolean existsByExpenseType(@Param("expenseType") String expenseType);
}
