package com.likhomanov.dao.repositories;

import com.likhomanov.dao.entities.RefuelEntity;
import com.likhomanov.dao.entities.ReplacedPartEntity;

public interface ExpenseToCarAttacher {

    RefuelEntity attachRefuelToCarAndSave(Long carId, RefuelEntity newRefuel);

    ReplacedPartEntity attachReplacedPartToCarAndSave(Long carId, ReplacedPartEntity newReplacedPart);
}
