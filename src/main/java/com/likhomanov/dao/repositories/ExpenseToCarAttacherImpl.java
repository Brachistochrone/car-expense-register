package com.likhomanov.dao.repositories;

import com.likhomanov.dao.entities.CarEntity;
import com.likhomanov.dao.entities.RefuelEntity;
import com.likhomanov.dao.entities.ReplacedPartEntity;
import com.likhomanov.exceptions.EntityNotFoundException;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Optional;

public class ExpenseToCarAttacherImpl implements ExpenseToCarAttacher {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    @Modifying
    @Override
    public RefuelEntity attachRefuelToCarAndSave(Long carId, RefuelEntity newRefuel) {
        CarEntity carEntity = findCarById(carId);
        carEntity.addRefuel(newRefuel);
        entityManager.merge(carEntity);
        return newRefuel;
    }

    @Transactional
    @Modifying
    @Override
    public ReplacedPartEntity attachReplacedPartToCarAndSave(Long carId, ReplacedPartEntity newReplacedPart) {
        CarEntity carEntity = findCarById(carId);
        carEntity.addReplacedPart(newReplacedPart);
        entityManager.merge(carEntity);
        return newReplacedPart;
    }

    private CarEntity findCarById(Long carId) {
        return Optional.ofNullable(entityManager.find(CarEntity.class, carId))
                       .orElseThrow(() -> new EntityNotFoundException("Car with id " +
                                                                      carId +
                                                                      " does not exist"));
    }
}
