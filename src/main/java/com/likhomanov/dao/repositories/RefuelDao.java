package com.likhomanov.dao.repositories;

import com.likhomanov.dao.entities.RefuelEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Optional;

@Repository
public interface RefuelDao extends JpaRepository<RefuelEntity, Long> {

    @SuppressWarnings("SpringDataRepositoryMethodReturnTypeInspection")
    @Query( "select sum(r.volume) from RefuelEntity r " +
            "where r.car.id = :carId " +
            "and r.expenseInfo.date between :startDate and :endDate")
    Optional<Double> findGuzzledGasForSomePeriodByCarId(@Param("carId") Long carId,
                                                        @Param("startDate") Date startDate,
                                                        @Param("endDate") Date endDate);
}
