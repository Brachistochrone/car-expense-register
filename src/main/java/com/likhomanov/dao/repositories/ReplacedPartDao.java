package com.likhomanov.dao.repositories;

import com.likhomanov.dao.entities.ReplacedPartEntity;
import com.likhomanov.dao.queryresults.MoneyByCurrencyResult;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ReplacedPartDao extends JpaRepository<ReplacedPartEntity, Long> {

    Page<ReplacedPartEntity> findByCarId(Long carId, Pageable pageable);

    @SuppressWarnings("SpringDataRepositoryMethodReturnTypeInspection")
    @Query( "select p.expenseInfo.currency as currency, sum(p.expenseInfo.sum) as dough " +
            "from ReplacedPartEntity p where p.car.id = :carId group by p.expenseInfo.currency")
    List<MoneyByCurrencyResult> findDoughSpentOnSparePartsByCarId(@Param("carId") Long carId);

    @SuppressWarnings("SpringDataRepositoryMethodReturnTypeInspection")
    @Query( "select p.expenseInfo.currency as currency, sum(p.expenseInfo.sum) as dough " +
            "from ReplacedPartEntity p " +
            "where p.car.id = :carId " +
            "and p.expenseInfo.date between :startDate and :endDate " +
            "group by p.expenseInfo.currency")
    List<MoneyByCurrencyResult> findDoughSpentOnSparePartsForSomePeriodByCarId(@Param("carId") Long carId,
                                                                               @Param("startDate") Date startDate,
                                                                               @Param("endDate") Date endDate);

    @Query(nativeQuery = true,
           value = "SELECT EXISTS (SELECT * FROM replaced_parts WHERE serial_number = :serialNumber)")
    boolean existsBySerialNumber(@Param("serialNumber") String serialNumber);
}
