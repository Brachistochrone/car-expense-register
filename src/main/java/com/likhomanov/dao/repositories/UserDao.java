package com.likhomanov.dao.repositories;

import com.likhomanov.dao.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDao extends JpaRepository<UserEntity, Long>, UserWithRoleSaver {

    Optional<UserEntity> findOneByEmail(String email);

    @Query(nativeQuery = true,
           value = "SELECT EXISTS (SELECT * FROM users WHERE email = :email)")
    boolean existsByEmail(@Param("email") String email);
}
