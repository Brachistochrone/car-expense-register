package com.likhomanov.dao.repositories;

import com.likhomanov.dao.entities.UserRoleEntity;
import com.likhomanov.enums.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRoleDao extends JpaRepository<UserRoleEntity, Long> {

    @Query("select r.id from UserRoleEntity r where r.role = :role")
    Optional<Long> findRoleIdByRoleName(@Param("role") Role role);
}
