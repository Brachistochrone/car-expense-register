package com.likhomanov.dao.repositories;

import com.likhomanov.dao.entities.UserEntity;

public interface UserWithRoleSaver {

    UserEntity saveUserWithRole(UserEntity userWithRole);
}
