package com.likhomanov.dao.repositories;

import com.likhomanov.dao.entities.UserEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class UserWithRoleSaverImpl implements UserWithRoleSaver {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    @Modifying
    @Override
    public UserEntity saveUserWithRole(UserEntity userWithRole) {
        return entityManager.merge(userWithRole);
    }
}
