package com.likhomanov.enums;

public enum Currency {

    USD, EUR, UAH, RUB, BTC
}
