package com.likhomanov.exceptions;

public class DateFormerException extends ValidationException {

    public DateFormerException(String message) {
        super(message);
    }

    public DateFormerException(String message, Throwable cause) {
        super(message, cause);
    }
}
