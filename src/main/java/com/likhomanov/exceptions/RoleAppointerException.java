package com.likhomanov.exceptions;

public class RoleAppointerException extends RuntimeException {

    public RoleAppointerException(String message) {
        super(message);
    }

    public RoleAppointerException(String message, Throwable cause) {
        super(message, cause);
    }
}
