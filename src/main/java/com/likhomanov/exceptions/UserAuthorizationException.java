package com.likhomanov.exceptions;

import javax.servlet.ServletException;

public class UserAuthorizationException extends ServletException {

    public UserAuthorizationException(String message) {
        super(message);
    }

    public UserAuthorizationException(String message, Throwable rootCause) {
        super(message, rootCause);
    }
}
