package com.likhomanov.model;

import java.util.Set;

public interface Car {

    Long getId();

    CarInfo getCarInfo();

    Set<? extends ReplacedPart> getReplacedParts();

    Set<? extends Refuel> getRefuels();
}
