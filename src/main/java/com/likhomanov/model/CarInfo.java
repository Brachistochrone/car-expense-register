package com.likhomanov.model;

public interface CarInfo {

    Long getId();

    String getVinCode();

    String getModel();

    String getColor();

    String getBody();

    String getYear();
}
