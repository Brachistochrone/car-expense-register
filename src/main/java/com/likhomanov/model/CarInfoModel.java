package com.likhomanov.model;

import lombok.Setter;

@Setter
public class CarInfoModel implements CarInfo {

    private Long id;
    private String vinCode;
    private String model;
    private String color;
    private String body;
    private String year;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getVinCode() {
        return vinCode;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public String getBody() {
        return body;
    }

    @Override
    public String getYear() {
        return year;
    }
}
