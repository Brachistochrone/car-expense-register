package com.likhomanov.model;

import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Setter
public class CarModel implements Car {

    private Long id;
    private CarInfoModel carInfo;
    private Set<ReplacedPartModel> replacedParts = new HashSet<>();
    private Set<RefuelModel> refuels = new HashSet<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public CarInfo getCarInfo() {
        return carInfo;
    }

    @Override
    public Set<? extends ReplacedPart> getReplacedParts() {
        return replacedParts;
    }

    @Override
    public Set<? extends Refuel> getRefuels() {
        return refuels;
    }

    public void setReplacedParts(Set<ReplacedPartModel> replacedParts) {
        replacedParts.forEach(this::addReplacedPart);
    }

    public void setRefuels(Set<RefuelModel> refuels) {
        refuels.forEach(this::addRefuel);
    }

    public void addReplacedPart(ReplacedPartModel replacedPart) {
        replacedPart.setCar(this);
        replacedParts.add(replacedPart);
    }

    public void removeReplacedPart(ReplacedPartModel replacedPart) {
        replacedParts.remove(replacedPart);
        replacedPart.setCar(null);
    }

    public void addRefuel(RefuelModel refuel) {
        refuel.setCar(this);
        refuels.add(refuel);
    }

    public void removeRefuel(RefuelModel refuel) {
        refuels.remove(refuel);
        refuel.setCar(null);
    }
}
