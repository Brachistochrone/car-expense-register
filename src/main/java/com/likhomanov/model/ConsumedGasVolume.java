package com.likhomanov.model;

public interface ConsumedGasVolume {

    Double getVolume();
}
