package com.likhomanov.model;

import lombok.Setter;

@Setter
public class ConsumedGasVolumeModel implements ConsumedGasVolume {

    private Double volume;

    @Override
    public Double getVolume() {
        return volume;
    }
}
