package com.likhomanov.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.likhomanov.enums.Currency;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CurrencyExchangeRate {

    @JsonProperty("ccy")
    private Currency currency;
    @JsonProperty("base_ccy")
    private Currency baseCurrency;
    @JsonProperty("buy")
    private Double buyRate;
    @JsonProperty("sale")
    private Double saleRate;
}
