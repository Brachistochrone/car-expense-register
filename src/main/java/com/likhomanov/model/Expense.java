package com.likhomanov.model;

public interface Expense {

    ExpenseInfo getExpenseInfo();
}
