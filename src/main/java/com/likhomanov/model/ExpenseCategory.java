package com.likhomanov.model;

public interface ExpenseCategory {

    Long getId();

    String getExpenseType();
}
