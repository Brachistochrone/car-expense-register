package com.likhomanov.model;

import lombok.Setter;

@Setter
public class ExpenseCategoryModel implements ExpenseCategory {

    private Long id;
    private String expenseType;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getExpenseType() {
        return expenseType;
    }
}
