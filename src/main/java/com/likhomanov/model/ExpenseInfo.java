package com.likhomanov.model;

import com.likhomanov.enums.Currency;

import java.util.Date;

public interface ExpenseInfo {

    Long getId();

    ExpenseCategory getExpenseCategory();

    Date getDate();

    Double getSum();

    Currency getCurrency();

    String getCompanyName();

    String getCompanyAddress();

    String getDescription();
}
