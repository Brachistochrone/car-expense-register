package com.likhomanov.model;

import com.likhomanov.enums.Currency;
import lombok.Setter;

import java.util.Date;

@Setter
public class ExpenseInfoModel implements ExpenseInfo {

    private Long id;
    private ExpenseCategoryModel expenseCategory;
    private Date date;
    private Double sum;
    private Currency currency;
    private String companyName;
    private String companyAddress;
    private String description;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public ExpenseCategory getExpenseCategory() {
        return expenseCategory;
    }

    @Override
    public Date getDate() {
        return date;
    }

    @Override
    public Double getSum() {
        return sum;
    }

    @Override
    public Currency getCurrency() {
        return currency;
    }

    @Override
    public String getCompanyName() {
        return companyName;
    }

    @Override
    public String getCompanyAddress() {
        return companyAddress;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
