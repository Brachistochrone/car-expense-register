package com.likhomanov.model;

public interface Refuel extends Expense {

    Long getId();

    Double getVolume();

    Car getCar();
}
