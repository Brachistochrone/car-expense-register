package com.likhomanov.model;

import lombok.Setter;

@Setter
public class RefuelModel implements Refuel {

    private Long id;
    private Double volume;
    private ExpenseInfoModel expenseInfo;
    private CarModel car;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Double getVolume() {
        return volume;
    }

    @Override
    public ExpenseInfo getExpenseInfo() {
        return expenseInfo;
    }

    @Override
    public Car getCar() {
        return car;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RefuelModel that = (RefuelModel) o;
        return id != null && id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
