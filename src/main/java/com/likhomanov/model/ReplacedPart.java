package com.likhomanov.model;

public interface ReplacedPart extends Expense {

    Long getId();

    String getSerialNumber();

    String getName();

    Car getCar();
}
