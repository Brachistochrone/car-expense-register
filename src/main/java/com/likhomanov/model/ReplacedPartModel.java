package com.likhomanov.model;

import lombok.Setter;

import java.util.Objects;

@Setter
public class ReplacedPartModel implements ReplacedPart {

    private Long id;
    private String serialNumber;
    private String name;
    private ExpenseInfoModel expenseInfo;
    private CarModel car;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getSerialNumber() {
        return serialNumber;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public ExpenseInfo getExpenseInfo() {
        return expenseInfo;
    }

    @Override
    public Car getCar() {
        return car;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReplacedPartModel that = (ReplacedPartModel) o;
        return getSerialNumber().equals(that.getSerialNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSerialNumber());
    }
}
