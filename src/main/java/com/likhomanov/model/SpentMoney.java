package com.likhomanov.model;

import com.likhomanov.enums.Currency;

public interface SpentMoney {

    Currency getCurrency();

    Double getSpentMoney();
}
