package com.likhomanov.model;

import com.likhomanov.enums.Currency;
import lombok.Setter;

@Setter
public class SpentMoneyModel implements SpentMoney {

    private Double spentMoney;
    private Currency currency;

    @Override
    public Currency getCurrency() {
        return currency;
    }

    @Override
    public Double getSpentMoney() {
        return spentMoney;
    }
}
