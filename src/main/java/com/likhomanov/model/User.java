package com.likhomanov.model;

import java.util.Set;

public interface User {

    Long getId();

    String getEmail();

    String getPassword();

    Set<? extends UserRole> getRoles();
}
