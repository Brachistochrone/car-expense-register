package com.likhomanov.model;

import lombok.Setter;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Setter
public class UserModel implements User {

    private Long id;
    private String email;
    private String password;
    private Set<UserRoleModel> roles = new HashSet<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public Set<? extends UserRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<UserRoleModel> roles) {
        roles.forEach(this::addRole);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserModel that = (UserModel) o;
        return getEmail().equals(that.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEmail());
    }

    public void addRole(UserRoleModel userRole) {
        userRole.addUser(this);
        roles.add(userRole);
    }

    public void removeRole(UserRoleModel userRole) {
        userRole.removeUser(this);
        roles.remove(userRole);
    }
}
