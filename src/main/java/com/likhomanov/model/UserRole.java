package com.likhomanov.model;

import com.likhomanov.enums.Role;

import java.util.Set;

public interface UserRole {

    Long getId();

    Role getRole();

    Set<? extends User> getUsers();
}
