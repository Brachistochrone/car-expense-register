package com.likhomanov.model;

import com.likhomanov.enums.Role;
import lombok.Setter;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Setter
public class UserRoleModel implements UserRole {

    private Long id;
    private Role role;
    private Set<UserModel> users = new HashSet<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Role getRole() {
        return role;
    }

    @Override
    public Set<? extends User> getUsers() {
        return users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserRoleModel that = (UserRoleModel) o;
        return role.name().equals(that.role.name());
    }

    @Override
    public int hashCode() {
        return Objects.hash(role.name());
    }

    public void addUser(UserModel user) {
        users.add(user);
    }

    public void removeUser(UserModel user) {
        users.remove(user);
    }
}
