package com.likhomanov.services;

import com.likhomanov.dao.repositories.CarDao;
import com.likhomanov.model.Car;
import com.likhomanov.services.mappers.CarMapper;
import com.likhomanov.services.validators.CarValidator;
import com.likhomanov.web.dto.CarDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;

@Service
public class CarService {

    private final CarDao carDao;
    private final CarMapper mapper;
    private final CarValidator carValidator;

    public CarService(CarDao carDao,
                      CarMapper mapper,
                      CarValidator carValidator) {
        this.carDao = carDao;
        this.mapper = mapper;
        this.carValidator = carValidator;
    }

    public CarDto addNewCar(@NotNull CarDto newCar) {
        carValidator.validate(newCar);

        Car car = mapper.dtoToModel(newCar);
        Car savedCar = carDao.save(mapper.modelToEntity(car));

        return mapper.modelToDto(savedCar);
    }

    public Page<CarDto> getAllCars(@NotNull Pageable pageable) {
        Page<? extends Car> cars = carDao.findAll(pageable);
        return mapper.pageOfCarsToDto(cars);
    }
}
