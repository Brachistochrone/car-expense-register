package com.likhomanov.services;

import com.likhomanov.exceptions.HttpRequestException;
import org.springframework.stereotype.Component;

import static org.apache.commons.validator.GenericValidator.isBlankOrNull;

@Component
public class CurrencyCodeNormalizer {

    public String normalizeRub(String response) {
        if (!isBlankOrNull(response)) {
            return response.replace("RUR", "RUB");
        } else {
            throw new HttpRequestException("Currency exchange rate source not available");
        }
    }
}
