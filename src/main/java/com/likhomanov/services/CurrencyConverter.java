package com.likhomanov.services;

import com.likhomanov.dao.queryresults.MoneyByCurrencyResult;
import com.likhomanov.enums.Currency;
import com.likhomanov.model.CurrencyExchangeRate;
import com.likhomanov.model.SpentMoney;
import com.likhomanov.model.SpentMoneyModel;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

import static com.likhomanov.enums.Currency.UAH;

@Component
public class CurrencyConverter {

    private final CurrencyExchangeRateFetcher exchangeRateFetcher;

    public CurrencyConverter(CurrencyExchangeRateFetcher exchangeRateFetcher) {
        this.exchangeRateFetcher = exchangeRateFetcher;
    }

    public SpentMoney convertCurrency(Currency preferredCurrency, @NotNull List<MoneyByCurrencyResult> moneyList) {
        Map<Currency, CurrencyExchangeRate> exchangeRates = exchangeRateFetcher.obtainCurrencyExchangeRates();

        double sumInUAH = moneyList.stream()
                                   .mapToDouble(money -> {
                                       if (money.getCurrency() == UAH) {
                                           return money.getDough();
                                       }
                                       return convertToUAH(money, exchangeRates);
                                   })
                                   .sum();

        SpentMoneyModel spentMoney = new SpentMoneyModel();

        if (preferredCurrency == UAH) {
            spentMoney.setCurrency(UAH);
            spentMoney.setSpentMoney(sumInUAH);
            return spentMoney;
        }

        spentMoney.setCurrency(preferredCurrency);
        spentMoney.setSpentMoney(convertToPreferredCurrency(sumInUAH, preferredCurrency, exchangeRates));

        return spentMoney;
    }

    private Double convertToUAH(MoneyByCurrencyResult money, Map<Currency, CurrencyExchangeRate> exchangeRates) {
        return money.getDough() * exchangeRates.get(money.getCurrency()).getSaleRate();
    }

    private Double convertToPreferredCurrency(double sumInUAH,
                                              Currency preferredCurrency,
                                              Map<Currency, CurrencyExchangeRate> exchangeRates) {
        double resultSum = sumInUAH / exchangeRates.get(preferredCurrency).getSaleRate();
        return Math.round(resultSum * 100.0) / 100.0;
    }
}
