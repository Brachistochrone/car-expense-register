package com.likhomanov.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.likhomanov.enums.Currency;
import com.likhomanov.exceptions.HttpRequestException;
import com.likhomanov.model.CurrencyExchangeRate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class CurrencyExchangeRateFetcher {

    private final String exchangeRateUrl;
    private final CurrencyCodeNormalizer currencyNormalizer;
    private HttpClient httpClient;
    private ObjectMapper objectMapper;

    public CurrencyExchangeRateFetcher(@Value("${currency.exchange.rate.source}") String exchangeRateUrl,
                                       CurrencyCodeNormalizer currencyNormalizer) {
        this.exchangeRateUrl = exchangeRateUrl;
        this.currencyNormalizer = currencyNormalizer;
    }

    public Map<Currency, CurrencyExchangeRate> obtainCurrencyExchangeRates() {
        try {
            HttpRequest request = HttpRequest.newBuilder()
                                             .GET()
                                             .uri(URI.create(exchangeRateUrl))
                                             .build();

            String response = getHttpClient().send(request, HttpResponse.BodyHandlers.ofString())
                                             .body();
            response = currencyNormalizer.normalizeRub(response);
            List<CurrencyExchangeRate> exchangeRates =
                    getObjectMapper().readValue(response,
                                                getObjectMapper().getTypeFactory()
                                                                 .constructCollectionType(List.class,
                                                                                          CurrencyExchangeRate.class));
            return toMap(exchangeRates);
        } catch (IOException | InterruptedException | IllegalArgumentException e) {
            throw new HttpRequestException("Currency exchange rate source not available", e);
        }
    }

    private HttpClient getHttpClient() {
        if (httpClient == null) {
            httpClient = HttpClient.newBuilder()
                                   .version(HttpClient.Version.HTTP_2)
                                   .build();
        }
        return httpClient;
    }

    private ObjectMapper getObjectMapper() {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
        }
        return objectMapper;
    }

    private Map<Currency, CurrencyExchangeRate> toMap(List<CurrencyExchangeRate> exchangeRates) {
        return exchangeRates.stream()
                            .collect(Collectors.toMap(CurrencyExchangeRate::getCurrency, rate -> rate));
    }
}
