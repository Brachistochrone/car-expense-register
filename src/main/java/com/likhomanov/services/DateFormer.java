package com.likhomanov.services;

import com.likhomanov.exceptions.DateFormerException;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.Year;
import java.time.ZoneId;
import java.time.format.DateTimeParseException;
import java.util.Date;

@Component
public class DateFormer {

    public Date getYearHead(@NotNull Year year) {
        LocalDate yearStart = LocalDate.of(year.getValue(), 1, 1);
        return Date.from(yearStart.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public Date getYearTail(@NotNull Year year) {
        LocalDate yearEnd = LocalDate.of(year.getValue(), 12, 31);
        return Date.from(yearEnd.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public Date stringToDate(@NotNull String date) {
        try {
            return Date.from(LocalDate.parse(date).atStartOfDay(ZoneId.systemDefault()).toInstant());
        } catch (DateTimeParseException e) {
            throw new DateFormerException("Invalid date format", e);
        }
    }
}
