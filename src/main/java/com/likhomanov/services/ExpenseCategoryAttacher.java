package com.likhomanov.services;

import com.likhomanov.dao.repositories.ExpenseCategoryDao;
import com.likhomanov.exceptions.EntityNotFoundException;
import com.likhomanov.model.Expense;
import org.springframework.security.util.FieldUtils;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;

@Component
public class ExpenseCategoryAttacher {

    private final ExpenseCategoryDao categoryDao;

    public ExpenseCategoryAttacher(ExpenseCategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }

    public <T extends Expense> T attachExpenseCategory(T expense) {
        Long categoryId = categoryDao.findIdByExpenseType(expense.getExpenseInfo()
                                                                 .getExpenseCategory()
                                                                 .getExpenseType())
                                     .orElseThrow(() -> new EntityNotFoundException("Expense category not found"));

        Field idField = FieldUtils.getField(expense.getExpenseInfo()
                                                   .getExpenseCategory()
                                                   .getClass(), "id");
        try {
            idField.setAccessible(true);
            idField.set(expense.getExpenseInfo().getExpenseCategory(), categoryId);
        } catch (IllegalAccessException e) {
            throw new EntityNotFoundException("Expense category not found");
        }
        return expense;
    }
}
