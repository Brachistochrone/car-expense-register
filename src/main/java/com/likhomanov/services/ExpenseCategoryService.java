package com.likhomanov.services;

import com.likhomanov.dao.repositories.ExpenseCategoryDao;
import com.likhomanov.model.ExpenseCategory;
import com.likhomanov.services.mappers.ExpenseCategoryMapper;
import com.likhomanov.services.validators.NewExpenseCategoryValidator;
import com.likhomanov.web.dto.ExpenseCategoryDto;
import com.likhomanov.web.dto.ListOfExpenseCategoriesDto;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;

@Service
public class ExpenseCategoryService {

    private final ExpenseCategoryDao expenseCategoryDao;
    private final ExpenseCategoryMapper mapper;
    private final NewExpenseCategoryValidator categoryValidator;

    public ExpenseCategoryService(ExpenseCategoryDao expenseCategoryDao,
                                  ExpenseCategoryMapper mapper,
                                  NewExpenseCategoryValidator categoryValidator) {
        this.expenseCategoryDao = expenseCategoryDao;
        this.mapper = mapper;
        this.categoryValidator = categoryValidator;
    }

    @Secured("ROLE_ADMIN")
    public ExpenseCategoryDto addNewCategory(@NotNull ExpenseCategoryDto newExpenseCategory) {
        categoryValidator.validate(newExpenseCategory);

        ExpenseCategory newCategory = mapper.dtoToModel(newExpenseCategory);
        ExpenseCategory savedCategory = expenseCategoryDao.save(mapper.modelToEntity(newCategory));

        return mapper.modelToDto(savedCategory);
    }

    public ListOfExpenseCategoriesDto getAllCategories() {
        List<? extends ExpenseCategory> allCategories = expenseCategoryDao.findAll();
        return mapper.listOfExpenseCategoriesToDto(allCategories);
    }
}
