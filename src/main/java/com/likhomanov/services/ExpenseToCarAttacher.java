package com.likhomanov.services;

import com.likhomanov.dao.repositories.CarDao;
import com.likhomanov.exceptions.EntityNotFoundException;
import com.likhomanov.model.Refuel;
import com.likhomanov.model.ReplacedPart;
import com.likhomanov.services.mappers.RefuelMapper;
import com.likhomanov.services.mappers.ReplacedPartMapper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
public class ExpenseToCarAttacher {

    private final CarDao carDao;
    private final RefuelMapper refuelMapper;
    private final ReplacedPartMapper partMapper;

    public ExpenseToCarAttacher(CarDao carDao,
                                RefuelMapper mapper,
                                ReplacedPartMapper partMapper) {
        this.carDao = carDao;
        this.refuelMapper = mapper;
        this.partMapper = partMapper;
    }

    public Refuel attachNewRefuelToCar(@NotNull Long carId,
                                       @NotNull Refuel newRefuel) throws EntityNotFoundException {
        return carDao.attachRefuelToCarAndSave(carId, refuelMapper.modelToEntity(newRefuel));
    }

    public ReplacedPart attachNewReplacedPartToCar(@NotNull Long carId,
                                                   @NotNull ReplacedPart newReplacedPart) throws EntityNotFoundException {
        return carDao.attachReplacedPartToCarAndSave(carId, partMapper.modelToEntity(newReplacedPart));
    }
}
