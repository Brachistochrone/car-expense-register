package com.likhomanov.services;

import com.likhomanov.dao.repositories.RefuelDao;
import com.likhomanov.exceptions.EntityNotFoundException;
import com.likhomanov.model.ConsumedGasVolumeModel;
import com.likhomanov.model.Refuel;
import com.likhomanov.services.mappers.ConsumedGasVolumeMapper;
import com.likhomanov.services.mappers.RefuelMapper;
import com.likhomanov.services.validators.CarIdValidator;
import com.likhomanov.services.validators.DateValidator;
import com.likhomanov.services.validators.RefuelValidator;
import com.likhomanov.web.dto.ConsumedGasVolumeDto;
import com.likhomanov.web.dto.RefuelDto;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;

@Service
public class RefuelService {

    private final RefuelDao refuelDao;
    private final RefuelMapper refuelMapper;
    private final ConsumedGasVolumeMapper gasVolumeMapper;
    private final ExpenseToCarAttacher refuelAttacher;
    private final ExpenseCategoryAttacher categoryAttacher;
    private final DateFormer dateFormer;
    private final RefuelValidator refuelValidator;
    private final CarIdValidator carIdValidator;
    private final DateValidator dateValidator;

    public RefuelService(RefuelDao refuelDao,
                         RefuelMapper refuelMapper,
                         ConsumedGasVolumeMapper gasVolumeMapper,
                         ExpenseToCarAttacher refuelAttacher,
                         ExpenseCategoryAttacher categoryAttacher,
                         DateFormer dateFormer,
                         RefuelValidator refuelValidator,
                         CarIdValidator carIdValidator,
                         DateValidator dateValidator) {
        this.refuelDao = refuelDao;
        this.refuelMapper = refuelMapper;
        this.gasVolumeMapper = gasVolumeMapper;
        this.refuelAttacher = refuelAttacher;
        this.categoryAttacher = categoryAttacher;
        this.dateFormer = dateFormer;
        this.refuelValidator = refuelValidator;
        this.carIdValidator = carIdValidator;
        this.dateValidator = dateValidator;
    }

    public RefuelDto createNewRefuel(@NotNull RefuelDto newRefuel) {
        refuelValidator.validate(newRefuel);

        Refuel refuelWithCategory = categoryAttacher.attachExpenseCategory(refuelMapper.dtoToModel(newRefuel));
        Refuel savedRefuel = refuelAttacher.attachNewRefuelToCar(newRefuel.getCarId(), refuelWithCategory);

        return refuelMapper.modelToDto(savedRefuel);
    }

    public ConsumedGasVolumeDto getConsumedVolumeOfGasByCarIdForSomePeriod(Long carId,
                                                                           String startDate,
                                                                           String endDate) {
        carIdValidator.validate(carId);
        dateValidator.validate(startDate);
        dateValidator.validate(endDate);

        Double guzzledGasVolume = refuelDao.findGuzzledGasForSomePeriodByCarId(carId,
                                                                               dateFormer.stringToDate(startDate),
                                                                               dateFormer.stringToDate(endDate))
                                           .orElseThrow(() -> new EntityNotFoundException("No data for specified " +
                                                                                          "car id or period " +
                                                                                          "was found"));

        ConsumedGasVolumeModel gasVolume = new ConsumedGasVolumeModel();
        gasVolume.setVolume(guzzledGasVolume);

        return gasVolumeMapper.modelToDto(gasVolume);
    }
}
