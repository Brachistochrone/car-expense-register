package com.likhomanov.services;

import com.likhomanov.dao.queryresults.MoneyByCurrencyResult;
import com.likhomanov.dao.repositories.ReplacedPartDao;
import com.likhomanov.enums.Currency;
import com.likhomanov.model.ReplacedPart;
import com.likhomanov.model.SpentMoney;
import com.likhomanov.services.mappers.ReplacedPartMapper;
import com.likhomanov.services.mappers.SpentMoneyMapper;
import com.likhomanov.services.validators.CarIdValidator;
import com.likhomanov.services.validators.CurrencyValidator;
import com.likhomanov.services.validators.NewReplacedPartValidator;
import com.likhomanov.services.validators.YearValidator;
import com.likhomanov.web.dto.NewReplacedPartDto;
import com.likhomanov.web.dto.ReplacedPartDto;
import com.likhomanov.web.dto.SpentMoneyDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.time.Year;
import java.util.Date;
import java.util.List;

@Service
public class ReplacedPartService {

    private final ReplacedPartDao replacedPartDao;
    private final ReplacedPartMapper partMapper;
    private final SpentMoneyMapper moneyMapper;
    private final ExpenseCategoryAttacher categoryAttacher;
    private final ExpenseToCarAttacher replacedPartAttacher;
    private final DateFormer dateFormer;
    private final CurrencyConverter currencyConverter;
    private final NewReplacedPartValidator newPartValidator;
    private final CarIdValidator carIdValidator;
    private final YearValidator yearValidator;
    private final CurrencyValidator currencyValidator;

    public ReplacedPartService(ReplacedPartDao replacedPartDao,
                               ReplacedPartMapper partMapper,
                               SpentMoneyMapper moneyMapper,
                               ExpenseCategoryAttacher categoryAttacher,
                               ExpenseToCarAttacher replacedPartAttacher,
                               DateFormer dateFormer,
                               CurrencyConverter currencyConverter,
                               NewReplacedPartValidator newPartValidator,
                               CarIdValidator carIdValidator,
                               YearValidator yearValidator,
                               CurrencyValidator currencyValidator) {
        this.replacedPartDao = replacedPartDao;
        this.partMapper = partMapper;
        this.moneyMapper = moneyMapper;
        this.categoryAttacher = categoryAttacher;
        this.replacedPartAttacher = replacedPartAttacher;
        this.dateFormer = dateFormer;
        this.currencyConverter = currencyConverter;
        this.newPartValidator = newPartValidator;
        this.carIdValidator = carIdValidator;
        this.yearValidator = yearValidator;
        this.currencyValidator = currencyValidator;
    }

    public NewReplacedPartDto createNewReplacedPart(@NotNull NewReplacedPartDto newReplacedPart) {
        newPartValidator.validate(newReplacedPart);

        ReplacedPart partWithCategory = categoryAttacher.attachExpenseCategory(partMapper.dtoToModel(newReplacedPart));
        ReplacedPart savedPart = replacedPartAttacher.attachNewReplacedPartToCar(newReplacedPart.getCarId(),
                                                                                 partWithCategory);

        return partMapper.modelToNewDto(savedPart);
    }

    public SpentMoneyDto getMoneySpentOnSparePartsByCarIdForYear(Long carId, Year year, Currency desiredCurrency) {
        carIdValidator.validate(carId);
        yearValidator.validate(year);
        currencyValidator.validate(desiredCurrency);

        Date yearHead = dateFormer.getYearHead(year);
        Date yearTail = dateFormer.getYearTail(year);

        List<MoneyByCurrencyResult> doughSpentOnSpareParts =
                replacedPartDao.findDoughSpentOnSparePartsForSomePeriodByCarId(carId, yearHead, yearTail);

        SpentMoney spentMoney = currencyConverter.convertCurrency(desiredCurrency, doughSpentOnSpareParts);

        return moneyMapper.modelToDto(spentMoney);
    }

    public SpentMoneyDto getMoneySpentOnSparePartsByCarId(Long carId, Currency desiredCurrency) {
        carIdValidator.validate(carId);
        currencyValidator.validate(desiredCurrency);

        List<MoneyByCurrencyResult> doughSpentOnSpareParts = replacedPartDao.findDoughSpentOnSparePartsByCarId(carId);

        SpentMoney spentMoney = currencyConverter.convertCurrency(desiredCurrency, doughSpentOnSpareParts);

        return moneyMapper.modelToDto(spentMoney);
    }

    public Page<ReplacedPartDto> getReplacedPartsByCarId(@NotNull Long carId, @NotNull Pageable pageable) {
        carIdValidator.validate(carId);

        Page<? extends ReplacedPart> replacedParts = replacedPartDao.findByCarId(carId, pageable);

        return partMapper.pageOfReplacedPartsToDto(replacedParts);
    }
}
