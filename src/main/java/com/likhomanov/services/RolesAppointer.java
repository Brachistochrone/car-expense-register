package com.likhomanov.services;

import com.likhomanov.dao.repositories.UserRoleDao;
import com.likhomanov.exceptions.RoleAppointerException;
import com.likhomanov.model.UserModel;
import com.likhomanov.model.UserRoleModel;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

import static com.likhomanov.enums.Role.ROLE_USER;

@Component
public class RolesAppointer {

    private final UserRoleDao roleDao;

    public RolesAppointer(UserRoleDao roleDao) {
        this.roleDao = roleDao;
    }

    public UserModel appointRoleUser(@NotNull UserModel newUser) {
        Long roleId = roleDao.findRoleIdByRoleName(ROLE_USER)
                             .orElseThrow(() -> new RoleAppointerException("Role does not exist"));

        UserRoleModel role = new UserRoleModel();
        role.setId(roleId);
        role.setRole(ROLE_USER);
        newUser.addRole(role);
        return newUser;
    }
}
