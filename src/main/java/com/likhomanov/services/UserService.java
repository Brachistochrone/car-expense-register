package com.likhomanov.services;

import com.likhomanov.dao.repositories.UserDao;
import com.likhomanov.exceptions.EntityNotFoundException;
import com.likhomanov.model.User;
import com.likhomanov.services.mappers.UserMapper;
import com.likhomanov.services.mappers.UserRoleMapper;
import com.likhomanov.services.validators.NewUserValidator;
import com.likhomanov.web.dto.NewUserDto;
import com.likhomanov.web.dto.UserDto;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;

@Service
public class UserService implements UserDetailsService {

    private final UserDao userDao;
    private final RolesAppointer rolesAppointer;
    private final UserMapper userMapper;
    private final UserRoleMapper roleMapper;
    private final NewUserValidator userValidator;

    public UserService(UserDao userDao,
                       RolesAppointer rolesAppointer,
                       UserMapper mapper,
                       UserRoleMapper roleMapper,
                       NewUserValidator userValidator) {
        this.userDao = userDao;
        this.rolesAppointer = rolesAppointer;
        this.userMapper = mapper;
        this.roleMapper = roleMapper;
        this.userValidator = userValidator;
    }

    public UserDto saveNewUser(@NotNull NewUserDto newUser) {
        userValidator.validate(newUser);

        User newUserWithRole = rolesAppointer.appointRoleUser(userMapper.dtoToModel(newUser));

        User savedUser = userDao.saveUserWithRole(userMapper.modelToEntity(newUserWithRole));

        return userMapper.modelToDto(savedUser);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User foundUser = userDao.findOneByEmail(email)
                                .orElseThrow(() -> new EntityNotFoundException("User with email " +
                                                                                       email +
                                                                                       " not found"));

        return new org.springframework.security.core.userdetails.User(foundUser.getEmail(),
                                                                      foundUser.getPassword(),
                                                                      roleMapper.userRolesToAuthorities(foundUser.getRoles()));
    }
}
