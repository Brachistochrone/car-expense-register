package com.likhomanov.services.mappers;

import com.likhomanov.dao.entities.CarInfoEntity;
import com.likhomanov.model.CarInfo;
import com.likhomanov.model.CarInfoModel;
import com.likhomanov.web.dto.CarInfoDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class CarInfoMapper implements DtoToModelMapper<CarInfoDto, CarInfoModel>,
                                               ModelToEntityMapper<CarInfo, CarInfoEntity>,
                                               EntityToModelMapper<CarInfo, CarInfoModel>,
                                               ModelToDtoMapper<CarInfo, CarInfoDto> {
}
