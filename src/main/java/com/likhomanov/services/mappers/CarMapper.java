package com.likhomanov.services.mappers;

import com.likhomanov.dao.entities.CarEntity;
import com.likhomanov.dao.entities.CarInfoEntity;
import com.likhomanov.dao.entities.RefuelEntity;
import com.likhomanov.dao.entities.ReplacedPartEntity;
import com.likhomanov.model.*;
import com.likhomanov.web.dto.CarDto;
import com.likhomanov.web.dto.CarInfoDto;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class CarMapper implements DtoToModelMapper<CarDto, CarModel>,
                                           ModelToEntityMapper<Car, CarEntity>,
                                           EntityToModelMapper<Car, CarModel>,
                                           ModelToDtoMapper<Car, CarDto> {

    @Autowired
    private CarInfoMapper infoMapper;
    @Autowired
    private ReplacedPartMapper replacedPartMapper;
    @Autowired
    private RefuelMapper refuelMapper;

    protected CarInfoModel infoDtoToModel(CarInfoDto carInfo) {
        return infoMapper.dtoToModel(carInfo);
    }

    protected CarInfoEntity infoModelToEntity(CarInfo carInfo) {
        return infoMapper.modelToEntity(carInfo);
    }

    protected CarInfoModel infoEntityToModel(CarInfo carInfo) {
        return infoMapper.entityToModel(carInfo);
    }

    protected CarInfoDto infoModelToDto(CarInfo carInfo) {
        return infoMapper.modelToDto(carInfo);
    }

    protected Set<ReplacedPartModel> replacedPartsToModels(Set<? extends ReplacedPart> replacedParts) {
        return replacedPartMapper.replacedPartsToModels(replacedParts);
    }

    protected Set<ReplacedPartEntity> replacedPartsToEntities(Set<? extends ReplacedPart> replacedParts) {
        return replacedPartMapper.replacedPartsToEntities(replacedParts);
    }

    protected Set<RefuelModel> refuelsToModels(Set<? extends Refuel> refuels) {
        return refuelMapper.refuelsToModels(refuels);
    }

    protected Set<RefuelEntity> refuelsToEntities(Set<? extends Refuel> refuels) {
        return refuelMapper.refuelsToEntities(refuels);
    }

    public Page<CarDto> pageOfCarsToDto(Page<? extends Car> cars) {
        if (cars == null) {
            return null;
        }

        List<CarDto> carDtos = cars.stream()
                                   .map(this::modelToDto)
                                   .collect(Collectors.toList());

        return new PageImpl<>(carDtos, cars.getPageable(), cars.getTotalElements());
    }
}
