package com.likhomanov.services.mappers;

import com.likhomanov.model.ConsumedGasVolume;
import com.likhomanov.web.dto.ConsumedGasVolumeDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class ConsumedGasVolumeMapper implements ModelToDtoMapper<ConsumedGasVolume, ConsumedGasVolumeDto> {
}
