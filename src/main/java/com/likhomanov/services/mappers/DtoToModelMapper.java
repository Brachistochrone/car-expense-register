package com.likhomanov.services.mappers;

public interface DtoToModelMapper<D, M> {

    M dtoToModel(D dto);
}
