package com.likhomanov.services.mappers;

public interface EntityToModelMapper<E, M> {

    M entityToModel(E entity);
}
