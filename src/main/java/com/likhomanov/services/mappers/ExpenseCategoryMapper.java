package com.likhomanov.services.mappers;

import com.likhomanov.dao.entities.ExpenseCategoryEntity;
import com.likhomanov.model.ExpenseCategory;
import com.likhomanov.model.ExpenseCategoryModel;
import com.likhomanov.web.dto.ExpenseCategoryDto;
import com.likhomanov.web.dto.ListOfExpenseCategoriesDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class ExpenseCategoryMapper implements DtoToModelMapper<ExpenseCategoryDto, ExpenseCategoryModel>,
                                                       ModelToEntityMapper<ExpenseCategory, ExpenseCategoryEntity>,
                                                       EntityToModelMapper<ExpenseCategory, ExpenseCategoryModel>,
                                                       ModelToDtoMapper<ExpenseCategory, ExpenseCategoryDto> {

    public ListOfExpenseCategoriesDto listOfExpenseCategoriesToDto(List<? extends ExpenseCategory> categories) {
        ListOfExpenseCategoriesDto listDto = new ListOfExpenseCategoriesDto();

        categories.stream()
                  .map(this::modelToDto)
                  .forEach(listDto::addExpenseCategory);

        return listDto;
    }
}
