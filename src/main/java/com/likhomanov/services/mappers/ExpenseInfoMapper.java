package com.likhomanov.services.mappers;

import com.likhomanov.dao.entities.ExpenseCategoryEntity;
import com.likhomanov.dao.entities.ExpenseInfoEntity;
import com.likhomanov.model.ExpenseCategory;
import com.likhomanov.model.ExpenseCategoryModel;
import com.likhomanov.model.ExpenseInfo;
import com.likhomanov.model.ExpenseInfoModel;
import com.likhomanov.web.dto.ExpenseCategoryDto;
import com.likhomanov.web.dto.ExpenseInfoDto;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class ExpenseInfoMapper implements DtoToModelMapper<ExpenseInfoDto, ExpenseInfoModel>,
                                                   ModelToEntityMapper<ExpenseInfo, ExpenseInfoEntity>,
                                                   EntityToModelMapper<ExpenseInfo, ExpenseInfoModel>,
                                                   ModelToDtoMapper<ExpenseInfo, ExpenseInfoDto> {

    @Autowired
    private ExpenseCategoryMapper categoryMapper;

    protected ExpenseCategoryModel categoryDtoToModel(ExpenseCategoryDto dto) {
        return categoryMapper.dtoToModel(dto);
    }

    protected ExpenseCategoryEntity categoryModelToEntity(ExpenseCategory category) {
        return categoryMapper.modelToEntity(category);
    }

    protected ExpenseCategoryModel categoryEntityToModel(ExpenseCategory category) {
        return categoryMapper.entityToModel(category);
    }

    protected ExpenseCategoryDto categoryModelToDto(ExpenseCategory category) {
        return categoryMapper.modelToDto(category);
    }
}
