package com.likhomanov.services.mappers;

public interface ModelToDtoMapper<M, D> {

    D modelToDto(M model);
}
