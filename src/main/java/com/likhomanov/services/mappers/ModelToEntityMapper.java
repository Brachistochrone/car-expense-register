package com.likhomanov.services.mappers;

public interface ModelToEntityMapper<M, E> {

    E modelToEntity(M model);
}
