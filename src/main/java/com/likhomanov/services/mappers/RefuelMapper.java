package com.likhomanov.services.mappers;

import com.likhomanov.dao.entities.ExpenseInfoEntity;
import com.likhomanov.dao.entities.RefuelEntity;
import com.likhomanov.model.ExpenseInfo;
import com.likhomanov.model.ExpenseInfoModel;
import com.likhomanov.model.Refuel;
import com.likhomanov.model.RefuelModel;
import com.likhomanov.web.dto.ExpenseInfoDto;
import com.likhomanov.web.dto.RefuelDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

@Mapper(componentModel = "spring")
public abstract class RefuelMapper implements DtoToModelMapper<RefuelDto, RefuelModel>,
                                              ModelToEntityMapper<Refuel, RefuelEntity>,
                                              EntityToModelMapper<Refuel, RefuelModel>,
                                              ModelToDtoMapper<Refuel, RefuelDto> {

    @Autowired
    private ExpenseInfoMapper infoMapper;

    @Override
    @Mapping(source = "refuelInfo", target = "expenseInfo")
    public abstract RefuelModel dtoToModel(RefuelDto dto);

    @Override
    public RefuelDto modelToDto(Refuel model) {
        if (model == null) {
            return null;
        }

        RefuelDto refuelDto = new RefuelDto();

        refuelDto.setRefuelInfo(infoModelToDto(model.getExpenseInfo()));
        refuelDto.setVolume(model.getVolume());
        refuelDto.setCarId(model.getCar().getId());

        return refuelDto;
    }

    @Override
    @Mapping(ignore = true, target = "car")
    public abstract RefuelEntity modelToEntity(Refuel model);

    @Override
    @Mapping(ignore = true, target = "car")
    public abstract RefuelModel entityToModel(Refuel entity);

    public abstract Set<RefuelModel> refuelsToModels(Set<? extends Refuel> refuels);

    public abstract Set<RefuelEntity> refuelsToEntities(Set<? extends Refuel> refuels);

    protected ExpenseInfoModel infoDtoToModel(ExpenseInfoDto dto) {
        return infoMapper.dtoToModel(dto);
    }

    protected ExpenseInfoEntity infoModelToEntity(ExpenseInfo info) {
        return infoMapper.modelToEntity(info);
    }

    protected ExpenseInfoModel infoEntityToModel(ExpenseInfo info) {
        return infoMapper.entityToModel(info);
    }

    protected ExpenseInfoDto infoModelToDto(ExpenseInfo info) {
        return infoMapper.modelToDto(info);
    }
}
