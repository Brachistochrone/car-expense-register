package com.likhomanov.services.mappers;

import com.likhomanov.dao.entities.ExpenseInfoEntity;
import com.likhomanov.dao.entities.ReplacedPartEntity;
import com.likhomanov.model.ExpenseInfo;
import com.likhomanov.model.ExpenseInfoModel;
import com.likhomanov.model.ReplacedPart;
import com.likhomanov.model.ReplacedPartModel;
import com.likhomanov.web.dto.ExpenseInfoDto;
import com.likhomanov.web.dto.NewReplacedPartDto;
import com.likhomanov.web.dto.ReplacedPartDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class ReplacedPartMapper implements DtoToModelMapper<NewReplacedPartDto, ReplacedPartModel>,
                                                    ModelToEntityMapper<ReplacedPart, ReplacedPartEntity>,
                                                    EntityToModelMapper<ReplacedPart, ReplacedPartModel>,
                                                    ModelToDtoMapper<ReplacedPart, ReplacedPartDto> {

    @Autowired
    private ExpenseInfoMapper infoMapper;

    @Override
    @Mapping(source = "replacedPartInfo", target = "expenseInfo")
    public abstract ReplacedPartModel dtoToModel(NewReplacedPartDto dto);

    @Override
    @Mapping(source = "expenseInfo", target = "replacedPartInfo")
    public abstract ReplacedPartDto modelToDto(ReplacedPart model);

    @Override
    @Mapping(ignore = true, target = "car")
    public abstract ReplacedPartModel entityToModel(ReplacedPart entity);

    @Override
    @Mapping(ignore = true, target = "car")
    public abstract ReplacedPartEntity modelToEntity(ReplacedPart model);

    public abstract Set<ReplacedPartModel> replacedPartsToModels(Set<? extends ReplacedPart> replacedParts);

    public abstract Set<ReplacedPartEntity> replacedPartsToEntities(Set<? extends ReplacedPart> replacedParts);

    protected ExpenseInfoModel infoDtoToModel(ExpenseInfoDto dto) {
        return infoMapper.dtoToModel(dto);
    }

    protected ExpenseInfoEntity infoModelToEntity(ExpenseInfo info) {
        return infoMapper.modelToEntity(info);
    }

    protected ExpenseInfoModel infoEntityToModel(ExpenseInfo info) {
        return infoMapper.entityToModel(info);
    }

    protected ExpenseInfoDto infoModelToDto(ExpenseInfo info) {
        return infoMapper.modelToDto(info);
    }

    public Page<ReplacedPartDto> pageOfReplacedPartsToDto(Page<? extends ReplacedPart> parts) {
        if (parts == null) {
            return null;
        }

        List<ReplacedPartDto> partDtos = parts.stream()
                                              .map(this::modelToDto)
                                              .collect(Collectors.toList());

        return new PageImpl<>(partDtos, parts.getPageable(), parts.getTotalElements());
    }

    public NewReplacedPartDto modelToNewDto(ReplacedPart model) {
        if (model == null) {
            return null;
        }

        NewReplacedPartDto replacedPartDto = new NewReplacedPartDto();

        replacedPartDto.setReplacedPartInfo(infoModelToDto(model.getExpenseInfo()));
        replacedPartDto.setSerialNumber(model.getSerialNumber());
        replacedPartDto.setName(model.getName());
        replacedPartDto.setCarId(model.getCar().getId());

        return replacedPartDto;
    }
}
