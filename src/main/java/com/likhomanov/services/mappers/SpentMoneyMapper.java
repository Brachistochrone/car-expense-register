package com.likhomanov.services.mappers;

import com.likhomanov.model.SpentMoney;
import com.likhomanov.web.dto.SpentMoneyDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class SpentMoneyMapper implements ModelToDtoMapper<SpentMoney, SpentMoneyDto> {
}
