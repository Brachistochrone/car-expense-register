package com.likhomanov.services.mappers;

import com.likhomanov.dao.entities.UserEntity;
import com.likhomanov.dao.entities.UserRoleEntity;
import com.likhomanov.model.User;
import com.likhomanov.model.UserModel;
import com.likhomanov.model.UserRole;
import com.likhomanov.model.UserRoleModel;
import com.likhomanov.web.dto.AuthenticatedUserDto;
import com.likhomanov.web.dto.NewUserDto;
import com.likhomanov.web.dto.UserDto;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Set;

@Mapper(componentModel = "spring")
public abstract class UserMapper implements DtoToModelMapper<NewUserDto, UserModel>,
                                            ModelToEntityMapper<User, UserEntity>,
                                            EntityToModelMapper<User, UserModel>,
                                            ModelToDtoMapper<User, UserDto> {

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Autowired
    private UserRoleMapper roleMapper;

    @Override
    public UserModel dtoToModel(NewUserDto newUserDto) {
        if (newUserDto == null) {
            return null;
        }

        UserModel userModel = new UserModel();

        userModel.setEmail(newUserDto.getEmail());
        String encodedPassword = passwordEncoder.encode(newUserDto.getPassword());
        userModel.setPassword(encodedPassword);

        return userModel;
    }

    protected Set<UserRoleEntity> rolesToEntities(Set<? extends UserRole> roles) {
        return roleMapper.rolesToEntities(roles);
    }

    protected Set<UserRoleModel> rolesToModels(Set<? extends UserRole> roles) {
        return roleMapper.rolesToModels(roles);
    }

    public AuthenticatedUserDto springUserToAuthenticatedUserDto(org.springframework.security.core.userdetails.User user) {
        if (user == null) {
            return null;
        }

        AuthenticatedUserDto authenticatedUser = new AuthenticatedUserDto();
        authenticatedUser.setEmail(user.getUsername());
        authenticatedUser.setPassword(user.getPassword());
        authenticatedUser.setRoles(roleMapper.authoritiesToRoles(user.getAuthorities()));

        return authenticatedUser;
    }
}
