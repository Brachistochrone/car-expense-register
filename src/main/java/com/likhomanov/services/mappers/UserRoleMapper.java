package com.likhomanov.services.mappers;

import com.likhomanov.dao.entities.UserRoleEntity;
import com.likhomanov.enums.Role;
import com.likhomanov.model.UserRole;
import com.likhomanov.model.UserRoleModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class UserRoleMapper implements ModelToEntityMapper<UserRole, UserRoleEntity>,
                                                EntityToModelMapper<UserRole, UserRoleModel> {

    @Override
    @Mapping(ignore = true, target = "users")
    public abstract UserRoleModel entityToModel(UserRole role);

    @Override
    @Mapping(ignore = true, target = "users")
    public abstract UserRoleEntity modelToEntity(UserRole role);

    public abstract Set<UserRoleEntity> rolesToEntities(Set<? extends UserRole> roles);

    public abstract Set<UserRoleModel> rolesToModels(Set<? extends UserRole> roles);

    public Set<? extends GrantedAuthority> userRolesToAuthorities(@NotNull Set<? extends UserRole> roles) {
        return roles.stream()
                    .map(this::userRoleToAuthority)
                    .collect(Collectors.toSet());
    }

    protected GrantedAuthority userRoleToAuthority(@NotNull UserRole role) {
        return new SimpleGrantedAuthority(role.getRole().name());
    }

    protected Set<Role> authoritiesToRoles(@NotNull Collection<GrantedAuthority> authorities) {
        return authorities.stream()
                          .map(auth -> Role.valueOf(auth.getAuthority()))
                          .collect(Collectors.toSet());
    }

    public Set<? extends GrantedAuthority> rolesToAuthorities(@NotNull Set<Role> roles) {
        return roles.stream()
                    .map(this::roleToAuthority)
                    .collect(Collectors.toSet());
    }

    protected GrantedAuthority roleToAuthority(@NotNull Role role) {
        return new SimpleGrantedAuthority(role.name());
    }
}
