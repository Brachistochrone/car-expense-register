package com.likhomanov.services.validators;

import com.likhomanov.exceptions.ValidationException;
import com.likhomanov.web.dto.AuthenticatedUserDto;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

import static org.apache.commons.validator.GenericValidator.isBlankOrNull;

@Component
public class AuthenticatedUserValidator implements Validator<AuthenticatedUserDto>, EmailPasswordValidator {

    @Override
    public void validate(@NotNull AuthenticatedUserDto authenticatedUser) {
        validateEmail(authenticatedUser.getEmail());

        if (!isBlankOrNull(authenticatedUser.getPassword())) {
            throw new ValidationException("Password should not be revealed");
        }

        if (authenticatedUser.getRoles() == null || authenticatedUser.getRoles().isEmpty()) {
            throw new ValidationException("User does not have roles");
        }
    }
}
