package com.likhomanov.services.validators;

import com.likhomanov.exceptions.ValidationException;
import org.springframework.stereotype.Component;

import static org.apache.commons.validator.GenericValidator.minValue;

@Component
public class CarIdValidator implements Validator<Long> {

    @Override
    public void validate(Long carId) {
        if (carId == null || !minValue(carId, 1)) {
            throw new ValidationException("Car id is missing");
        }
    }
}
