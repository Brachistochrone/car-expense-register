package com.likhomanov.services.validators;

import com.likhomanov.dao.repositories.CarInfoDao;
import com.likhomanov.exceptions.NotUniqueEntityException;
import com.likhomanov.exceptions.ValidationException;
import com.likhomanov.web.dto.CarInfoDto;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

import static org.apache.commons.validator.GenericValidator.*;

@Component
public class CarInfoValidator implements Validator<CarInfoDto> {

    private final CarInfoDao carInfoDao;

    public CarInfoValidator(CarInfoDao carInfoDao) {
        this.carInfoDao = carInfoDao;
    }

    @Override
    public void validate(@NotNull CarInfoDto carInfo) {
        if (isBlankOrNull(carInfo.getVinCode())) {
            throw new ValidationException("Car's vin code is missing");
        }

        if (isBlankOrNull(carInfo.getModel())) {
            throw new ValidationException("Car's model is missing");
        }

        if (isBlankOrNull(carInfo.getColor())) {
            throw new ValidationException("Car's color is missing");
        }

        if (isBlankOrNull(carInfo.getBody())) {
            throw new ValidationException("Car's body type is missing");
        }

        if (isBlankOrNull(carInfo.getYear())) {
            throw new ValidationException("Car's production year is missing");
        }

        if (!minLength(carInfo.getVinCode(), 4) || !matchRegexp(carInfo.getVinCode(), ONLY_NUMBERS_AND_LETTERS)) {
            throw new ValidationException("Invalid car's vin code format");
        }

        if (!minLength(carInfo.getModel(), 3) || !matchRegexp(carInfo.getModel(), ONLY_NUMBERS_LETTERS_AND_SPACE)) {
            throw new ValidationException("Invalid car's model format");
        }

        if (!minLength(carInfo.getColor(), 3) || !matchRegexp(carInfo.getColor(), ONLY_LETTERS_AND_SPACE)) {
            throw new ValidationException("Invalid car's color format");
        }

        if (!minLength(carInfo.getBody(), 4) || !matchRegexp(carInfo.getBody(), ONLY_LETTERS_AND_SPACE)) {
            throw new ValidationException("Invalid car's body type format");
        }

        if (!minLength(carInfo.getYear(), 4) || !matchRegexp(carInfo.getYear(), ONLY_NUMBERS)) {
            throw new ValidationException("Invalid car's year format");
        }

        if (carInfoDao.existsByVinCode(carInfo.getVinCode())) {
            throw new NotUniqueEntityException("Car with such vin code already exists");
        }
    }
}
