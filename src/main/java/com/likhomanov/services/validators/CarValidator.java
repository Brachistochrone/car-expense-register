package com.likhomanov.services.validators;

import com.likhomanov.exceptions.ValidationException;
import com.likhomanov.web.dto.CarDto;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
public class CarValidator implements Validator<CarDto> {

    private final CarInfoValidator infoValidator;

    public CarValidator(CarInfoValidator infoValidator) {
        this.infoValidator = infoValidator;
    }

    @Override
    public void validate(@NotNull CarDto car) {
        if (car.getCarInfo() == null) throw new ValidationException("Car info is missing");

        infoValidator.validate(car.getCarInfo());
    }
}
