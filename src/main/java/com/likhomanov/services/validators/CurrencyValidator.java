package com.likhomanov.services.validators;

import com.likhomanov.enums.Currency;
import com.likhomanov.exceptions.ValidationException;
import org.springframework.stereotype.Component;

@Component
public class CurrencyValidator implements Validator<Currency> {

    @Override
    public void validate(Currency currency) {
        if (currency == null) {
            throw new ValidationException("Currency is missing");
        }
    }
}
