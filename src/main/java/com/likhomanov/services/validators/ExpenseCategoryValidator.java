package com.likhomanov.services.validators;

import com.likhomanov.exceptions.ValidationException;
import com.likhomanov.web.dto.ExpenseCategoryDto;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

import static org.apache.commons.validator.GenericValidator.*;

@Component
public class ExpenseCategoryValidator implements Validator<ExpenseCategoryDto> {

    @Override
    public void validate(@NotNull ExpenseCategoryDto category) {
        if (isBlankOrNull(category.getExpenseType())) {
            throw new ValidationException("Expense type is missing");
        }

        if (!minLength(category.getExpenseType(), 3) || !matchRegexp(category.getExpenseType(), ONLY_LETTERS)) {
            throw new ValidationException("Invalid expense type format");
        }
    }
}
