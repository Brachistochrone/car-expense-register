package com.likhomanov.services.validators;

import com.likhomanov.exceptions.ValidationException;
import com.likhomanov.web.dto.ExpenseInfoDto;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

import static org.apache.commons.validator.GenericValidator.isBlankOrNull;
import static org.apache.commons.validator.GenericValidator.minValue;

@Component
public class ExpenseInfoValidator implements Validator<ExpenseInfoDto> {

    private final ExpenseCategoryValidator categoryValidator;

    public ExpenseInfoValidator(ExpenseCategoryValidator categoryValidator) {
        this.categoryValidator = categoryValidator;
    }

    @Override
    public void validate(@NotNull ExpenseInfoDto info) {
        if (info.getDate() == null) {
            throw new ValidationException("Expense date is missing");
        }

        if (info.getSum() == null || !minValue(info.getSum(), 0.01)) {
            throw new ValidationException("Expense sum is missing");
        }

        if (info.getCurrency() == null) {
            throw new ValidationException("Currency is missing");
        }

        if (isBlankOrNull(info.getCompanyName())) {
            throw new ValidationException("Company name is missing");
        }

        if (isBlankOrNull(info.getCompanyAddress())) {
            throw new ValidationException("Company address is missing");
        }

        if (isBlankOrNull(info.getDescription())) {
            throw new ValidationException("Expense description is missing");
        }

        if (info.getExpenseCategory() == null) {
            throw new ValidationException("Expense category is missing");
        }

        categoryValidator.validate(info.getExpenseCategory());
    }
}
