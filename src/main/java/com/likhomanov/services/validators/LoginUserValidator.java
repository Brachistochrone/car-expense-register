package com.likhomanov.services.validators;

import com.likhomanov.web.dto.LoginUserDto;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
public class LoginUserValidator implements Validator<LoginUserDto>, EmailPasswordValidator {

    @Override
    public void validate(@NotNull LoginUserDto loginUser) {
        validateEmail(loginUser.getEmail());
        validatePassword(loginUser.getPassword());
    }
}
