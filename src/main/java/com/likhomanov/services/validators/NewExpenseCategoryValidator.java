package com.likhomanov.services.validators;

import com.likhomanov.dao.repositories.ExpenseCategoryDao;
import com.likhomanov.exceptions.NotUniqueEntityException;
import com.likhomanov.web.dto.ExpenseCategoryDto;
import org.springframework.stereotype.Component;

@Component
public class NewExpenseCategoryValidator implements Validator<ExpenseCategoryDto> {

    private final ExpenseCategoryValidator categoryValidator;
    private final ExpenseCategoryDao categoryDao;

    public NewExpenseCategoryValidator(ExpenseCategoryValidator categoryValidator,
                                       ExpenseCategoryDao categoryDao) {
        this.categoryValidator = categoryValidator;
        this.categoryDao = categoryDao;
    }

    @Override
    public void validate(ExpenseCategoryDto category) {
        categoryValidator.validate(category);

        if (categoryDao.existsByExpenseType(category.getExpenseType())) {
            throw new NotUniqueEntityException("Such an expense category already exists");
        }
    }
}
