package com.likhomanov.services.validators;

import com.likhomanov.dao.repositories.ReplacedPartDao;
import com.likhomanov.exceptions.NotUniqueEntityException;
import com.likhomanov.exceptions.ValidationException;
import com.likhomanov.web.dto.NewReplacedPartDto;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

import static org.apache.commons.validator.GenericValidator.*;

@Component
public class NewReplacedPartValidator implements Validator<NewReplacedPartDto> {

    private final CarIdValidator carIdValidator;
    private final ExpenseInfoValidator infoValidator;
    private final ReplacedPartDao partDao;

    public NewReplacedPartValidator(CarIdValidator carIdValidator,
                                    ExpenseInfoValidator infoValidator,
                                    ReplacedPartDao partDao) {
        this.carIdValidator = carIdValidator;
        this.infoValidator = infoValidator;
        this.partDao = partDao;
    }

    @Override
    public void validate(@NotNull NewReplacedPartDto newReplacedPart) {
        if (isBlankOrNull(newReplacedPart.getSerialNumber())) {
            throw new ValidationException("Replaced part's serial number is missing");
        }

        if (isBlankOrNull(newReplacedPart.getName())) {
            throw new ValidationException("Replaced part's name is missing");
        }

        if (newReplacedPart.getReplacedPartInfo() == null) {
            throw new ValidationException("Replaced part's info is missing");
        }

        if (!minLength(newReplacedPart.getSerialNumber(), 4) ||
            !matchRegexp(newReplacedPart.getSerialNumber(), ONLY_NUMBERS_LETTERS_AND_SPACE)) {
            throw new ValidationException("Invalid serial number format");
        }

        carIdValidator.validate(newReplacedPart.getCarId());

        infoValidator.validate(newReplacedPart.getReplacedPartInfo());

        if (partDao.existsBySerialNumber(newReplacedPart.getSerialNumber())) {
            throw new NotUniqueEntityException("Replaced part with such a serial number already exists");
        }
    }
}
