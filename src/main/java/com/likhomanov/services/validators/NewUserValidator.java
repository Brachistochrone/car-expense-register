package com.likhomanov.services.validators;

import com.likhomanov.dao.repositories.UserDao;
import com.likhomanov.exceptions.NotUniqueEntityException;
import com.likhomanov.web.dto.NewUserDto;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
public class NewUserValidator implements Validator<NewUserDto>, EmailPasswordValidator {

    private final UserDao userDao;

    public NewUserValidator(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void validate(@NotNull NewUserDto newUser) {
        validateEmail(newUser.getEmail());
        validatePassword(newUser.getPassword());

        if (userDao.existsByEmail(newUser.getEmail())) {
            throw new NotUniqueEntityException("User with such email already exists");
        }
    }
}
