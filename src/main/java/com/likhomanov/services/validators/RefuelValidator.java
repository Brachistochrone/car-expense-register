package com.likhomanov.services.validators;

import com.likhomanov.exceptions.ValidationException;
import com.likhomanov.web.dto.RefuelDto;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

import static org.apache.commons.validator.GenericValidator.minValue;

@Component
public class RefuelValidator implements Validator<RefuelDto> {

    private final ExpenseInfoValidator infoValidator;
    private final CarIdValidator carIdValidator;

    public RefuelValidator(ExpenseInfoValidator infoValidator,
                           CarIdValidator carIdValidator) {
        this.infoValidator = infoValidator;
        this.carIdValidator = carIdValidator;
    }

    @Override
    public void validate(@NotNull RefuelDto refuel) {

        if (refuel.getVolume() == null || !minValue(refuel.getVolume(), 0.01)) {
            throw new ValidationException("Refuel volume is missing");
        }

        if (refuel.getRefuelInfo() == null) {
            throw new ValidationException("Refuel info is missing");
        }

        carIdValidator.validate(refuel.getCarId());

        infoValidator.validate(refuel.getRefuelInfo());
    }
}
