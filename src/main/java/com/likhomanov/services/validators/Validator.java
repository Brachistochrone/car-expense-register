package com.likhomanov.services.validators;

public interface Validator<T> {

    String ONLY_NUMBERS = "[0-9]+";
    String ONLY_LETTERS = "[a-zA-Z]+";
    String ONLY_LETTERS_AND_SPACE = "[a-zA-Z ]+";
    String ONLY_NUMBERS_AND_LETTERS = "[0-9a-zA-Z]+";
    String ONLY_NUMBERS_LETTERS_AND_SPACE = "[0-9a-zA-Z ]+";

    void validate(T t);
}
