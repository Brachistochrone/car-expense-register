package com.likhomanov.services.validators;

import com.likhomanov.exceptions.ValidationException;
import org.springframework.stereotype.Component;

import java.time.Year;

@Component
public class YearValidator implements Validator<Year> {

    @Override
    public void validate(Year year) {
        if (year == null) {
            throw new ValidationException("Year is missing");
        }

        if (year.getValue() < 1900 || year.getValue() > 2100) {
            throw new ValidationException("Year is out of range");
        }
    }
}
