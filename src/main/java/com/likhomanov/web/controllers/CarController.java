package com.likhomanov.web.controllers;

import com.likhomanov.services.CarService;
import com.likhomanov.web.dto.CarDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/cars")
public class CarController {

    private final CarService carService;

    public CarController(CarService carService) {
        this.carService = carService;
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public CarDto addNewCar(@RequestBody CarDto newCar) {
        return carService.addNewCar(newCar);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public Page<CarDto> getAllCars(Pageable pageable) {
        return carService.getAllCars(pageable);
    }
}
