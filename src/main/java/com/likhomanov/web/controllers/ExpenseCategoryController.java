package com.likhomanov.web.controllers;

import com.likhomanov.services.ExpenseCategoryService;
import com.likhomanov.web.dto.ExpenseCategoryDto;
import com.likhomanov.web.dto.ListOfExpenseCategoriesDto;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/expense_categories")
public class ExpenseCategoryController {

    private final ExpenseCategoryService expenseCategoryService;

    public ExpenseCategoryController(ExpenseCategoryService expenseCategoryService) {
        this.expenseCategoryService = expenseCategoryService;
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ExpenseCategoryDto createNewCategory(@RequestBody ExpenseCategoryDto newExpenseCategory) {
        return expenseCategoryService.addNewCategory(newExpenseCategory);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ListOfExpenseCategoriesDto getAllAvailableCategories() {
        return expenseCategoryService.getAllCategories();
    }
}
