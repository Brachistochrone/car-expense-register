package com.likhomanov.web.controllers;

import com.likhomanov.services.RefuelService;
import com.likhomanov.web.dto.ConsumedGasVolumeDto;
import com.likhomanov.web.dto.RefuelDto;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/refuels")
public class RefuelController {

    private final RefuelService refuelService;

    public RefuelController(RefuelService refuelService) {
        this.refuelService = refuelService;
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public RefuelDto addRefuel(@RequestBody RefuelDto newRefuel) {
        return refuelService.createNewRefuel(newRefuel);
    }

    @GetMapping(params = {"car_id", "start_date", "end_date"},
                produces = APPLICATION_JSON_VALUE)
    public ConsumedGasVolumeDto getConsumedGasByCarForSomePeriod(@RequestParam("car_id") Long carId,
                                                                 @RequestParam("start_date") String startDate,
                                                                 @RequestParam("end_date") String endDate) {
        return refuelService.getConsumedVolumeOfGasByCarIdForSomePeriod(carId, startDate, endDate);
    }
}
