package com.likhomanov.web.controllers;

import com.likhomanov.enums.Currency;
import com.likhomanov.services.ReplacedPartService;
import com.likhomanov.web.dto.NewReplacedPartDto;
import com.likhomanov.web.dto.ReplacedPartDto;
import com.likhomanov.web.dto.SpentMoneyDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.time.Year;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/replaced_parts")
public class ReplacedPartController {

    private final ReplacedPartService replacedPartService;

    public ReplacedPartController(ReplacedPartService replacedPartService) {
        this.replacedPartService = replacedPartService;
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public NewReplacedPartDto addReplacedPart(@RequestBody NewReplacedPartDto newReplacedPart) {
        return replacedPartService.createNewReplacedPart(newReplacedPart);
    }

    @GetMapping(value = "/spent_money",
                params = {"car_id", "year", "currency"},
                produces = APPLICATION_JSON_VALUE)
    public SpentMoneyDto getMoneySpentOnSparePartsForCar(@RequestParam("car_id") Long carId,
                                                         @RequestParam(value = "year", required = false) Year year,
                                                         @RequestParam("currency") Currency currency) {
        if(year == null) {
            return replacedPartService.getMoneySpentOnSparePartsByCarId(carId, currency);
        }

        return replacedPartService.getMoneySpentOnSparePartsByCarIdForYear(carId, year, currency);
    }

    @GetMapping(value = "/{car_id}", produces = APPLICATION_JSON_VALUE)
    public Page<ReplacedPartDto> getSparePartsForCar(@PathVariable("car_id") Long carId, Pageable pageable) {
        return replacedPartService.getReplacedPartsByCarId(carId, pageable);
    }
}
