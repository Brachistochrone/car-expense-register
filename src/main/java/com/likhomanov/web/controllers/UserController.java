package com.likhomanov.web.controllers;

import com.likhomanov.services.UserService;
import com.likhomanov.web.dto.NewUserDto;
import com.likhomanov.web.dto.UserDto;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public UserDto createNewUser(@RequestBody NewUserDto newUser) {
        return userService.saveNewUser(newUser);
    }
}
