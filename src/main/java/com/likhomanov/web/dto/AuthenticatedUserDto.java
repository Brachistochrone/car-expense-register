package com.likhomanov.web.dto;

import com.likhomanov.enums.Role;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class AuthenticatedUserDto extends EmailPasswordHolderDto {

    private Set<Role> roles;
}
