package com.likhomanov.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CarDto {

    private Long id;
    private CarInfoDto carInfo;
}
