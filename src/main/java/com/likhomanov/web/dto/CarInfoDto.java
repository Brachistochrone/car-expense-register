package com.likhomanov.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CarInfoDto {

    private String vinCode;
    private String model;
    private String color;
    private String body;
    private String year;
}
