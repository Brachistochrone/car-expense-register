package com.likhomanov.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmailPasswordHolderDto {

    private String email;
    private String password;
}
