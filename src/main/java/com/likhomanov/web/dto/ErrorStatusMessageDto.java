package com.likhomanov.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorStatusMessageDto {

    private String timestamp;
    private Integer status;
    private String error;
    private String message;
    private String path;
}
