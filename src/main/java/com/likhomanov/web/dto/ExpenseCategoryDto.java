package com.likhomanov.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpenseCategoryDto {

    private Long id;
    private String expenseType;
}
