package com.likhomanov.web.dto;

import com.likhomanov.enums.Currency;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ExpenseInfoDto {

    private ExpenseCategoryDto expenseCategory;
    private Date date;
    private Double sum;
    private Currency currency;
    private String companyName;
    private String companyAddress;
    private String description;
}
