package com.likhomanov.web.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ListOfExpenseCategoriesDto {

    private List<ExpenseCategoryDto> expenseCategories = new ArrayList<>();

    public void addExpenseCategory(ExpenseCategoryDto category) {
        expenseCategories.add(category);
    }
}
