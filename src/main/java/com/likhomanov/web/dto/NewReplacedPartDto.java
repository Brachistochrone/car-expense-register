package com.likhomanov.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewReplacedPartDto extends ReplacedPartDto {

    private Long carId;
}
