package com.likhomanov.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RefuelDto {

    private Double volume;
    private ExpenseInfoDto refuelInfo;
    private Long carId;
}
