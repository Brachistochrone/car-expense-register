package com.likhomanov.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReplacedPartDto {

    private String serialNumber;
    private String name;
    private ExpenseInfoDto replacedPartInfo;
}
