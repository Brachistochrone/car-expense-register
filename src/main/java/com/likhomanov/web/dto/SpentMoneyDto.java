package com.likhomanov.web.dto;

import com.likhomanov.enums.Currency;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SpentMoneyDto {

    private Double spentMoney;
    private Currency currency;
}
