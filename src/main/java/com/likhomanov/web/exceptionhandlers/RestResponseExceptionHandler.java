package com.likhomanov.web.exceptionhandlers;

import com.likhomanov.exceptions.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.*;

@ControllerAdvice
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = ValidationException.class)
    protected ResponseEntity<Object> handleValidationException(ValidationException ex, WebRequest request) {
        return handleException(ex, request, BAD_REQUEST);
    }

    @ExceptionHandler(value = NotUniqueEntityException.class)
    protected ResponseEntity<Object> handleNotUniqueEntityException(NotUniqueEntityException ex, WebRequest request) {
        return handleException(ex, request, BAD_REQUEST);
    }

    @ExceptionHandler(value = HttpRequestException.class)
    protected ResponseEntity<Object> handleHttpRequestException(HttpRequestException ex, WebRequest request) {
        return handleException(ex, request, SERVICE_UNAVAILABLE);
    }

    @ExceptionHandler(value = EntityNotFoundException.class)
    protected ResponseEntity<Object> handleEntityNotFoundException(EntityNotFoundException ex, WebRequest request) {
        return handleException(ex, request, BAD_REQUEST);
    }

    @ExceptionHandler(value = RoleAppointerException.class)
    protected ResponseEntity<Object> handleRoleAppointerException(RoleAppointerException ex, WebRequest request) {
        return handleException(ex, request, INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<Object> handleException(RuntimeException ex, WebRequest request, HttpStatus statusCode) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), statusCode, request);
    }
}
