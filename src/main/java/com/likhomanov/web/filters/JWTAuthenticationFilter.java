package com.likhomanov.web.filters;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.likhomanov.exceptions.UserAuthenticationException;
import com.likhomanov.exceptions.ValidationException;
import com.likhomanov.services.mappers.UserMapper;
import com.likhomanov.services.validators.AuthenticatedUserValidator;
import com.likhomanov.services.validators.LoginUserValidator;
import com.likhomanov.web.dto.AuthenticatedUserDto;
import com.likhomanov.web.dto.LoginUserDto;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;

import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter implements StatusMessenger {

    private final AuthenticationManager authenticationManager;
    private final LoginUserValidator loginUserValidator;
    private final AuthenticatedUserValidator authenticatedUserValidator;
    private final UserMapper mapper;
    private final ObjectMapper objectMapper;
    private final String secret;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager,
                                   LoginUserValidator loginUserValidator,
                                   AuthenticatedUserValidator authenticatedUserValidator,
                                   UserMapper mapper,
                                   String secret) {
        this.authenticationManager = authenticationManager;
        this.loginUserValidator = loginUserValidator;
        this.authenticatedUserValidator = authenticatedUserValidator;
        this.mapper = mapper;
        this.secret = secret;
        objectMapper = new ObjectMapper();
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException {
        try (ServletInputStream inStream = request.getInputStream()){
            final LoginUserDto loginUserDto = objectMapper.readValue(inStream, LoginUserDto.class);

            loginUserValidator.validate(loginUserDto);

            final String email = loginUserDto.getEmail();
            final String password = loginUserDto.getPassword();

            Authentication authentication = new UsernamePasswordAuthenticationToken(email,
                                                                                    password,
                                                                                    Collections.emptyList());
            return authenticationManager.authenticate(authentication);
        } catch (IOException | ValidationException e) {
            throw new UserAuthenticationException("User authentication failed", e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {
        Object principal = authResult.getPrincipal();

        if (!(principal instanceof User)) {
            sendStatusMessage(response,
                              INTERNAL_SERVER_ERROR,
                              "User authentication failed",
                              "/login",
                              objectMapper);
            throw new UserAuthenticationException("User authentication failed");
        }

        User user = (User) principal;

        try {
            AuthenticatedUserDto authenticatedUser = mapper.springUserToAuthenticatedUserDto(user);

            authenticatedUserValidator.validate(authenticatedUser);

            final String subject = objectMapper.writeValueAsString(authenticatedUser);
            final Date expiresAt = new Date(System.currentTimeMillis() + 600_000);
            final Algorithm algorithm =Algorithm.HMAC512(secret);
            final String token = JWT.create().withSubject(subject).withExpiresAt(expiresAt).sign(algorithm);

            response.addHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.AUTHORIZATION);
            response.addHeader(HttpHeaders.AUTHORIZATION, token);
        } catch (ValidationException e) {
            sendStatusMessage(response,
                              INTERNAL_SERVER_ERROR,
                              "User authentication failed",
                              "/login",
                              objectMapper);
            throw new UserAuthenticationException("User authentication failed");
        }
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request,
                                              HttpServletResponse response,
                                              AuthenticationException failed) throws IOException, ServletException {
        sendStatusMessage(response, FORBIDDEN, failed.getMessage(), "/login", objectMapper);
    }
}
