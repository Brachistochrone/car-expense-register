package com.likhomanov.web.filters;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.likhomanov.exceptions.UserAuthorizationException;
import com.likhomanov.exceptions.ValidationException;
import com.likhomanov.services.mappers.UserRoleMapper;
import com.likhomanov.services.validators.AuthenticatedUserValidator;
import com.likhomanov.web.dto.AuthenticatedUserDto;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter implements StatusMessenger {

    private final UserRoleMapper mapper;
    private final ObjectMapper objectMapper;
    private final AuthenticatedUserValidator authenticatedUserValidator;
    private final String secret;

    public JWTAuthorizationFilter(AuthenticationManager authenticationManager,
                                  UserRoleMapper mapper,
                                  AuthenticatedUserValidator authenticatedUserValidator,
                                  String secret) {
        super(authenticationManager);
        this.mapper = mapper;
        this.authenticatedUserValidator = authenticatedUserValidator;
        this.secret = secret;
        objectMapper = new ObjectMapper();
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {
        try {
            String token = request.getHeader(HttpHeaders.AUTHORIZATION);

            if (token != null) {
                Authentication authentication = decipherToken(token);
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }

            chain.doFilter(request, response);
        } catch (UserAuthorizationException | IOException e) {
            sendStatusMessage(response, HttpStatus.FORBIDDEN, e.getMessage(), request.getRequestURI(), objectMapper);
        }
    }

    private Authentication decipherToken(@NotNull String token) throws UserAuthorizationException {
        try {
            final String subject = JWT.require(Algorithm.HMAC512(secret))
                                      .build()
                                      .verify(token)
                                      .getSubject();

            AuthenticatedUserDto authenticatedUser = objectMapper.readValue(subject, AuthenticatedUserDto.class);
            if (authenticatedUser == null) return null;

            authenticatedUserValidator.validate(authenticatedUser);

            return new UsernamePasswordAuthenticationToken(authenticatedUser,
                                                           null,
                                                           mapper.rolesToAuthorities(authenticatedUser.getRoles()));
        } catch (JsonProcessingException | JWTVerificationException | ValidationException e) {
            throw new UserAuthorizationException("User authorization failed", e);
        }
    }
}
