package com.likhomanov.web.filters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.likhomanov.web.dto.ErrorStatusMessageDto;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;

public interface StatusMessenger {

    default void sendStatusMessage(HttpServletResponse response,
                                   HttpStatus code,
                                   String message,
                                   String path,
                                   ObjectMapper objectMapper) throws IOException {
        ErrorStatusMessageDto errorDto = new ErrorStatusMessageDto();
        errorDto.setTimestamp(Calendar.getInstance().getTime().toString());
        errorDto.setStatus(code.value());
        errorDto.setError(code.getReasonPhrase());
        errorDto.setMessage(message);
        errorDto.setPath(path);

        final String jsonBody = objectMapper.writeValueAsString(errorDto);

        try (PrintWriter writer = response.getWriter()){
            response.setStatus(code.value());
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            writer.print(jsonBody);
            writer.flush();
        }
    }
}
