insert into users (email, password)
values ('admin@gmail.com', '$2a$10$ZPaG.hTbCZjcxagNHXAvMecdGjili3rAsDAuVNYB4RmS8UeteSf0G');

insert into user_roles (role)
values ('ROLE_ADMIN');

with admin_id as (select currval('users_id_seq')), admin_role_id as (select currval('user_roles_id_seq'))
insert into users_roles_mapping (user_id, role_id)
values ((select * from admin_id), (select * from admin_role_id));