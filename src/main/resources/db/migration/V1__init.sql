create table if not exists users
(
    id              serial primary key,
    email           varchar(256) unique not null,
    password        varchar(256) not null
);

create table if not exists user_roles
(
    id              serial primary key,
    role            varchar(128) not null
);

create table if not exists users_roles_mapping
(
    userId          bigint,
    roleId          bigint,

    constraint "userId_fkey" foreign key (userId) references users (id),
    constraint "roleId_fkey" foreign key (roleId) references user_roles (id)
)