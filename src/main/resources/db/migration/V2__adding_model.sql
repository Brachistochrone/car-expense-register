create table if not exists expense_categories
(
    id                      serial primary key,
    expenseType             varchar(128) unique not null
);

create table if not exists expense_info
(
    id                      serial primary key,
    expenseCategoryId       bigint not null,
    date                    date not null,
    sum                     numeric(10, 4) not null,
    currency                varchar(32) not null,
    companyName             varchar(256),
    companyAddress          varchar(512),
    description             text,
    constraint "expenseCategoryId_fkey" foreign key (expenseCategoryId) references expense_categories (id)
);

create table if not exists car_info
(
    id                      serial primary key,
    vinCode                 varchar(128) unique not null,
    model                   varchar(128) not null,
    color                   varchar(128) not null,
    body                    varchar(128) not null,
    year                    varchar(128) not null
);

create table if not exists cars
(
    id                      serial primary key,
    carInfoId               bigint not null,
    constraint "carInfoId_fkey" foreign key (carInfoId) references car_info (id)
);

create table if not exists replaced_parts
(
    id                      serial primary key,
    serialNumber            varchar(128) unique not null,
    name                    varchar(256) not null,
    expenseInfoId           bigint not null,
    carId                   bigint not null,
    constraint "expenseInfoId_fkey" foreign key (expenseInfoId) references expense_info (id),
    constraint "carId_fkey" foreign key (carId) references cars (id)
);

create table if not exists refuels
(
    id                      serial primary key,
    volume                  numeric(10, 4) not null,
    expenseInfoId           bigint not null,
    carId                   bigint not null,
    constraint "expenseInfoId_fkey" foreign key (expenseInfoId) references expense_info (id),
    constraint "carId_fkey" foreign key (carId) references cars (id)
);