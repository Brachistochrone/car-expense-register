alter table users_roles_mapping
rename column userid to user_id;

alter table users_roles_mapping
rename column roleid to role_id;