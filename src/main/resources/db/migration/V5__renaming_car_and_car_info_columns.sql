alter table cars
rename column carinfoid to car_info_id;

alter table car_info
rename column vincode to vin_code;