alter table refuels
rename column expenseinfoid to expense_info_id;

alter table expense_info
rename column expensecategoryid to expense_category_id;

alter table expense_info
rename column companyname to company_name;

alter table expense_info
rename column companyaddress to company_address;