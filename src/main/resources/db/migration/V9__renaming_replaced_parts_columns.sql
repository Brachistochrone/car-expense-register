alter table replaced_parts
rename column carid to car_id;

alter table replaced_parts
rename column serialnumber to serial_number;

alter table replaced_parts
rename column expenseinfoid to expense_info_id;