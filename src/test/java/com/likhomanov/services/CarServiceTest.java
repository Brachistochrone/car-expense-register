package com.likhomanov.services;

import com.likhomanov.dao.entities.CarEntity;
import com.likhomanov.dao.repositories.CarDao;
import com.likhomanov.model.Car;
import com.likhomanov.model.CarModel;
import com.likhomanov.services.mappers.CarMapper;
import com.likhomanov.services.validators.CarValidator;
import com.likhomanov.web.dto.CarDto;
import com.likhomanov.web.dto.CarInfoDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CarServiceTest {

    @Mock
    private CarDao carDao;
    @Mock
    private CarMapper mapper;
    @Mock
    private CarValidator carValidator;
    @InjectMocks
    private CarService carService;
    private final CarEntity carOneEntity = new CarEntity();
    private final CarEntity carTwoEntity = new CarEntity();
    private final CarDto carOneDto = new CarDto();
    private final CarDto carTwoDto = new CarDto();
    private final CarInfoDto carOneInfoDto = new CarInfoDto();
    private final CarInfoDto carTwoInfoDto = new CarInfoDto();

    @BeforeEach
    public void init() {
        carOneInfoDto.setVinCode("12345");
        carOneInfoDto.setBody("Sedan");
        carOneInfoDto.setModel("Model X");
        carOneInfoDto.setColor("Red");
        carOneInfoDto.setYear("2010");

        carTwoInfoDto.setVinCode("56789");
        carTwoInfoDto.setBody("SUV");
        carTwoInfoDto.setModel("Model Y");
        carTwoInfoDto.setColor("Blue");
        carTwoInfoDto.setYear("2015");

        carOneDto.setId(1L);
        carOneDto.setCarInfo(carOneInfoDto);

        carTwoDto.setId(2L);
        carTwoDto.setCarInfo(carTwoInfoDto);
    }

    @Test
    public void newCarWillBeAddedToDbWhenGivenCarDtoWithUniqueVinCode() {
        doNothing().when(carValidator).validate(carOneDto);
        doReturn(new CarModel()).when(mapper).dtoToModel(carOneDto);
        doReturn(carOneEntity).when(mapper).modelToEntity(any(Car.class));
        doReturn(carOneEntity).when(carDao).save(carOneEntity);
        doReturn(carOneDto).when(mapper).modelToDto(carOneEntity);

        CarDto result = carService.addNewCar(carOneDto);

        verify(carValidator).validate(carOneDto);
        verify(mapper).dtoToModel(carOneDto);
        verify(mapper).modelToEntity(any(Car.class));
        verify(carDao).save(carOneEntity);
        verify(mapper).modelToDto(carOneEntity);

        assertEquals(carOneDto.getId(), result.getId());
        assertEquals(carOneDto.getCarInfo().getVinCode(), result.getCarInfo().getVinCode());
        assertEquals(carOneDto.getCarInfo().getBody(), result.getCarInfo().getBody());
        assertEquals(carOneDto.getCarInfo().getColor(), result.getCarInfo().getColor());
        assertEquals(carOneDto.getCarInfo().getModel(), result.getCarInfo().getModel());
        assertEquals(carOneDto.getCarInfo().getYear(), result.getCarInfo().getYear());
    }

    @Test
    public void pageOfCarDtosWillBeReturnedWhenGivenPageable() {
        Pageable pageable = new PageableImpl();
        Page<? extends Car> cars = new PageImpl<>(Arrays.asList(carOneEntity, carTwoEntity));
        Page<CarDto> carDtos = new PageImpl<>(Arrays.asList(carOneDto, carTwoDto));

        doReturn(cars).when(carDao).findAll(any(Pageable.class));
        doReturn(carDtos).when(mapper).pageOfCarsToDto(cars);

        Page<CarDto> result = carService.getAllCars(pageable);

        verify(carDao).findAll(any(Pageable.class));
        verify(mapper).pageOfCarsToDto(cars);

        assertEquals(2, result.getContent().size());
        assertEquals(carOneDto.getId(),
                     result.getContent().get(0).getId());
        assertEquals(carOneDto.getCarInfo().getVinCode(),
                     result.getContent().get(0).getCarInfo().getVinCode());
        assertEquals(carOneDto.getCarInfo().getBody(),
                     result.getContent().get(0).getCarInfo().getBody());
        assertEquals(carOneDto.getCarInfo().getModel(),
                     result.getContent().get(0).getCarInfo().getModel());
        assertEquals(carOneDto.getCarInfo().getColor(),
                     result.getContent().get(0).getCarInfo().getColor());
        assertEquals(carOneDto.getCarInfo().getYear(),
                     result.getContent().get(0).getCarInfo().getYear());
        assertEquals(carTwoDto.getId(),
                     result.getContent().get(1).getId());
        assertEquals(carTwoDto.getCarInfo().getVinCode(),
                     result.getContent().get(1).getCarInfo().getVinCode());
        assertEquals(carTwoDto.getCarInfo().getBody(),
                     result.getContent().get(1).getCarInfo().getBody());
        assertEquals(carTwoDto.getCarInfo().getModel(),
                     result.getContent().get(1).getCarInfo().getModel());
        assertEquals(carTwoDto.getCarInfo().getColor(),
                     result.getContent().get(1).getCarInfo().getColor());
        assertEquals(carTwoDto.getCarInfo().getYear(),
                     result.getContent().get(1).getCarInfo().getYear());
        assertEquals(2, result.getTotalElements());
        assertNotNull(result.getPageable());
    }

    @Test
    public void emptyPageOfCarDtosWillBeReturnedWhenNoCarsInDb() {
        Pageable pageable = new PageableImpl();
        Page<? extends Car> cars = new PageImpl<>(Collections.emptyList());
        Page<CarDto> carDtos = new PageImpl<>(Collections.emptyList());

        doReturn(cars).when(carDao).findAll(any(Pageable.class));
        doReturn(carDtos).when(mapper).pageOfCarsToDto(cars);

        Page<CarDto> result = carService.getAllCars(pageable);

        verify(carDao).findAll(any(Pageable.class));
        verify(mapper).pageOfCarsToDto(cars);

        assertTrue(result.getContent().isEmpty());
        assertEquals(0, result.getTotalElements());
        assertNotNull(result.getPageable());
    }

    private static final class PageableImpl implements Pageable {

        @Override
        public int getPageNumber() {
            return 0;
        }

        @Override
        public int getPageSize() {
            return 2;
        }

        @Override
        public long getOffset() {
            return 0;
        }

        @Override
        public Sort getSort() {
            return null;
        }

        @Override
        public Pageable next() {
            return null;
        }

        @Override
        public Pageable previousOrFirst() {
            return null;
        }

        @Override
        public Pageable first() {
            return null;
        }

        @Override
        public boolean hasPrevious() {
            return false;
        }
    }
}