package com.likhomanov.services;

import com.likhomanov.exceptions.HttpRequestException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class CurrencyCodeNormalizerTest {

    @InjectMocks
    private CurrencyCodeNormalizer currencyCodeNormalizer;

    @Test
    public void rurWillBeReplacedWithRubInGivenString() {
        String testString = "This is just some string containing RUR";

        testString = currencyCodeNormalizer.normalizeRub(testString);

        assertEquals("This is just some string containing RUB", testString);
    }

    @Test
    public void httpRequestExceptionWillBeThrownWhenGivenNullString() {
        assertThrows(HttpRequestException.class, () -> currencyCodeNormalizer.normalizeRub(null));
    }

    @Test
    public void httpRequestExceptionWillBeThrownWhenGivenEmptyString() {
        assertThrows(HttpRequestException.class, () -> currencyCodeNormalizer.normalizeRub(""));
    }
}