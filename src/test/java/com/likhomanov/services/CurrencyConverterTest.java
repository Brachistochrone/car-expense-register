package com.likhomanov.services;

import com.likhomanov.dao.queryresults.MoneyByCurrencyResult;
import com.likhomanov.enums.Currency;
import com.likhomanov.model.CurrencyExchangeRate;
import com.likhomanov.model.SpentMoney;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashMap;

import static com.likhomanov.enums.Currency.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CurrencyConverterTest {

    @Mock
    private CurrencyExchangeRateFetcher exchangeRateFetcher;
    @Spy
    private ArrayList<MoneyByCurrencyResult> moneyList;
    @Mock
    private MoneyByCurrencyResult moneyUAH;
    @Mock
    private MoneyByCurrencyResult moneyUSD;
    @Mock
    private MoneyByCurrencyResult moneyEUR;
    @Mock
    private MoneyByCurrencyResult moneyRUB;
    private CurrencyExchangeRate rateUSD;
    private CurrencyExchangeRate rateEUR;
    private CurrencyExchangeRate rateRUB;
    @Spy
    private HashMap<Currency, CurrencyExchangeRate> exchangeRates;
    @InjectMocks
    private CurrencyConverter currencyConverter;

    @BeforeEach
    public void init() {
        if (rateUSD == null) {
            rateUSD = new CurrencyExchangeRate();
            rateUSD.setCurrency(USD);
            rateUSD.setBaseCurrency(UAH);
            rateUSD.setBuyRate(27.0);
            rateUSD.setSaleRate(28.0);
        }

        if (rateEUR == null) {
            rateEUR = new CurrencyExchangeRate();
            rateEUR.setCurrency(EUR);
            rateEUR.setBaseCurrency(UAH);
            rateEUR.setBuyRate(32.0);
            rateEUR.setSaleRate(33.0);
        }

        if (rateRUB == null) {
            rateRUB = new CurrencyExchangeRate();
            rateRUB.setCurrency(RUB);
            rateRUB.setBaseCurrency(UAH);
            rateRUB.setBuyRate(0.36);
            rateRUB.setSaleRate(0.38);
        }

        if (exchangeRates.isEmpty()) {
            exchangeRates.put(USD, rateUSD);
            exchangeRates.put(EUR, rateEUR);
            exchangeRates.put(RUB, rateRUB);
        }

        moneyList.clear();
    }

    @Test
    public void zeroUAHWillBeReturnedWhenGivenEmptyListOfMoney() {
        doReturn(exchangeRates).when(exchangeRateFetcher).obtainCurrencyExchangeRates();

        SpentMoney spentMoney = currencyConverter.convertCurrency(UAH, moneyList);

        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();
        verify(moneyList).stream();
        verify(exchangeRates, never()).get(any(Currency.class));

        assertEquals(0.0, spentMoney.getSpentMoney());
        assertEquals(UAH, spentMoney.getCurrency());
    }

    @Test
    public void zeroUSDWillBeReturnedWhenGivenEmptyListOfMoney() {
        doReturn(exchangeRates).when(exchangeRateFetcher).obtainCurrencyExchangeRates();

        SpentMoney spentMoney = currencyConverter.convertCurrency(USD, moneyList);

        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();
        verify(moneyList).stream();
        verify(exchangeRates).get(any(Currency.class));

        assertEquals(0.0, spentMoney.getSpentMoney());
        assertEquals(USD, spentMoney.getCurrency());
    }

    @Test
    public void spentMoneyInUahWillBeReturnedWhenGivenUahMoneyAndUahAsDesiredCurrency() {
        moneyList.add(moneyUAH);

        doReturn(exchangeRates).when(exchangeRateFetcher).obtainCurrencyExchangeRates();
        doReturn(UAH).when(moneyUAH).getCurrency();
        doReturn(10.0).when(moneyUAH).getDough();

        SpentMoney spentMoney = currencyConverter.convertCurrency(UAH, moneyList);

        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();
        verify(moneyList).stream();
        verify(moneyUAH).getCurrency();
        verify(moneyUAH).getDough();
        verify(exchangeRates, never()).get(any(Currency.class));

        assertEquals(UAH, spentMoney.getCurrency());
        assertEquals(10.0, spentMoney.getSpentMoney());
    }

    @Test
    public void spentMoneyInUsdWillBeReturnedWhenGivenUahMoneyAndUsdAsDesiredCurrency() {
        moneyList.add(moneyUAH);

        doReturn(exchangeRates).when(exchangeRateFetcher).obtainCurrencyExchangeRates();
        doReturn(UAH).when(moneyUAH).getCurrency();
        doReturn(2800.0).when(moneyUAH).getDough();

        SpentMoney spentMoney = currencyConverter.convertCurrency(USD, moneyList);

        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();
        verify(moneyList).stream();
        verify(moneyUAH).getCurrency();
        verify(moneyUAH).getDough();
        verify(exchangeRates).get(any(Currency.class));

        assertEquals(USD, spentMoney.getCurrency());
        assertEquals(100.0, spentMoney.getSpentMoney());
    }

    @Test
    public void spentMoneyInEurWillBeReturnedWhenGivenUahMoneyAndEurAsDesiredCurrency() {
        moneyList.add(moneyUAH);

        doReturn(exchangeRates).when(exchangeRateFetcher).obtainCurrencyExchangeRates();
        doReturn(UAH).when(moneyUAH).getCurrency();
        doReturn(3300.0).when(moneyUAH).getDough();

        SpentMoney spentMoney = currencyConverter.convertCurrency(EUR, moneyList);

        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();
        verify(moneyList).stream();
        verify(moneyUAH).getCurrency();
        verify(moneyUAH).getDough();
        verify(exchangeRates).get(any(Currency.class));

        assertEquals(EUR, spentMoney.getCurrency());
        assertEquals(100.0, spentMoney.getSpentMoney());
    }

    @Test
    public void spentMoneyInRubWillBeReturnedWhenGivenUahMoneyAndRubAsDesiredCurrency() {
        moneyList.add(moneyUAH);

        doReturn(exchangeRates).when(exchangeRateFetcher).obtainCurrencyExchangeRates();
        doReturn(UAH).when(moneyUAH).getCurrency();
        doReturn(38.0).when(moneyUAH).getDough();

        SpentMoney spentMoney = currencyConverter.convertCurrency(RUB, moneyList);

        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();
        verify(moneyList).stream();
        verify(moneyUAH).getCurrency();
        verify(moneyUAH).getDough();
        verify(exchangeRates).get(any(Currency.class));

        assertEquals(RUB, spentMoney.getCurrency());
        assertEquals(100.0, spentMoney.getSpentMoney());
    }

    @Test
    public void spentMoneyInUahWillBeReturnedWhenGivenMoneyListAndUahAsDesiredCurrency() {
        moneyList.add(moneyUAH);
        moneyList.add(moneyUSD);
        moneyList.add(moneyEUR);
        moneyList.add(moneyRUB);

        doReturn(exchangeRates).when(exchangeRateFetcher).obtainCurrencyExchangeRates();
        doReturn(UAH).when(moneyUAH).getCurrency();
        doReturn(USD).when(moneyUSD).getCurrency();
        doReturn(EUR).when(moneyEUR).getCurrency();
        doReturn(RUB).when(moneyRUB).getCurrency();
        doReturn(100.0).when(moneyUAH).getDough();
        doReturn(100.0).when(moneyUSD).getDough();
        doReturn(100.0).when(moneyEUR).getDough();
        doReturn(100.0).when(moneyRUB).getDough();

        SpentMoney spentMoney = currencyConverter.convertCurrency(UAH, moneyList);

        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();
        verify(moneyList).stream();
        verify(moneyUAH).getCurrency();
        verify(moneyUAH).getDough();
        verify(moneyUSD, times(2)).getCurrency();
        verify(moneyUSD).getDough();
        verify(moneyEUR, times(2)).getCurrency();
        verify(moneyEUR).getDough();
        verify(moneyRUB, times(2)).getCurrency();
        verify(moneyRUB).getDough();
        verify(exchangeRates).get(USD);
        verify(exchangeRates).get(EUR);
        verify(exchangeRates).get(RUB);

        assertEquals(UAH, spentMoney.getCurrency());
        assertEquals(6238.0, spentMoney.getSpentMoney());
    }

    @Test
    public void spentMoneyInUsdWillBeReturnedWhenGivenMoneyListAndUsdAsDesiredCurrency() {
        moneyList.add(moneyUAH);
        moneyList.add(moneyUSD);
        moneyList.add(moneyEUR);
        moneyList.add(moneyRUB);

        doReturn(exchangeRates).when(exchangeRateFetcher).obtainCurrencyExchangeRates();
        doReturn(UAH).when(moneyUAH).getCurrency();
        doReturn(USD).when(moneyUSD).getCurrency();
        doReturn(EUR).when(moneyEUR).getCurrency();
        doReturn(RUB).when(moneyRUB).getCurrency();
        doReturn(100.0).when(moneyUAH).getDough();
        doReturn(100.0).when(moneyUSD).getDough();
        doReturn(100.0).when(moneyEUR).getDough();
        doReturn(100.0).when(moneyRUB).getDough();

        SpentMoney spentMoney = currencyConverter.convertCurrency(USD, moneyList);

        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();
        verify(moneyList).stream();
        verify(moneyUAH).getCurrency();
        verify(moneyUAH).getDough();
        verify(moneyUSD, times(2)).getCurrency();
        verify(moneyUSD).getDough();
        verify(moneyEUR, times(2)).getCurrency();
        verify(moneyEUR).getDough();
        verify(moneyRUB, times(2)).getCurrency();
        verify(moneyRUB).getDough();
        verify(exchangeRates, times(2)).get(USD);
        verify(exchangeRates).get(EUR);
        verify(exchangeRates).get(RUB);

        assertEquals(USD, spentMoney.getCurrency());
        assertEquals(222.79, spentMoney.getSpentMoney());
    }

    @Test
    public void spentMoneyInEurWillBeReturnedWhenGivenMoneyListAndEurAsDesiredCurrency() {
        moneyList.add(moneyUAH);
        moneyList.add(moneyUSD);
        moneyList.add(moneyEUR);
        moneyList.add(moneyRUB);

        doReturn(exchangeRates).when(exchangeRateFetcher).obtainCurrencyExchangeRates();
        doReturn(UAH).when(moneyUAH).getCurrency();
        doReturn(USD).when(moneyUSD).getCurrency();
        doReturn(EUR).when(moneyEUR).getCurrency();
        doReturn(RUB).when(moneyRUB).getCurrency();
        doReturn(100.0).when(moneyUAH).getDough();
        doReturn(100.0).when(moneyUSD).getDough();
        doReturn(100.0).when(moneyEUR).getDough();
        doReturn(100.0).when(moneyRUB).getDough();

        SpentMoney spentMoney = currencyConverter.convertCurrency(EUR, moneyList);

        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();
        verify(moneyList).stream();
        verify(moneyUAH).getCurrency();
        verify(moneyUAH).getDough();
        verify(moneyUSD, times(2)).getCurrency();
        verify(moneyUSD).getDough();
        verify(moneyEUR, times(2)).getCurrency();
        verify(moneyEUR).getDough();
        verify(moneyRUB, times(2)).getCurrency();
        verify(moneyRUB).getDough();
        verify(exchangeRates, times(2)).get(EUR);
        verify(exchangeRates).get(USD);
        verify(exchangeRates).get(RUB);

        assertEquals(EUR, spentMoney.getCurrency());
        assertEquals(189.03, spentMoney.getSpentMoney());
    }

    @Test
    public void spentMoneyInRubWillBeReturnedWhenGivenMoneyListAndRubAsDesiredCurrency() {
        moneyList.add(moneyUAH);
        moneyList.add(moneyUSD);
        moneyList.add(moneyEUR);
        moneyList.add(moneyRUB);

        doReturn(exchangeRates).when(exchangeRateFetcher).obtainCurrencyExchangeRates();
        doReturn(UAH).when(moneyUAH).getCurrency();
        doReturn(USD).when(moneyUSD).getCurrency();
        doReturn(EUR).when(moneyEUR).getCurrency();
        doReturn(RUB).when(moneyRUB).getCurrency();
        doReturn(100.0).when(moneyUAH).getDough();
        doReturn(100.0).when(moneyUSD).getDough();
        doReturn(100.0).when(moneyEUR).getDough();
        doReturn(100.0).when(moneyRUB).getDough();

        SpentMoney spentMoney = currencyConverter.convertCurrency(RUB, moneyList);

        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();
        verify(moneyList).stream();
        verify(moneyUAH).getCurrency();
        verify(moneyUAH).getDough();
        verify(moneyUSD, times(2)).getCurrency();
        verify(moneyUSD).getDough();
        verify(moneyEUR, times(2)).getCurrency();
        verify(moneyEUR).getDough();
        verify(moneyRUB, times(2)).getCurrency();
        verify(moneyRUB).getDough();
        verify(exchangeRates, times(2)).get(RUB);
        verify(exchangeRates).get(USD);
        verify(exchangeRates).get(EUR);

        assertEquals(RUB, spentMoney.getCurrency());
        assertEquals(16415.79, spentMoney.getSpentMoney());
    }
}