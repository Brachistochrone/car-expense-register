package com.likhomanov.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.likhomanov.enums.Currency;
import com.likhomanov.exceptions.HttpRequestException;
import com.likhomanov.model.CurrencyExchangeRate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Map;

import static com.likhomanov.enums.Currency.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CurrencyExchangeRateFetcherTest {

    @Mock
    private CurrencyCodeNormalizer currencyNormalizer;
    @Mock
    private HttpClient httpClient;
    @Mock
    private ObjectMapper objectMapper;
    @Mock
    private HttpResponse<String> httpResponse;
    @Spy
    private ArrayList<CurrencyExchangeRate> exchangeRates;
    @Spy
    private CurrencyExchangeRate rateUSD;
    @Spy
    private CurrencyExchangeRate rateEUR;
    @Spy
    private CurrencyExchangeRate rateRUB;
    @InjectMocks
    private CurrencyExchangeRateFetcher exchangeRateFetcher;

    @BeforeEach
    public void init() {
        rateUSD.setCurrency(USD);
        rateUSD.setBaseCurrency(UAH);
        rateUSD.setBuyRate(27.0);
        rateUSD.setSaleRate(28.0);

        rateEUR.setCurrency(EUR);
        rateEUR.setBaseCurrency(UAH);
        rateEUR.setBuyRate(32.0);
        rateEUR.setSaleRate(33.0);

        rateRUB.setCurrency(RUB);
        rateRUB.setBaseCurrency(UAH);
        rateRUB.setBuyRate(0.36);
        rateRUB.setSaleRate(0.38);

        if (exchangeRates.isEmpty()) {
            exchangeRates.add(rateUSD);
            exchangeRates.add(rateEUR);
            exchangeRates.add(rateRUB);
        }

        ReflectionTestUtils.setField(exchangeRateFetcher, "httpClient", httpClient);
        ReflectionTestUtils.setField(exchangeRateFetcher, "objectMapper", objectMapper);
    }

    @Test
    public void mapOfExchangeCurrencyRatesWillBeReturnedWhenEverythingIsOk() throws IOException, InterruptedException {
        String jsonResponse = "Some Json response with RUR";
        String normalizedJsonResponse = "Some Json response with RUB";

        ReflectionTestUtils.setField(exchangeRateFetcher, "exchangeRateUrl", "http://testurl.com");

        doReturn(httpResponse).when(httpClient)
                              .send(any(HttpRequest.class), any(HttpResponse.BodyHandler.class));
        doReturn(jsonResponse).when(httpResponse).body();
        doReturn(normalizedJsonResponse).when(currencyNormalizer).normalizeRub(anyString());
        doReturn(exchangeRates).when(objectMapper).readValue(eq(normalizedJsonResponse), any(CollectionType.class));
        doReturn(TypeFactory.defaultInstance()).when(objectMapper).getTypeFactory();

        Map<Currency, CurrencyExchangeRate> currencyRatesMap = exchangeRateFetcher.obtainCurrencyExchangeRates();

        verify(httpClient).send(any(HttpRequest.class), any(HttpResponse.BodyHandler.class));
        verify(httpResponse).body();
        verify(currencyNormalizer).normalizeRub(anyString());
        verify(objectMapper).readValue(eq(normalizedJsonResponse), any(CollectionType.class));
        verify(objectMapper).getTypeFactory();
        verify(exchangeRates).stream();
        verify(rateUSD).getCurrency();
        verify(rateEUR).getCurrency();
        verify(rateRUB).getCurrency();

        assertEquals(3, currencyRatesMap.size());
        assertNotNull(currencyRatesMap.get(USD));
        assertNotNull(currencyRatesMap.get(EUR));
        assertNotNull(currencyRatesMap.get(RUB));
        assertEquals(rateUSD, currencyRatesMap.get(USD));
        assertEquals(rateEUR, currencyRatesMap.get(EUR));
        assertEquals(rateRUB, currencyRatesMap.get(RUB));
        assertEquals(USD, currencyRatesMap.get(USD).getCurrency());
        assertEquals(EUR, currencyRatesMap.get(EUR).getCurrency());
        assertEquals(RUB, currencyRatesMap.get(RUB).getCurrency());
        assertEquals(UAH, currencyRatesMap.get(USD).getBaseCurrency());
        assertEquals(UAH, currencyRatesMap.get(EUR).getBaseCurrency());
        assertEquals(UAH, currencyRatesMap.get(RUB).getBaseCurrency());
        assertEquals(27.0, currencyRatesMap.get(USD).getBuyRate());
        assertEquals(32.0, currencyRatesMap.get(EUR).getBuyRate());
        assertEquals(0.36, currencyRatesMap.get(RUB).getBuyRate());
        assertEquals(28.0, currencyRatesMap.get(USD).getSaleRate());
        assertEquals(33.0, currencyRatesMap.get(EUR).getSaleRate());
        assertEquals(0.38, currencyRatesMap.get(RUB).getSaleRate());
    }

    @Test
    public void httpRequestExceptionWillBeThrownWhenInvalidUriIsUsedToCreateHttpRequest() {
        ReflectionTestUtils.setField(exchangeRateFetcher, "exchangeRateUrl", "http://test_url.com");

        assertThrows(HttpRequestException.class, () -> exchangeRateFetcher.obtainCurrencyExchangeRates());
    }

    @Test
    public void httpRequestExceptionWillBeThrownWhenEmptyUriIsUsedToCreateHttpRequest() {
        ReflectionTestUtils.setField(exchangeRateFetcher, "exchangeRateUrl", "");

        assertThrows(HttpRequestException.class, () -> exchangeRateFetcher.obtainCurrencyExchangeRates());
    }

    @Test
    public void httpRequestExceptionWillBeThrownWhenHttpClientThrowsInterruptedException() throws IOException,
                                                                                                  InterruptedException {
        ReflectionTestUtils.setField(exchangeRateFetcher, "exchangeRateUrl", "http://testurl.com");

        doThrow(InterruptedException.class).when(httpClient).send(any(HttpRequest.class),
                                                                  any(HttpResponse.BodyHandler.class));

        assertThrows(HttpRequestException.class, () -> exchangeRateFetcher.obtainCurrencyExchangeRates());
    }

    @Test
    public void httpRequestExceptionWillBeThrownWhenHttpClientThrowsIOException() throws IOException,
                                                                                         InterruptedException {
        ReflectionTestUtils.setField(exchangeRateFetcher, "exchangeRateUrl", "http://testurl.com");

        doThrow(IOException.class).when(httpClient).send(any(HttpRequest.class),
                                                         any(HttpResponse.BodyHandler.class));

        assertThrows(HttpRequestException.class, () -> exchangeRateFetcher.obtainCurrencyExchangeRates());
    }

    @Test
    public void httpRequestExceptionWillBeThrownWhenObjectMapperThrowsJsonProcessingException() throws IOException,
                                                                                                       InterruptedException {
        ReflectionTestUtils.setField(exchangeRateFetcher, "exchangeRateUrl", "http://testurl.com");

        doReturn(httpResponse).when(httpClient)
                              .send(any(HttpRequest.class), any(HttpResponse.BodyHandler.class));
        doReturn("").when(httpResponse).body();
        doReturn("").when(currencyNormalizer).normalizeRub(anyString());
        doReturn(TypeFactory.defaultInstance()).when(objectMapper).getTypeFactory();
        doThrow(JsonProcessingException.class).when(objectMapper).readValue(anyString(), any(CollectionType.class));

        assertThrows(HttpRequestException.class, () -> exchangeRateFetcher.obtainCurrencyExchangeRates());
    }

    @Test
    public void httpRequestExceptionWillBeThrownWhenObjectMapperThrowsJsonMappingException() throws IOException,
                                                                                                    InterruptedException {
        ReflectionTestUtils.setField(exchangeRateFetcher, "exchangeRateUrl", "http://testurl.com");

        doReturn(httpResponse).when(httpClient)
                              .send(any(HttpRequest.class), any(HttpResponse.BodyHandler.class));
        doReturn("").when(httpResponse).body();
        doReturn("").when(currencyNormalizer).normalizeRub(anyString());
        doReturn(TypeFactory.defaultInstance()).when(objectMapper).getTypeFactory();
        doThrow(JsonMappingException.class).when(objectMapper).readValue(anyString(), any(CollectionType.class));

        assertThrows(HttpRequestException.class, () -> exchangeRateFetcher.obtainCurrencyExchangeRates());
    }
}