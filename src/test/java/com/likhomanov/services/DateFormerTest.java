package com.likhomanov.services;

import com.likhomanov.exceptions.DateFormerException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.Year;
import java.time.ZoneId;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class DateFormerTest {

    @InjectMocks
    private DateFormer dateFormer;

    @Test
    public void firstOfJanuaryOfTheYearDateWillBeReturnedWhenGivenYear() {
        Year year = Year.of(2020);

        Date yearHead = dateFormer.getYearHead(year);
        LocalDate localDate = yearHead.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        assertEquals(2020, localDate.getYear());
        assertEquals(1, localDate.getMonthValue());
        assertEquals(1, localDate.getDayOfMonth());
    }

    @Test
    public void thirtyFirstOfDecemberOfTheYearDateWillBeReturnedWhenGivenYear() {
        Year year = Year.of(2020);

        Date yearTail = dateFormer.getYearTail(year);
        LocalDate localDate = yearTail.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        assertEquals(2020, localDate.getYear());
        assertEquals(12, localDate.getMonthValue());
        assertEquals(31, localDate.getDayOfMonth());
    }

    @Test
    public void dateWillBeReturnedWhenGivenItsStringRepresentation() {
        String date = "2020-06-01";

        Date realDate = dateFormer.stringToDate(date);
        LocalDate localDate = realDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        assertEquals(2020, localDate.getYear());
        assertEquals(6, localDate.getMonthValue());
        assertEquals(1, localDate.getDayOfMonth());
    }

    @Test
    public void dateFormerExceptionWillBeThrownWhenGivenInvalidDateString() {
        String date = "2020-06-35";

        assertThrows(DateFormerException.class, () -> dateFormer.stringToDate(date));
    }
}