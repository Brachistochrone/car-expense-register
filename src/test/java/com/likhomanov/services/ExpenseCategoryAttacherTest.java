package com.likhomanov.services;

import com.likhomanov.dao.repositories.ExpenseCategoryDao;
import com.likhomanov.exceptions.EntityNotFoundException;
import com.likhomanov.model.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ExpenseCategoryAttacherTest {

    @Mock
    private ExpenseCategoryDao categoryDao;
    @InjectMocks
    private ExpenseCategoryAttacher categoryAttacher;

    @Test
    public void expenseWithExpenseCategoryWillBeReturnedWhenGivenNewRefuelInstanceWithCorrectExpenseType() {
        RefuelModel newRefuel = new RefuelModel();
        ExpenseInfoModel expenseInfo = new ExpenseInfoModel();
        ExpenseCategoryModel expenseCategory = new ExpenseCategoryModel();
        expenseCategory.setExpenseType("REFUEL");
        expenseInfo.setExpenseCategory(expenseCategory);
        newRefuel.setExpenseInfo(expenseInfo);

        doReturn(Optional.of(1L)).when(categoryDao).findIdByExpenseType("REFUEL");

        Refuel refuelWithCategory = categoryAttacher.attachExpenseCategory(newRefuel);

        verify(categoryDao).findIdByExpenseType("REFUEL");

        assertEquals(newRefuel, refuelWithCategory);
        assertEquals(expenseInfo, refuelWithCategory.getExpenseInfo());
        assertEquals(expenseCategory, refuelWithCategory.getExpenseInfo().getExpenseCategory());
        assertEquals("REFUEL", refuelWithCategory.getExpenseInfo().getExpenseCategory().getExpenseType());
        assertEquals(1L, refuelWithCategory.getExpenseInfo().getExpenseCategory().getId());
    }

    @Test
    public void entityNotFoundExceptionWillBeThrownWhenGivenNewRefuelInstanceWithInvalidExpenseType() {
        RefuelModel newRefuel = new RefuelModel();
        ExpenseInfoModel expenseInfo = new ExpenseInfoModel();
        ExpenseCategoryModel expenseCategory = new ExpenseCategoryModel();
        expenseCategory.setExpenseType("BRIBE");
        expenseInfo.setExpenseCategory(expenseCategory);
        newRefuel.setExpenseInfo(expenseInfo);

        doReturn(Optional.empty()).when(categoryDao).findIdByExpenseType("BRIBE");

        assertThrows(EntityNotFoundException.class, () -> categoryAttacher.attachExpenseCategory(newRefuel));
    }

    @Test
    public void expenseWithExpenseCategoryWillBeReturnedWhenGivenNewReplacedPartInstanceWithCorrectExpenseType() {
        ReplacedPartModel newReplacedPart = new ReplacedPartModel();
        ExpenseInfoModel expenseInfo = new ExpenseInfoModel();
        ExpenseCategoryModel expenseCategory = new ExpenseCategoryModel();
        expenseCategory.setExpenseType("REPAIR");
        expenseInfo.setExpenseCategory(expenseCategory);
        newReplacedPart.setExpenseInfo(expenseInfo);

        doReturn(Optional.of(2L)).when(categoryDao).findIdByExpenseType("REPAIR");

        ReplacedPart partWithCategory = categoryAttacher.attachExpenseCategory(newReplacedPart);

        verify(categoryDao).findIdByExpenseType("REPAIR");

        assertEquals(newReplacedPart, partWithCategory);
        assertEquals(expenseInfo, partWithCategory.getExpenseInfo());
        assertEquals(expenseCategory, partWithCategory.getExpenseInfo().getExpenseCategory());
        assertEquals("REPAIR", partWithCategory.getExpenseInfo().getExpenseCategory().getExpenseType());
        assertEquals(2L, partWithCategory.getExpenseInfo().getExpenseCategory().getId());
    }

    @Test
    public void entityNotFoundExceptionWillBeThrownWhenGivenNewReplacedPartInstanceWithInvalidExpenseType() {
        ReplacedPartModel newReplacedPart = new ReplacedPartModel();
        ExpenseInfoModel expenseInfo = new ExpenseInfoModel();
        ExpenseCategoryModel expenseCategory = new ExpenseCategoryModel();
        expenseCategory.setExpenseType("BRIBE");
        expenseInfo.setExpenseCategory(expenseCategory);
        newReplacedPart.setExpenseInfo(expenseInfo);

        doReturn(Optional.empty()).when(categoryDao).findIdByExpenseType("BRIBE");

        assertThrows(EntityNotFoundException.class, () -> categoryAttacher.attachExpenseCategory(newReplacedPart));
    }
}