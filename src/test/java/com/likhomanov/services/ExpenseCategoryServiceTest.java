package com.likhomanov.services;

import com.likhomanov.dao.entities.ExpenseCategoryEntity;
import com.likhomanov.dao.repositories.ExpenseCategoryDao;
import com.likhomanov.model.ExpenseCategoryModel;
import com.likhomanov.services.mappers.ExpenseCategoryMapper;
import com.likhomanov.services.validators.NewExpenseCategoryValidator;
import com.likhomanov.web.dto.ExpenseCategoryDto;
import com.likhomanov.web.dto.ListOfExpenseCategoriesDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ExpenseCategoryServiceTest {

    @Mock
    private ExpenseCategoryDao expenseCategoryDao;
    @Mock
    private ExpenseCategoryMapper mapper;
    @Mock
    private NewExpenseCategoryValidator categoryValidator;
    @InjectMocks
    private ExpenseCategoryService categoryService;

    @Test
    public void newExpenseCategoryWillBeAddedToDbWhenGivenExpenseCategoryDto() {
        ExpenseCategoryDto dto = new ExpenseCategoryDto();
        dto.setId(1L);
        dto.setExpenseType("PARKING");
        ExpenseCategoryModel instance = new ExpenseCategoryModel();
        ExpenseCategoryEntity entity = new ExpenseCategoryEntity();

        doNothing().when(categoryValidator).validate(dto);
        doReturn(instance).when(mapper).dtoToModel(dto);
        doReturn(entity).when(mapper).modelToEntity(instance);
        doReturn(entity).when(expenseCategoryDao).save(entity);
        doReturn(dto).when(mapper).modelToDto(entity);

        ExpenseCategoryDto returnedDto = categoryService.addNewCategory(dto);

        verify(categoryValidator).validate(dto);
        verify(mapper).dtoToModel(dto);
        verify(mapper).modelToEntity(instance);
        verify(expenseCategoryDao).save(entity);
        verify(mapper).modelToDto(entity);

        assertEquals(1L, returnedDto.getId());
        assertEquals("PARKING", returnedDto.getExpenseType());
    }

    @Test
    public void listOfExpenseCategoriesDtoWillBeReturnedWhenThereAreCategoriesInDb() {
        List<ExpenseCategoryEntity> allCategories = new ArrayList<>();
        ExpenseCategoryDto firstCategoryDto = new ExpenseCategoryDto();
        ExpenseCategoryDto secondCategoryDto = new ExpenseCategoryDto();
        firstCategoryDto.setExpenseType("REFUEL");
        secondCategoryDto.setExpenseType("REPAIR");
        ListOfExpenseCategoriesDto listDto = new ListOfExpenseCategoriesDto();
        listDto.setExpenseCategories(Arrays.asList(firstCategoryDto, secondCategoryDto));

        doReturn(allCategories).when(expenseCategoryDao).findAll();
        doReturn(listDto).when(mapper).listOfExpenseCategoriesToDto(allCategories);

        ListOfExpenseCategoriesDto returnedListDto = categoryService.getAllCategories();

        verify(expenseCategoryDao).findAll();
        verify(mapper).listOfExpenseCategoriesToDto(allCategories);

        assertEquals(listDto, returnedListDto);
        assertEquals(2, returnedListDto.getExpenseCategories().size());
        assertEquals("REFUEL", returnedListDto.getExpenseCategories().get(0).getExpenseType());
        assertEquals("REPAIR", returnedListDto.getExpenseCategories().get(1).getExpenseType());
    }

    @Test
    public void emptyListOfExpenseCategoriesDtoWillBeReturnedWhenThereAreNoCategoriesInDb() {
        List<ExpenseCategoryEntity> allCategories = new ArrayList<>();
        ListOfExpenseCategoriesDto listDto = new ListOfExpenseCategoriesDto();

        doReturn(allCategories).when(expenseCategoryDao).findAll();
        doReturn(listDto).when(mapper).listOfExpenseCategoriesToDto(allCategories);

        ListOfExpenseCategoriesDto returnedListDto = categoryService.getAllCategories();

        verify(expenseCategoryDao).findAll();
        verify(mapper).listOfExpenseCategoriesToDto(allCategories);

        assertEquals(listDto, returnedListDto);
        assertTrue(returnedListDto.getExpenseCategories().isEmpty());
    }
}