package com.likhomanov.services;

import com.likhomanov.dao.entities.CarEntity;
import com.likhomanov.dao.entities.ExpenseInfoEntity;
import com.likhomanov.dao.entities.RefuelEntity;
import com.likhomanov.dao.entities.ReplacedPartEntity;
import com.likhomanov.dao.repositories.CarDao;
import com.likhomanov.exceptions.EntityNotFoundException;
import com.likhomanov.model.Refuel;
import com.likhomanov.model.RefuelModel;
import com.likhomanov.model.ReplacedPart;
import com.likhomanov.model.ReplacedPartModel;
import com.likhomanov.services.mappers.RefuelMapper;
import com.likhomanov.services.mappers.ReplacedPartMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ExpenseToCarAttacherTest {

    @Captor
    private ArgumentCaptor<RefuelEntity> refuelCaptor;
    @Captor
    private ArgumentCaptor<ReplacedPartEntity> partCaptor;
    @Mock
    private CarDao carDao;
    @Mock
    private RefuelMapper refuelMapper;
    @Mock
    private ReplacedPartMapper partMapper;
    @InjectMocks
    private ExpenseToCarAttacher expenseAttacher;

    @Test
    public void newRefuelWillBeAttachedToExistingCarAndSavedToDbWhenGivenValidCarIdAndRefuel() {
        RefuelModel refuelModel = new RefuelModel();
        RefuelEntity refuelEntity = new RefuelEntity();
        ExpenseInfoEntity infoEntity = new ExpenseInfoEntity();
        refuelEntity.setVolume(10.0);
        refuelEntity.setExpenseInfo(infoEntity);
        Long carId = 1L;

        doReturn(refuelEntity).when(refuelMapper).modelToEntity(refuelModel);
        doAnswer(invocationOnMock -> {
            RefuelEntity refuelToSave = invocationOnMock.getArgument(1);
            CarEntity carFromDb = new CarEntity();
            carFromDb.setId(1L);
            carFromDb.addRefuel(refuelToSave);
            return refuelToSave;
        }).when(carDao).attachRefuelToCarAndSave(carId, refuelEntity);

        Refuel result = expenseAttacher.attachNewRefuelToCar(carId, refuelModel);

        verify(refuelMapper).modelToEntity(refuelModel);
        verify(carDao).attachRefuelToCarAndSave(eq(carId), refuelCaptor.capture());

        assertSame(refuelEntity, refuelCaptor.getValue());
        assertNull(refuelCaptor.getValue().getId());
        assertEquals(10.0, refuelCaptor.getValue().getVolume());
        assertSame(infoEntity, refuelCaptor.getValue().getExpenseInfo());
        assertSame(refuelEntity, result);
        assertNotNull(result.getCar());
        assertNotNull(result.getExpenseInfo());
        assertEquals(10.0, result.getVolume());
        assertEquals(1L, result.getCar().getId());
    }

    @Test
    public void entityNotFoundExceptionWillBeThrownWhenGivenInvalidCarIdAndRefuel() {
        RefuelModel refuelModel = new RefuelModel();
        RefuelEntity refuelEntity = new RefuelEntity();
        Long carId = 1L;

        doReturn(refuelEntity).when(refuelMapper).modelToEntity(refuelModel);
        doThrow(EntityNotFoundException.class).when(carDao).attachRefuelToCarAndSave(carId, refuelEntity);

        assertThrows(EntityNotFoundException.class, () -> expenseAttacher.attachNewRefuelToCar(carId, refuelModel));
    }

    @Test
    public void newReplacedPartWillBeAttachedToExistingCarAndSavedToDbWhenGivenValidCarIdAndReplacedPart() {
        ReplacedPartModel partInstance = new ReplacedPartModel();
        ReplacedPartEntity partEntity = new ReplacedPartEntity();
        ExpenseInfoEntity infoEntity = new ExpenseInfoEntity();
        partEntity.setSerialNumber("123456");
        partEntity.setName("Spare part");
        partEntity.setExpenseInfo(infoEntity);
        Long carId = 1L;

        doReturn(partEntity).when(partMapper).modelToEntity(partInstance);
        doAnswer(invocationOnMock -> {
            ReplacedPartEntity partToSave = invocationOnMock.getArgument(1);
            CarEntity carFromDb = new CarEntity();
            carFromDb.setId(1L);
            carFromDb.addReplacedPart(partToSave);
            return partToSave;
        }).when(carDao).attachReplacedPartToCarAndSave(carId, partEntity);

        ReplacedPart result = expenseAttacher.attachNewReplacedPartToCar(carId, partInstance);

        verify(partMapper).modelToEntity(partInstance);
        verify(carDao).attachReplacedPartToCarAndSave(eq(carId), partCaptor.capture());

        assertSame(partEntity, partCaptor.getValue());
        assertNull(partCaptor.getValue().getId());
        assertEquals("123456", partCaptor.getValue().getSerialNumber());
        assertEquals("Spare part", partCaptor.getValue().getName());
        assertSame(infoEntity, partCaptor.getValue().getExpenseInfo());
        assertSame(partEntity, result);
        assertNotNull(result.getCar());
        assertNotNull(result.getExpenseInfo());
        assertEquals("123456", result.getSerialNumber());
        assertEquals("Spare part", result.getName());
        assertEquals(1L, result.getCar().getId());
    }

    @Test
    public void entityNotFoundExceptionWillBeThrownWhenGivenInvalidCarIdAndReplacedPart() {
        ReplacedPartModel partInstance = new ReplacedPartModel();
        ReplacedPartEntity partEntity = new ReplacedPartEntity();
        Long carId = 1L;

        doReturn(partEntity).when(partMapper).modelToEntity(partInstance);
        doThrow(EntityNotFoundException.class).when(carDao).attachReplacedPartToCarAndSave(carId, partEntity);

        assertThrows(EntityNotFoundException.class, () -> expenseAttacher.attachNewReplacedPartToCar(carId, partInstance));
    }
}