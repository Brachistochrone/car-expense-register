package com.likhomanov.services;

import com.likhomanov.dao.repositories.RefuelDao;
import com.likhomanov.exceptions.EntityNotFoundException;
import com.likhomanov.model.*;
import com.likhomanov.services.mappers.ConsumedGasVolumeMapper;
import com.likhomanov.services.mappers.RefuelMapper;
import com.likhomanov.services.validators.CarIdValidator;
import com.likhomanov.services.validators.DateValidator;
import com.likhomanov.services.validators.RefuelValidator;
import com.likhomanov.web.dto.ConsumedGasVolumeDto;
import com.likhomanov.web.dto.ExpenseCategoryDto;
import com.likhomanov.web.dto.ExpenseInfoDto;
import com.likhomanov.web.dto.RefuelDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

import static com.likhomanov.enums.Currency.UAH;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RefuelServiceTest {

    @Captor
    private ArgumentCaptor<Refuel> refuelCaptor;
    @Captor
    private ArgumentCaptor<ConsumedGasVolumeModel> consumedGasCaptor;
    @Mock
    private RefuelDao refuelDao;
    @Mock
    private RefuelMapper refuelMapper;
    @Mock
    private ConsumedGasVolumeMapper gasVolumeMapper;
    @Mock
    private ExpenseToCarAttacher refuelAttacher;
    @Mock
    private ExpenseCategoryAttacher categoryAttacher;
    @Mock
    private DateFormer dateFormer;
    @Mock
    private RefuelValidator refuelValidator;
    @Mock
    private CarIdValidator carIdValidator;
    @Mock
    private DateValidator dateValidator;
    @InjectMocks
    private RefuelService refuelService;
    private final RefuelDto newRefuel = new RefuelDto();
    private final ExpenseInfoDto expenseInfo = new ExpenseInfoDto();
    private final ExpenseCategoryDto expenseCategory = new ExpenseCategoryDto();

    @BeforeEach
    public void init() {
        expenseCategory.setExpenseType("REFUEL");

        expenseInfo.setDate(new Date());
        expenseInfo.setSum(100.0);
        expenseInfo.setCurrency(UAH);
        expenseInfo.setCompanyName("Some company");
        expenseInfo.setCompanyAddress("Some company address");
        expenseInfo.setDescription("Something happened");
        expenseInfo.setExpenseCategory(expenseCategory);

        newRefuel.setCarId(1L);
        newRefuel.setVolume(100.0);
        newRefuel.setRefuelInfo(expenseInfo);
    }

    @Test
    public void newRefuelWillBeAttachedToCarAndSavedToDbWhenGivenRefuelDto() {
        RefuelModel refuelModel = new RefuelModel();
        ExpenseInfoModel refuelInfo = new ExpenseInfoModel();
        CarModel car = new CarModel();
        car.setId(1L);
        refuelModel.setExpenseInfo(refuelInfo);
        refuelModel.setVolume(100.0);
        refuelModel.setCar(car);

        doNothing().when(refuelValidator).validate(newRefuel);
        doReturn(refuelModel).when(refuelMapper).dtoToModel(newRefuel);
        doReturn(refuelModel).when(categoryAttacher).attachExpenseCategory(refuelModel);
        doReturn(refuelModel).when(refuelAttacher).attachNewRefuelToCar(newRefuel.getCarId(), refuelModel);
        doReturn(newRefuel).when(refuelMapper).modelToDto(refuelModel);

        RefuelDto savedRefuel = refuelService.createNewRefuel(newRefuel);

        verify(refuelValidator).validate(newRefuel);
        verify(refuelMapper).dtoToModel(newRefuel);
        verify(categoryAttacher).attachExpenseCategory(refuelModel);
        verify(refuelAttacher).attachNewRefuelToCar(eq(newRefuel.getCarId()), refuelCaptor.capture());
        verify(refuelMapper).modelToDto(refuelModel);

        assertSame(newRefuel, savedRefuel);
        assertEquals(newRefuel.getCarId(), savedRefuel.getCarId());
        assertEquals(newRefuel.getVolume(), savedRefuel.getVolume());
        assertEquals(newRefuel.getRefuelInfo().getSum(), savedRefuel.getRefuelInfo().getSum());
        assertEquals(newRefuel.getRefuelInfo().getCompanyAddress(), savedRefuel.getRefuelInfo().getCompanyAddress());
        assertEquals(newRefuel.getRefuelInfo().getCompanyName(), savedRefuel.getRefuelInfo().getCompanyName());
        assertEquals(newRefuel.getRefuelInfo().getCurrency(), savedRefuel.getRefuelInfo().getCurrency());
        assertEquals(newRefuel.getRefuelInfo().getDate(), savedRefuel.getRefuelInfo().getDate());
        assertEquals(newRefuel.getRefuelInfo().getDescription(), savedRefuel.getRefuelInfo().getDescription());
        assertEquals(newRefuel.getRefuelInfo().getExpenseCategory().getExpenseType(),
                     savedRefuel.getRefuelInfo().getExpenseCategory().getExpenseType());
        assertEquals(refuelModel, refuelCaptor.getValue());
        assertEquals(refuelInfo, refuelCaptor.getValue().getExpenseInfo());
        assertEquals(100.0, refuelCaptor.getValue().getVolume());
        assertEquals(car, refuelCaptor.getValue().getCar());
        assertEquals(1L, refuelCaptor.getValue().getCar().getId());
        assertNull(refuelCaptor.getValue().getId());
    }

    @Test
    public void consumedGasVolumeDtoWillBeReturnedWhenGivenValidCarIdAndStartAndEndDate() {
        Long carId = 1L;
        String startDate = "2020-08-01";
        String endDate = "2020-08-05";
        Date startDateDate = Date.from(LocalDate.parse(startDate).atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date endDateDate = Date.from(LocalDate.parse(endDate).atStartOfDay(ZoneId.systemDefault()).toInstant());
        Double guzzledGasVolume = 20.0;

        doNothing().when(carIdValidator).validate(carId);
        doNothing().when(dateValidator).validate(startDate);
        doNothing().when(dateValidator).validate(endDate);
        doReturn(startDateDate).when(dateFormer).stringToDate(startDate);
        doReturn(endDateDate).when(dateFormer).stringToDate(endDate);
        doReturn(Optional.of(guzzledGasVolume)).when(refuelDao).findGuzzledGasForSomePeriodByCarId(carId,
                                                                                                   startDateDate,
                                                                                                   endDateDate);
        doReturn(new ConsumedGasVolumeDto()).when(gasVolumeMapper).modelToDto(any(ConsumedGasVolumeModel.class));

        refuelService.getConsumedVolumeOfGasByCarIdForSomePeriod(carId, startDate,endDate);

        verify(carIdValidator).validate(carId);
        verify(dateValidator).validate(startDate);
        verify(dateValidator).validate(endDate);
        verify(dateFormer).stringToDate(startDate);
        verify(dateFormer).stringToDate(endDate);
        verify(refuelDao).findGuzzledGasForSomePeriodByCarId(carId, startDateDate, endDateDate);
        verify(gasVolumeMapper).modelToDto(consumedGasCaptor.capture());

        assertEquals(guzzledGasVolume, consumedGasCaptor.getValue().getVolume());
    }

    @Test
    public void entityNotFoundExceptionWillBeThrownWhenGivenInvalidCarIdOrDates() {
        Long carId = 2L;
        String startDate = "2020-08-01";
        String endDate = "2020-08-05";
        Date startDateDate = Date.from(LocalDate.parse(startDate).atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date endDateDate = Date.from(LocalDate.parse(endDate).atStartOfDay(ZoneId.systemDefault()).toInstant());

        doNothing().when(carIdValidator).validate(carId);
        doNothing().when(dateValidator).validate(startDate);
        doNothing().when(dateValidator).validate(endDate);
        doReturn(startDateDate).when(dateFormer).stringToDate(startDate);
        doReturn(endDateDate).when(dateFormer).stringToDate(endDate);
        doReturn(Optional.empty()).when(refuelDao).findGuzzledGasForSomePeriodByCarId(carId,
                                                                                      startDateDate,
                                                                                      endDateDate);

        assertThrows(EntityNotFoundException.class,
                     () -> refuelService.getConsumedVolumeOfGasByCarIdForSomePeriod(carId, startDate,endDate));
    }
}