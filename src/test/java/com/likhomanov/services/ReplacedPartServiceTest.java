package com.likhomanov.services;

import com.likhomanov.dao.queryresults.MoneyByCurrencyResult;
import com.likhomanov.dao.repositories.ReplacedPartDao;
import com.likhomanov.enums.Currency;
import com.likhomanov.model.*;
import com.likhomanov.services.mappers.ReplacedPartMapper;
import com.likhomanov.services.mappers.SpentMoneyMapper;
import com.likhomanov.services.validators.CarIdValidator;
import com.likhomanov.services.validators.CurrencyValidator;
import com.likhomanov.services.validators.NewReplacedPartValidator;
import com.likhomanov.services.validators.YearValidator;
import com.likhomanov.web.dto.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.time.Year;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.likhomanov.enums.Currency.UAH;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ReplacedPartServiceTest {

    @Captor
    private ArgumentCaptor<ReplacedPart> replacedPartCaptor;
    @Captor
    private ArgumentCaptor<List<MoneyByCurrencyResult>> moneyResultCaptor;
    @Captor
    private ArgumentCaptor<SpentMoney> spentMoneyCaptor;
    @Mock
    private ReplacedPartDao replacedPartDao;
    @Mock
    private ReplacedPartMapper partMapper;
    @Mock
    private SpentMoneyMapper moneyMapper;
    @Mock
    private ExpenseCategoryAttacher categoryAttacher;
    @Mock
    private ExpenseToCarAttacher replacedPartAttacher;
    @Mock
    private DateFormer dateFormer;
    @Mock
    private CurrencyConverter currencyConverter;
    @Mock
    private NewReplacedPartValidator newPartValidator;
    @Mock
    private CarIdValidator carIdValidator;
    @Mock
    private YearValidator yearValidator;
    @Mock
    private CurrencyValidator currencyValidator;
    @InjectMocks
    private ReplacedPartService partService;
    private final NewReplacedPartDto newReplacedPart = new NewReplacedPartDto();
    private final ExpenseInfoDto expenseInfo = new ExpenseInfoDto();
    private final ExpenseCategoryDto expenseCategory = new ExpenseCategoryDto();
    private final ReplacedPartModel replacedPart = new ReplacedPartModel();

    @BeforeEach
    public void init() {
        expenseCategory.setExpenseType("REPAIR");

        expenseInfo.setDate(new Date());
        expenseInfo.setSum(100.0);
        expenseInfo.setCurrency(UAH);
        expenseInfo.setCompanyName("Some company");
        expenseInfo.setCompanyAddress("Some company address");
        expenseInfo.setDescription("Something happened");
        expenseInfo.setExpenseCategory(expenseCategory);

        newReplacedPart.setCarId(1L);
        newReplacedPart.setSerialNumber("WT123456");
        newReplacedPart.setName("Some spare part");
        newReplacedPart.setReplacedPartInfo(expenseInfo);

        replacedPart.setId(1L);
        replacedPart.setSerialNumber("WT123456");
        replacedPart.setName("Some spare part");
    }

    @Test
    public void newReplacedPartWillBeAttachedToCarAndSavedToDbWhenGivenNewReplacedPartDto() {
        ReplacedPartModel replacedPartModel = new ReplacedPartModel();
        ExpenseInfoModel partInfo = new ExpenseInfoModel();
        CarModel car = new CarModel();
        car.setId(1L);
        replacedPartModel.setExpenseInfo(partInfo);
        replacedPartModel.setSerialNumber("WT123456");
        replacedPartModel.setName("Some spare part");
        replacedPartModel.setCar(car);

        doNothing().when(newPartValidator).validate(newReplacedPart);
        doReturn(replacedPartModel).when(partMapper).dtoToModel(newReplacedPart);
        doReturn(replacedPartModel).when(categoryAttacher).attachExpenseCategory(replacedPartModel);
        doReturn(replacedPartModel).when(replacedPartAttacher).attachNewReplacedPartToCar(newReplacedPart.getCarId(),
                                                                                          replacedPartModel);
        doReturn(newReplacedPart).when(partMapper).modelToNewDto(replacedPartModel);

        NewReplacedPartDto savedReplacedPart = partService.createNewReplacedPart(newReplacedPart);

        verify(newPartValidator).validate(newReplacedPart);
        verify(partMapper).dtoToModel(newReplacedPart);
        verify(categoryAttacher).attachExpenseCategory(replacedPartModel);
        verify(replacedPartAttacher).attachNewReplacedPartToCar(eq(newReplacedPart.getCarId()),
                                                                replacedPartCaptor.capture());
        verify(partMapper).modelToNewDto(replacedPartModel);

        assertSame(newReplacedPart, savedReplacedPart);
        assertEquals(newReplacedPart.getCarId(),
                     savedReplacedPart.getCarId());
        assertEquals(newReplacedPart.getName(),
                     savedReplacedPart.getName());
        assertEquals(newReplacedPart.getSerialNumber(),
                     savedReplacedPart.getSerialNumber());
        assertEquals(newReplacedPart.getReplacedPartInfo().getSum(),
                     savedReplacedPart.getReplacedPartInfo().getSum());
        assertEquals(newReplacedPart.getReplacedPartInfo().getCompanyAddress(),
                     savedReplacedPart.getReplacedPartInfo().getCompanyAddress());
        assertEquals(newReplacedPart.getReplacedPartInfo().getCompanyName(),
                     savedReplacedPart.getReplacedPartInfo().getCompanyName());
        assertEquals(newReplacedPart.getReplacedPartInfo().getCurrency(),
                     savedReplacedPart.getReplacedPartInfo().getCurrency());
        assertEquals(newReplacedPart.getReplacedPartInfo().getDate(),
                     savedReplacedPart.getReplacedPartInfo().getDate());
        assertEquals(newReplacedPart.getReplacedPartInfo().getDescription(),
                     savedReplacedPart.getReplacedPartInfo().getDescription());
        assertEquals(newReplacedPart.getReplacedPartInfo().getExpenseCategory().getExpenseType(),
                     savedReplacedPart.getReplacedPartInfo().getExpenseCategory().getExpenseType());
        assertEquals(replacedPartModel, replacedPartCaptor.getValue());
        assertEquals(partInfo, replacedPartCaptor.getValue().getExpenseInfo());
        assertEquals("WT123456", replacedPartCaptor.getValue().getSerialNumber());
        assertEquals("Some spare part", replacedPartCaptor.getValue().getName());
        assertEquals(car, replacedPartCaptor.getValue().getCar());
        assertEquals(1L, replacedPartCaptor.getValue().getCar().getId());
        assertNull(replacedPartCaptor.getValue().getId());
    }

    @Test
    public void spentMoneyDtoWithSumWillBeReturnedWhenGivenValidCarIdYearAndCurrency() {
        Long carId = 1L;
        Year year = Year.of(2020);
        Date yearHead = Date.from(LocalDate.of(year.getValue(), 1, 1)
                                           .atStartOfDay(ZoneId.systemDefault())
                                           .toInstant());
        Date yearTail = Date.from(LocalDate.of(year.getValue(), 12, 31)
                                           .atStartOfDay(ZoneId.systemDefault())
                                           .toInstant());
        MoneyByCurrencyResult moneyResult = new MoneyByCurrencyResult() {
            @Override
            public Currency getCurrency() {
                return UAH;
            }

            @Override
            public Double getDough() {
                return 100.0;
            }
        };
        SpentMoneyModel spentMoney = new SpentMoneyModel();
        spentMoney.setCurrency(UAH);
        spentMoney.setSpentMoney(100.0);
        SpentMoneyDto spentMoneyDto = new SpentMoneyDto();
        spentMoneyDto.setCurrency(UAH);
        spentMoneyDto.setSpentMoney(100.0);

        doNothing().when(carIdValidator).validate(carId);
        doNothing().when(yearValidator).validate(year);
        doNothing().when(currencyValidator).validate(UAH);
        doReturn(yearHead).when(dateFormer).getYearHead(year);
        doReturn(yearTail).when(dateFormer).getYearTail(year);
        doReturn(Collections.singletonList(moneyResult)).when(replacedPartDao)
                                                  .findDoughSpentOnSparePartsForSomePeriodByCarId(carId,
                                                                                                  yearHead,
                                                                                                  yearTail);
        doReturn(spentMoney).when(currencyConverter).convertCurrency(eq(UAH), anyList());
        doReturn(spentMoneyDto).when(moneyMapper).modelToDto(spentMoney);

        SpentMoneyDto returnedSpentMoneyDto = partService.getMoneySpentOnSparePartsByCarIdForYear(carId, year, UAH);

        verify(carIdValidator).validate(carId);
        verify(yearValidator).validate(year);
        verify(currencyValidator).validate(UAH);
        verify(dateFormer).getYearHead(year);
        verify(dateFormer).getYearTail(year);
        verify(replacedPartDao).findDoughSpentOnSparePartsForSomePeriodByCarId(carId, yearHead, yearTail);
        verify(currencyConverter).convertCurrency(eq(UAH), moneyResultCaptor.capture());
        verify(moneyMapper).modelToDto(spentMoneyCaptor.capture());

        assertEquals(1, moneyResultCaptor.getValue().size());
        assertEquals(moneyResult, moneyResultCaptor.getValue().get(0));
        assertEquals(UAH, moneyResultCaptor.getValue().get(0).getCurrency());
        assertEquals(100.0, moneyResultCaptor.getValue().get(0).getDough());
        assertEquals(spentMoney, spentMoneyCaptor.getValue());
        assertEquals(UAH, spentMoneyCaptor.getValue().getCurrency());
        assertEquals(100.0, spentMoneyCaptor.getValue().getSpentMoney());
        assertEquals(spentMoneyDto, returnedSpentMoneyDto);
        assertEquals(UAH, returnedSpentMoneyDto.getCurrency());
        assertEquals(100.0, returnedSpentMoneyDto.getSpentMoney());
    }

    @Test
    public void spentMoneyDtoWithZeroSumWillBeReturnedWhenGivenInvalidCarIdOrYearAndCurrency() {
        Long carId = 5L;
        Year year = Year.of(1980);
        Date yearHead = Date.from(LocalDate.of(year.getValue(), 1, 1)
                                           .atStartOfDay(ZoneId.systemDefault())
                                           .toInstant());
        Date yearTail = Date.from(LocalDate.of(year.getValue(), 12, 31)
                                           .atStartOfDay(ZoneId.systemDefault())
                                           .toInstant());
        SpentMoneyModel spentMoney = new SpentMoneyModel();
        spentMoney.setCurrency(UAH);
        spentMoney.setSpentMoney(0.0);
        SpentMoneyDto spentMoneyDto = new SpentMoneyDto();
        spentMoneyDto.setCurrency(UAH);
        spentMoneyDto.setSpentMoney(0.0);

        doNothing().when(carIdValidator).validate(carId);
        doNothing().when(yearValidator).validate(year);
        doNothing().when(currencyValidator).validate(UAH);
        doReturn(yearHead).when(dateFormer).getYearHead(year);
        doReturn(yearTail).when(dateFormer).getYearTail(year);
        doReturn(new ArrayList<>()).when(replacedPartDao)
                                   .findDoughSpentOnSparePartsForSomePeriodByCarId(carId, yearHead, yearTail);
        doReturn(spentMoney).when(currencyConverter).convertCurrency(eq(UAH), anyList());
        doReturn(spentMoneyDto).when(moneyMapper).modelToDto(spentMoney);

        SpentMoneyDto returnedSpentMoneyDto = partService.getMoneySpentOnSparePartsByCarIdForYear(carId, year, UAH);

        verify(carIdValidator).validate(carId);
        verify(yearValidator).validate(year);
        verify(currencyValidator).validate(UAH);
        verify(dateFormer).getYearHead(year);
        verify(dateFormer).getYearTail(year);
        verify(replacedPartDao).findDoughSpentOnSparePartsForSomePeriodByCarId(carId, yearHead, yearTail);
        verify(currencyConverter).convertCurrency(eq(UAH), moneyResultCaptor.capture());
        verify(moneyMapper).modelToDto(spentMoneyCaptor.capture());

        assertTrue(moneyResultCaptor.getValue().isEmpty());
        assertEquals(spentMoney, spentMoneyCaptor.getValue());
        assertEquals(UAH, spentMoneyCaptor.getValue().getCurrency());
        assertEquals(0.0, spentMoneyCaptor.getValue().getSpentMoney());
        assertEquals(spentMoneyDto, returnedSpentMoneyDto);
        assertEquals(UAH, returnedSpentMoneyDto.getCurrency());
        assertEquals(0.0, returnedSpentMoneyDto.getSpentMoney());
    }

    @Test
    public void spentMoneyDtoWithSumWillBeReturnedWhenGivenValidCarIdCurrency() {
        Long carId = 1L;
        MoneyByCurrencyResult moneyResult = new MoneyByCurrencyResult() {
            @Override
            public Currency getCurrency() {
                return UAH;
            }

            @Override
            public Double getDough() {
                return 100.0;
            }
        };
        SpentMoneyModel spentMoney = new SpentMoneyModel();
        spentMoney.setCurrency(UAH);
        spentMoney.setSpentMoney(100.0);
        SpentMoneyDto spentMoneyDto = new SpentMoneyDto();
        spentMoneyDto.setCurrency(UAH);
        spentMoneyDto.setSpentMoney(100.0);

        doNothing().when(carIdValidator).validate(carId);
        doNothing().when(currencyValidator).validate(UAH);
        doReturn(Collections.singletonList(moneyResult)).when(replacedPartDao).findDoughSpentOnSparePartsByCarId(carId);
        doReturn(spentMoney).when(currencyConverter).convertCurrency(eq(UAH), anyList());
        doReturn(spentMoneyDto).when(moneyMapper).modelToDto(spentMoney);

        SpentMoneyDto returnedSpentMoneyDto = partService.getMoneySpentOnSparePartsByCarId(carId, UAH);

        verify(carIdValidator).validate(carId);
        verify(currencyValidator).validate(UAH);
        verify(replacedPartDao).findDoughSpentOnSparePartsByCarId(carId);
        verify(currencyConverter).convertCurrency(eq(UAH), moneyResultCaptor.capture());
        verify(moneyMapper).modelToDto(spentMoneyCaptor.capture());

        assertEquals(1, moneyResultCaptor.getValue().size());
        assertEquals(moneyResult, moneyResultCaptor.getValue().get(0));
        assertEquals(UAH, moneyResultCaptor.getValue().get(0).getCurrency());
        assertEquals(100.0, moneyResultCaptor.getValue().get(0).getDough());
        assertEquals(spentMoney, spentMoneyCaptor.getValue());
        assertEquals(UAH, spentMoneyCaptor.getValue().getCurrency());
        assertEquals(100.0, spentMoneyCaptor.getValue().getSpentMoney());
        assertEquals(spentMoneyDto, returnedSpentMoneyDto);
        assertEquals(UAH, returnedSpentMoneyDto.getCurrency());
        assertEquals(100.0, returnedSpentMoneyDto.getSpentMoney());
    }

    @Test
    public void spentMoneyDtoWithZeroSumWillBeReturnedWhenGivenInvalidCarIdAndCurrency() {
        Long carId = 5L;
        SpentMoneyModel spentMoney = new SpentMoneyModel();
        spentMoney.setCurrency(UAH);
        spentMoney.setSpentMoney(0.0);
        SpentMoneyDto spentMoneyDto = new SpentMoneyDto();
        spentMoneyDto.setCurrency(UAH);
        spentMoneyDto.setSpentMoney(0.0);

        doNothing().when(carIdValidator).validate(carId);
        doNothing().when(currencyValidator).validate(UAH);
        doReturn(new ArrayList<>()).when(replacedPartDao).findDoughSpentOnSparePartsByCarId(carId);
        doReturn(spentMoney).when(currencyConverter).convertCurrency(eq(UAH), anyList());
        doReturn(spentMoneyDto).when(moneyMapper).modelToDto(spentMoney);

        SpentMoneyDto returnedSpentMoneyDto = partService.getMoneySpentOnSparePartsByCarId(carId, UAH);

        verify(carIdValidator).validate(carId);
        verify(currencyValidator).validate(UAH);
        verify(replacedPartDao).findDoughSpentOnSparePartsByCarId(carId);
        verify(currencyConverter).convertCurrency(eq(UAH), moneyResultCaptor.capture());
        verify(moneyMapper).modelToDto(spentMoneyCaptor.capture());

        assertTrue(moneyResultCaptor.getValue().isEmpty());
        assertEquals(spentMoney, spentMoneyCaptor.getValue());
        assertEquals(UAH, spentMoneyCaptor.getValue().getCurrency());
        assertEquals(0.0, spentMoneyCaptor.getValue().getSpentMoney());
        assertEquals(spentMoneyDto, returnedSpentMoneyDto);
        assertEquals(UAH, returnedSpentMoneyDto.getCurrency());
        assertEquals(0.0, returnedSpentMoneyDto.getSpentMoney());
    }

    @Test
    public void pageOfReplacedPartsDtoWillBeReturnedWhenGivenValidCarIdAndPageable() {
        Long carId = 1L;
        Pageable pageable = new PageableImpl();
        Page<? extends ReplacedPart> replacedParts = new PageImpl<>(Collections.singletonList(replacedPart));
        Page<ReplacedPartDto> replacedPartDtos = new PageImpl<>(Collections.singletonList(newReplacedPart));

        doNothing().when(carIdValidator).validate(carId);
        doReturn(replacedParts).when(replacedPartDao).findByCarId(carId, pageable);
        doReturn(replacedPartDtos).when(partMapper).pageOfReplacedPartsToDto(replacedParts);

        Page<ReplacedPartDto> result = partService.getReplacedPartsByCarId(carId, pageable);

        verify(carIdValidator).validate(carId);
        verify(replacedPartDao).findByCarId(carId, pageable);
        verify(partMapper).pageOfReplacedPartsToDto(replacedParts);

        assertEquals(1,
                     result.getContent().size());
        assertEquals("WT123456",
                     result.getContent().get(0).getSerialNumber());
        assertEquals("Some spare part",
                     result.getContent().get(0).getName());
        assertNotNull(result.getContent().get(0).getReplacedPartInfo());
        assertNotNull(result.getContent().get(0).getReplacedPartInfo().getDate());
        assertEquals(100.0,
                     result.getContent().get(0).getReplacedPartInfo().getSum());
        assertEquals(UAH,
                     result.getContent().get(0).getReplacedPartInfo().getCurrency());
        assertEquals("Some company",
                     result.getContent().get(0).getReplacedPartInfo().getCompanyName());
        assertEquals("Some company address",
                     result.getContent().get(0).getReplacedPartInfo().getCompanyAddress());
        assertEquals("Something happened",
                     result.getContent().get(0).getReplacedPartInfo().getDescription());
        assertNotNull(result.getContent().get(0).getReplacedPartInfo().getExpenseCategory());
        assertEquals("REPAIR",
                     result.getContent().get(0).getReplacedPartInfo().getExpenseCategory().getExpenseType());
    }

    @Test
    public void emptyPageOfReplacedPartDtosWillBeReturnedWhenGivenInvalidCarId() {
        Long carId = 5L;
        Pageable pageable = new PageableImpl();
        Page<? extends ReplacedPart> replacedParts = new PageImpl<>(Collections.emptyList());
        Page<ReplacedPartDto> replacedPartDtos = new PageImpl<>(Collections.emptyList());

        doNothing().when(carIdValidator).validate(carId);
        doReturn(replacedParts).when(replacedPartDao).findByCarId(carId, pageable);
        doReturn(replacedPartDtos).when(partMapper).pageOfReplacedPartsToDto(replacedParts);

        Page<ReplacedPartDto> result = partService.getReplacedPartsByCarId(carId, pageable);

        verify(carIdValidator).validate(carId);
        verify(replacedPartDao).findByCarId(carId, pageable);
        verify(partMapper).pageOfReplacedPartsToDto(replacedParts);

        assertTrue(result.getContent().isEmpty());
    }

    private static final class PageableImpl implements org.springframework.data.domain.Pageable {

        @Override
        public int getPageNumber() {
            return 0;
        }

        @Override
        public int getPageSize() {
            return 2;
        }

        @Override
        public long getOffset() {
            return 0;
        }

        @Override
        public Sort getSort() {
            return null;
        }

        @Override
        public org.springframework.data.domain.Pageable next() {
            return null;
        }

        @Override
        public org.springframework.data.domain.Pageable previousOrFirst() {
            return null;
        }

        @Override
        public org.springframework.data.domain.Pageable first() {
            return null;
        }

        @Override
        public boolean hasPrevious() {
            return false;
        }
    }
}