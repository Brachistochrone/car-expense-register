package com.likhomanov.services;

import com.likhomanov.dao.repositories.UserRoleDao;
import com.likhomanov.exceptions.RoleAppointerException;
import com.likhomanov.model.User;
import com.likhomanov.model.UserModel;
import com.likhomanov.model.UserRoleModel;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.likhomanov.enums.Role.ROLE_USER;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class RolesAppointerTest {

    @Captor
    private ArgumentCaptor<UserRoleModel> userRoleCaptor;
    @Mock
    private UserRoleDao roleDao;
    @Spy
    private UserModel newUser;
    @InjectMocks
    private RolesAppointer rolesAppointer;

    @Test
    public void roleUserWillBeAppointedToNewUserAndUserInstanceWillBeReturned() {
        newUser.setEmail("some_email@gmail.com");
        newUser.setPassword("qwerty");

        doReturn(Optional.of(1L)).when(roleDao).findRoleIdByRoleName(ROLE_USER);

        User returnedUser = rolesAppointer.appointRoleUser(newUser);

        verify(roleDao).findRoleIdByRoleName(ROLE_USER);
        verify(newUser).addRole(userRoleCaptor.capture());

        assertEquals(newUser, returnedUser);
        assertEquals("some_email@gmail.com", returnedUser.getEmail());
        assertEquals("qwerty", returnedUser.getPassword());
        assertNull(returnedUser.getId());
        assertEquals(1, returnedUser.getRoles().size());
        assertEquals(1L, userRoleCaptor.getValue().getId());
        assertEquals(ROLE_USER, userRoleCaptor.getValue().getRole());
    }

    @Test
    public void roleAppointerExceptionWillBeThrownWhenRoleDoesNotExistInDb() {
        doReturn(Optional.empty()).when(roleDao).findRoleIdByRoleName(ROLE_USER);

        assertThrows(RoleAppointerException.class, () -> rolesAppointer.appointRoleUser(newUser));
    }
}