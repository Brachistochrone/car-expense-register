package com.likhomanov.services;

import com.likhomanov.dao.entities.UserEntity;
import com.likhomanov.dao.entities.UserRoleEntity;
import com.likhomanov.dao.repositories.UserDao;
import com.likhomanov.exceptions.EntityNotFoundException;
import com.likhomanov.model.UserModel;
import com.likhomanov.model.UserRoleModel;
import com.likhomanov.services.mappers.UserMapper;
import com.likhomanov.services.mappers.UserRoleMapper;
import com.likhomanov.services.validators.NewUserValidator;
import com.likhomanov.web.dto.NewUserDto;
import com.likhomanov.web.dto.UserDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

import static com.likhomanov.enums.Role.ROLE_USER;
import static java.util.Collections.singleton;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Captor
    private ArgumentCaptor<NewUserDto> newUserDtoCaptor;
    @Captor
    private ArgumentCaptor<UserModel> userInstanceCaptor;
    @Captor
    private ArgumentCaptor<UserEntity> userEntityCaptor;
    @Mock
    private UserDao userDao;
    @Mock
    private RolesAppointer rolesAppointer;
    @Mock
    private UserMapper userMapper;
    @Mock
    private UserRoleMapper roleMapper;
    @Mock
    private NewUserValidator userValidator;
    @InjectMocks
    private UserService userService;

    @Test
    public void newUserWillBeSavedToDpAndUserDtoWillBeReturnedWhenGivenNewUserDto() {
        String testEmail = "some_email@gmail.com";
        String testPassword = "qwerty";
        Long testId = 1L;
        UserRoleModel roleInstance = new UserRoleModel();
        roleInstance.setId(testId);
        roleInstance.setRole(ROLE_USER);
        UserRoleEntity roleEntity = new UserRoleEntity();
        roleEntity.setId(testId);
        roleEntity.setRole(ROLE_USER);
        NewUserDto newUser = new NewUserDto();
        newUser.setEmail(testEmail);
        newUser.setPassword(testPassword);
        UserModel userModelWithoutRole = new UserModel();
        userModelWithoutRole.setEmail(testEmail);
        userModelWithoutRole.setPassword(testPassword);
        UserModel userModelWithRole = new UserModel();
        userModelWithRole.setEmail(testEmail);
        userModelWithRole.setPassword(testPassword);
        userModelWithRole.addRole(roleInstance);
        UserEntity newUserEntity = new UserEntity();
        newUserEntity.setEmail(testEmail);
        newUserEntity.setPassword(testPassword);
        newUserEntity.addRole(roleEntity);
        UserEntity savedUserEntity = new UserEntity();
        savedUserEntity.setId(testId);
        savedUserEntity.setEmail(testEmail);
        savedUserEntity.setPassword(testPassword);
        savedUserEntity.addRole(roleEntity);
        UserDto userDto = new UserDto();
        userDto.setEmail(testEmail);
        userDto.setId(testId);

        doNothing().when(userValidator).validate(newUser);
        doReturn(userModelWithoutRole).when(userMapper).dtoToModel(newUser);
        doReturn(userModelWithRole).when(rolesAppointer).appointRoleUser(userModelWithoutRole);
        doReturn(newUserEntity).when(userMapper).modelToEntity(userModelWithRole);
        doReturn(savedUserEntity).when(userDao).saveUserWithRole(newUserEntity);
        doReturn(userDto).when(userMapper).modelToDto(savedUserEntity);

        UserDto returnedUser = userService.saveNewUser(newUser);

        verify(userValidator).validate(newUser);
        verify(userMapper).dtoToModel(newUserDtoCaptor.capture());
        verify(rolesAppointer).appointRoleUser(userInstanceCaptor.capture());
        verify(userMapper).modelToEntity(userInstanceCaptor.capture());
        verify(userDao).saveUserWithRole(userEntityCaptor.capture());
        verify(userMapper).modelToDto(userEntityCaptor.capture());

        assertEquals(testEmail, newUserDtoCaptor.getValue().getEmail());
        assertEquals(testPassword, newUserDtoCaptor.getValue().getPassword());
        assertEquals(testEmail, userInstanceCaptor.getAllValues().get(0).getEmail());
        assertEquals(testPassword, userInstanceCaptor.getAllValues().get(0).getPassword());
        assertNull(userInstanceCaptor.getAllValues().get(0).getId());
        assertTrue(userInstanceCaptor.getAllValues().get(0).getRoles().isEmpty());
        assertEquals(testEmail, userInstanceCaptor.getAllValues().get(1).getEmail());
        assertEquals(testPassword, userInstanceCaptor.getAllValues().get(1).getPassword());
        assertNull(userInstanceCaptor.getAllValues().get(1).getId());
        assertEquals(1, userInstanceCaptor.getAllValues().get(1).getRoles().size());
        assertTrue(userInstanceCaptor.getAllValues().get(1).getRoles().contains(roleInstance));
        assertEquals(testEmail, userEntityCaptor.getAllValues().get(0).getEmail());
        assertEquals(testPassword, userEntityCaptor.getAllValues().get(0).getPassword());
        assertNull(userEntityCaptor.getAllValues().get(0).getId());
        assertEquals(1, userEntityCaptor.getAllValues().get(0).getRoles().size());
        assertTrue(userEntityCaptor.getAllValues().get(0).getRoles().contains(roleEntity));
        assertEquals(testEmail, userEntityCaptor.getAllValues().get(1).getEmail());
        assertEquals(testPassword, userEntityCaptor.getAllValues().get(1).getPassword());
        assertEquals(testId, userEntityCaptor.getAllValues().get(1).getId());
        assertEquals(1, userEntityCaptor.getAllValues().get(1).getRoles().size());
        assertTrue(userEntityCaptor.getAllValues().get(1).getRoles().contains(roleEntity));
        assertEquals(userDto, returnedUser);
        assertEquals(testEmail, returnedUser.getEmail());
        assertEquals(testId, returnedUser.getId());
    }

    @Test
    public void userDetailsWillBeReturnedWhenGivenValidEmail() {
        String email = "test_email@gmail.com";
        UserRoleEntity role = new UserRoleEntity();
        role.setId(1L);
        role.setRole(ROLE_USER);
        UserEntity user = new UserEntity();
        user.setId(1L);
        user.setEmail(email);
        user.setPassword("qwerty");
        user.addRole(role);

        doReturn(Optional.of(user)).when(userDao).findOneByEmail(email);
        doReturn(singleton(new SimpleGrantedAuthority(ROLE_USER.name())))
                .when(roleMapper)
                .userRolesToAuthorities(anySet());

        UserDetails userDetails = userService.loadUserByUsername(email);

        verify(userDao).findOneByEmail(email);
        verify(roleMapper).userRolesToAuthorities(anySet());

        assertNotNull(userDetails);
        assertEquals(email, userDetails.getUsername());
        assertEquals("qwerty", userDetails.getPassword());
        assertEquals(1, userDetails.getAuthorities().size());
    }

    @Test
    public void entityNotFoundExceptionWillBeThrownWhenGivenUnknownEmail() {
        String email = "test_email@gmail.com";

        doReturn(Optional.empty()).when(userDao).findOneByEmail(email);

        assertThrows(EntityNotFoundException.class, () -> userService.loadUserByUsername(email));

        verify(userDao).findOneByEmail(email);
        verifyNoInteractions(userMapper);
    }
}