package com.likhomanov.services.mappers;

import com.likhomanov.dao.entities.CarInfoEntity;
import com.likhomanov.dao.entities.RefuelEntity;
import com.likhomanov.dao.entities.ReplacedPartEntity;
import com.likhomanov.model.*;
import com.likhomanov.web.dto.CarDto;
import com.likhomanov.web.dto.CarInfoDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CarMapperTest {

    @Mock
    private CarInfoMapper infoMapper;
    @Mock
    private ReplacedPartMapper replacedPartMapper;
    @Mock
    private RefuelMapper refuelMapper;
    @InjectMocks
    private final CarMapper carMapper = new CarMapperImpl();

    @Test
    public void carInfoDtoWillBeConvertedIntoModel() {
        CarInfoDto carInfoDto = new CarInfoDto();
        CarInfoModel carInfo = new CarInfoModel();

        doReturn(carInfo).when(infoMapper).dtoToModel(carInfoDto);

        CarInfoModel result = carMapper.infoDtoToModel(carInfoDto);

        verify(infoMapper).dtoToModel(carInfoDto);
        verifyNoInteractions(replacedPartMapper);
        verifyNoInteractions(refuelMapper);

        assertSame(carInfo, result);
    }

    @Test
    public void carInfoWillBeConvertedIntoEntity() {
        CarInfo carInfo = new CarInfoModel();
        CarInfoEntity carInfoEntity = new CarInfoEntity();

        doReturn(carInfoEntity).when(infoMapper).modelToEntity(carInfo);

        CarInfoEntity result = carMapper.infoModelToEntity(carInfo);

        verify(infoMapper).modelToEntity(carInfo);
        verifyNoInteractions(replacedPartMapper);
        verifyNoInteractions(refuelMapper);

        assertSame(carInfoEntity, result);
    }

    @Test
    public void carInfoWillBeConvertedIntoModel() {
        CarInfo carInfo = new CarInfoEntity();
        CarInfoModel carInfoModel = new CarInfoModel();

        doReturn(carInfoModel).when(infoMapper).entityToModel(carInfo);

        CarInfoModel result = carMapper.infoEntityToModel(carInfo);

        verify(infoMapper).entityToModel(carInfo);
        verifyNoInteractions(replacedPartMapper);
        verifyNoInteractions(refuelMapper);

        assertSame(carInfoModel, result);
    }

    @Test
    public void carInfoWillBeConvertedIntoCarModelDto() {
        CarInfo carInfo = new CarInfoModel();
        CarInfoDto carInfoDto = new CarInfoDto();

        doReturn(carInfoDto).when(infoMapper).modelToDto(carInfo);

        CarInfoDto result = carMapper.infoModelToDto(carInfo);

        verify(infoMapper).modelToDto(carInfo);
        verifyNoInteractions(replacedPartMapper);
        verifyNoInteractions(refuelMapper);

        assertSame(carInfoDto, result);
    }

    @Test
    public void replacedPartsWillBeConvertedIntoModels() {
        Set<? extends ReplacedPart> replacedParts = new HashSet<>();
        ReplacedPartModel part1 = new ReplacedPartModel();
        ReplacedPartModel part2 = new ReplacedPartModel();
        part1.setSerialNumber("123456");
        part2.setSerialNumber("567894");
        Set<ReplacedPartModel> partModels = new HashSet<>(Arrays.asList(part1, part2));

        doReturn(partModels).when(replacedPartMapper).replacedPartsToModels(replacedParts);

        Set<ReplacedPartModel> result = carMapper.replacedPartsToModels(replacedParts);

        verify(replacedPartMapper).replacedPartsToModels(replacedParts);
        verifyNoInteractions(infoMapper);
        verifyNoInteractions(refuelMapper);

        assertSame(partModels, result);
        assertEquals(2, result.size());
        assertTrue(result.contains(part1));
        assertTrue(result.contains(part2));
    }

    @Test
    public void replacedPartsWillBeConvertedIntoEntities() {
        Set<? extends ReplacedPart> replacedParts = new HashSet<>();
        ReplacedPartEntity part1 = new ReplacedPartEntity();
        ReplacedPartEntity part2 = new ReplacedPartEntity();
        part1.setSerialNumber("123456");
        part2.setSerialNumber("567894");
        Set<ReplacedPartEntity> partEntities = new HashSet<>(Arrays.asList(part1, part2));

        doReturn(partEntities).when(replacedPartMapper).replacedPartsToEntities(replacedParts);

        Set<ReplacedPartEntity> result = carMapper.replacedPartsToEntities(replacedParts);

        verify(replacedPartMapper).replacedPartsToEntities(replacedParts);
        verifyNoInteractions(infoMapper);
        verifyNoInteractions(refuelMapper);

        assertSame(partEntities, result);
        assertEquals(2, result.size());
        assertTrue(result.contains(part1));
        assertTrue(result.contains(part2));
    }

    @Test
    public void refuelsWillBeConvertedIntoModels() {
        Set<? extends Refuel> refuels = new HashSet<>();
        RefuelModel refuel1 = new RefuelModel();
        RefuelModel refuel2 = new RefuelModel();
        refuel1.setId(1L);
        refuel2.setId(2L);
        Set<RefuelModel> refuelModels = new HashSet<>(Arrays.asList(refuel1, refuel2));

        doReturn(refuelModels).when(refuelMapper).refuelsToModels(refuels);

        Set<RefuelModel> result = carMapper.refuelsToModels(refuels);

        verify(refuelMapper).refuelsToModels(refuels);
        verifyNoInteractions(infoMapper);
        verifyNoInteractions(replacedPartMapper);

        assertSame(refuelModels, result);
        assertEquals(2, result.size());
        assertTrue(result.contains(refuel1));
        assertTrue(result.contains(refuel2));
    }

    @Test
    public void refuelsWillBeConvertedIntoEntities() {
        Set<? extends Refuel> refuels = new HashSet<>();
        RefuelEntity refuel1 = new RefuelEntity();
        RefuelEntity refuel2 = new RefuelEntity();
        refuel1.setId(1L);
        refuel2.setId(2L);
        Set<RefuelEntity> refuelEntities = new HashSet<>(Arrays.asList(refuel1, refuel2));

        doReturn(refuelEntities).when(refuelMapper).refuelsToEntities(refuels);

        Set<RefuelEntity> result = carMapper.refuelsToEntities(refuels);

        verify(refuelMapper).refuelsToEntities(refuels);
        verifyNoInteractions(infoMapper);
        verifyNoInteractions(replacedPartMapper);

        assertSame(refuelEntities, result);
        assertEquals(2, result.size());
        assertTrue(result.contains(refuel1));
        assertTrue(result.contains(refuel2));
    }

    @Test
    public void pageOfCarsWillBeConvertedIntoPageOfCarDtos() {
        CarModel car1 = new CarModel();
        CarModel car2 = new CarModel();
        car1.setId(1L);
        car2.setId(2L);
        car1.setCarInfo(new CarInfoModel());
        car2.setCarInfo(new CarInfoModel());
        Page<? extends Car> cars = new PageImpl<>(Arrays.asList(car1, car2));

        doReturn(new CarInfoDto()).when(infoMapper).modelToDto(any(CarInfo.class));

        Page<CarDto> carDtos = carMapper.pageOfCarsToDto(cars);

        verify(infoMapper, times(2)).modelToDto(any(CarInfo.class));
        verifyNoInteractions(replacedPartMapper);
        verifyNoInteractions(refuelMapper);

        assertEquals(2, carDtos.getContent().size());
        assertEquals(1L, carDtos.getContent().get(0).getId());
        assertEquals(2L, carDtos.getContent().get(1).getId());
        assertNotNull(carDtos.getContent().get(0).getCarInfo());
        assertNotNull(carDtos.getContent().get(1).getCarInfo());
        assertNotNull(carDtos.getPageable());
        assertEquals(2, carDtos.getTotalElements());
    }

    @Test
    public void nullWillBeReturnedWhenGivenNullPageOfCars() {
        Page<CarDto> carDtos = carMapper.pageOfCarsToDto(null);

        verifyNoInteractions(infoMapper);
        verifyNoInteractions(refuelMapper);
        verifyNoInteractions(replacedPartMapper);

        assertNull(carDtos);
    }
}