package com.likhomanov.services.mappers;

import com.likhomanov.model.ExpenseCategory;
import com.likhomanov.model.ExpenseCategoryModel;
import com.likhomanov.web.dto.ListOfExpenseCategoriesDto;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExpenseCategoryMapperTest {

    private final ExpenseCategoryMapper mapper = new ExpenseCategoryMapperImpl();

    @Test
    public void listOfExpenseCategoriesWillBeConvertedToDto() {
        ExpenseCategoryModel category1 = new ExpenseCategoryModel();
        ExpenseCategoryModel category2 = new ExpenseCategoryModel();
        category1.setId(1L);
        category2.setId(2L);
        category1.setExpenseType("REFUEL");
        category2.setExpenseType("REPAIR");
        List<? extends ExpenseCategory> categories = Arrays.asList(category1, category2);

        ListOfExpenseCategoriesDto result = mapper.listOfExpenseCategoriesToDto(categories);

        assertEquals(2, result.getExpenseCategories().size());
        assertEquals(1L, result.getExpenseCategories().get(0).getId());
        assertEquals(2L, result.getExpenseCategories().get(1).getId());
        assertEquals("REFUEL", result.getExpenseCategories().get(0).getExpenseType());
        assertEquals("REPAIR", result.getExpenseCategories().get(1).getExpenseType());
    }
}