package com.likhomanov.services.mappers;

import com.likhomanov.dao.entities.ExpenseCategoryEntity;
import com.likhomanov.model.ExpenseCategoryModel;
import com.likhomanov.web.dto.ExpenseCategoryDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ExpenseInfoMapperTest {

    @Mock
    private ExpenseCategoryMapper categoryMapper;
    @InjectMocks
    private final ExpenseInfoMapper infoMapper = new ExpenseInfoMapperImpl();

    @Test
    public void expenseCategoryDtoWillBeConvertedIntoModel() {
        ExpenseCategoryDto dto = new ExpenseCategoryDto();
        ExpenseCategoryModel model = new ExpenseCategoryModel();
        model.setExpenseType("REPAIR");

        doReturn(model).when(categoryMapper).dtoToModel(dto);

        ExpenseCategoryModel result = infoMapper.categoryDtoToModel(dto);

        verify(categoryMapper).dtoToModel(dto);

        assertSame(model, result);
        assertEquals("REPAIR", result.getExpenseType());
    }

    @Test
    public void expenseCategoryWillBeConvertedIntoEntity() {
        ExpenseCategoryModel model = new ExpenseCategoryModel();
        ExpenseCategoryEntity entity = new ExpenseCategoryEntity();
        entity.setId(1L);
        entity.setExpenseType("REPAIR");

        doReturn(entity).when(categoryMapper).modelToEntity(model);

        ExpenseCategoryEntity result = infoMapper.categoryModelToEntity(model);

        verify(categoryMapper).modelToEntity(model);

        assertSame(entity, result);
        assertEquals(1L, result.getId());
        assertEquals("REPAIR", result.getExpenseType());
    }

    @Test
    public void expenseCategoryWillBeConvertedIntoModel() {
        ExpenseCategoryEntity entity = new ExpenseCategoryEntity();
        ExpenseCategoryModel model = new ExpenseCategoryModel();
        model.setId(1L);
        model.setExpenseType("REPAIR");

        doReturn(model).when(categoryMapper).entityToModel(entity);

        ExpenseCategoryModel result = infoMapper.categoryEntityToModel(entity);

        verify(categoryMapper).entityToModel(entity);

        assertSame(model, result);
        assertEquals(1L, result.getId());
        assertEquals("REPAIR", result.getExpenseType());
    }

    @Test
    public void expenseCategoryWillBeConvertedIntoDto() {
        ExpenseCategoryModel model = new ExpenseCategoryModel();
        ExpenseCategoryDto dto = new ExpenseCategoryDto();
        dto.setId(1L);
        dto.setExpenseType("REPAIR");

        doReturn(dto).when(categoryMapper).modelToDto(model);

        ExpenseCategoryDto result = infoMapper.categoryModelToDto(model);

        verify(categoryMapper).modelToDto(model);

        assertSame(dto, result);
        assertEquals(1L, result.getId());
        assertEquals("REPAIR", result.getExpenseType());
    }
}