package com.likhomanov.services.mappers;

import com.likhomanov.dao.entities.ExpenseInfoEntity;
import com.likhomanov.model.CarModel;
import com.likhomanov.model.ExpenseInfoModel;
import com.likhomanov.model.RefuelModel;
import com.likhomanov.web.dto.ExpenseInfoDto;
import com.likhomanov.web.dto.RefuelDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.likhomanov.enums.Currency.UAH;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RefuelMapperTest {

    @Mock
    private ExpenseInfoMapper infoMapper;
    @InjectMocks
    private final RefuelMapper refuelMapper = new RefuelMapperImpl();
    private final ExpenseInfoDto infoDto = new ExpenseInfoDto();
    private final ExpenseInfoModel infoModel = new ExpenseInfoModel();
    private final ExpenseInfoEntity infoEntity = new ExpenseInfoEntity();
    private final CarModel carModel = new CarModel();
    private final RefuelModel refuelModel = new RefuelModel();

    @BeforeEach
    public void init() {
        infoDto.setSum(100.0);
        infoDto.setCurrency(UAH);
        infoDto.setCompanyName("Name");
        infoDto.setCompanyAddress("Address");
        infoDto.setDescription("Description");

        infoModel.setId(1L);
        infoModel.setSum(100.0);
        infoModel.setCurrency(UAH);
        infoModel.setCompanyName("Name");
        infoModel.setCompanyAddress("Address");
        infoModel.setDescription("Description");

        infoEntity.setId(1L);
        infoEntity.setSum(100.0);
        infoEntity.setCurrency(UAH);
        infoEntity.setCompanyName("Name");
        infoEntity.setCompanyAddress("Address");
        infoEntity.setDescription("Description");

        carModel.setId(1L);

        refuelModel.setVolume(100.0);
        refuelModel.setExpenseInfo(infoModel);
        refuelModel.setCar(carModel);
    }

    @Test
    public void expenseInfoDtoWillBeConvertedIntoModel() {
        infoModel.setId(null);

        doReturn(infoModel).when(infoMapper).dtoToModel(infoDto);

        ExpenseInfoModel result = refuelMapper.infoDtoToModel(infoDto);

        verify(infoMapper).dtoToModel(infoDto);

        assertSame(infoModel, result);
        assertNull(result.getId());
        assertEquals(100.0, result.getSum());
        assertEquals(UAH, result.getCurrency());
        assertEquals("Name", result.getCompanyName());
        assertEquals("Address", result.getCompanyAddress());
        assertEquals("Description", result.getDescription());
    }

    @Test
    public void expenseInfoWillBeConvertedIntoEntity() {
        doReturn(infoEntity).when(infoMapper).modelToEntity(infoModel);

        ExpenseInfoEntity result = refuelMapper.infoModelToEntity(infoModel);

        verify(infoMapper).modelToEntity(infoModel);

        assertSame(infoEntity, result);
        assertEquals(1L, result.getId());
        assertEquals(100.0, result.getSum());
        assertEquals(UAH, result.getCurrency());
        assertEquals("Name", result.getCompanyName());
        assertEquals("Address", result.getCompanyAddress());
        assertEquals("Description", result.getDescription());
    }

    @Test
    public void expenseInfoWillBeConvertedIntoModel() {
        doReturn(infoModel).when(infoMapper).entityToModel(infoEntity);

        ExpenseInfoModel result = refuelMapper.infoEntityToModel(infoEntity);

        verify(infoMapper).entityToModel(infoEntity);

        assertSame(infoModel, result);
        assertEquals(1L, result.getId());
        assertEquals(100.0, result.getSum());
        assertEquals(UAH, result.getCurrency());
        assertEquals("Name", result.getCompanyName());
        assertEquals("Address", result.getCompanyAddress());
        assertEquals("Description", result.getDescription());
    }

    @Test
    public void expenseInfoWillBeConvertedIntoDto() {
        doReturn(infoDto).when(infoMapper).modelToDto(infoModel);

        ExpenseInfoDto result = refuelMapper.infoModelToDto(infoModel);

        verify(infoMapper).modelToDto(infoModel);

        assertSame(infoDto, result);
        assertEquals(100.0, result.getSum());
        assertEquals(UAH, result.getCurrency());
        assertEquals("Name", result.getCompanyName());
        assertEquals("Address", result.getCompanyAddress());
        assertEquals("Description", result.getDescription());
    }

    @Test
    public void refuelModelWillBeConvertedIntoDto() {
        doReturn(infoDto).when(infoMapper).modelToDto(infoModel);

        RefuelDto result = refuelMapper.modelToDto(refuelModel);

        verify(infoMapper).modelToDto(infoModel);

        assertEquals(1L, result.getCarId());
        assertEquals(100.0, result.getVolume());
        assertEquals(100.0, result.getRefuelInfo().getSum());
        assertEquals(UAH, result.getRefuelInfo().getCurrency());
        assertEquals("Name", result.getRefuelInfo().getCompanyName());
        assertEquals("Address", result.getRefuelInfo().getCompanyAddress());
        assertEquals("Description", result.getRefuelInfo().getDescription());
    }

    @Test
    public void nullWillBeReturnedWhenGivenNullRefuelModel() {
        RefuelDto result = refuelMapper.modelToDto(null);

        verifyNoInteractions(infoMapper);

        assertNull(result);
    }
}