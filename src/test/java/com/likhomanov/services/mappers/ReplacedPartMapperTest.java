package com.likhomanov.services.mappers;

import com.likhomanov.dao.entities.ExpenseInfoEntity;
import com.likhomanov.model.CarModel;
import com.likhomanov.model.ExpenseInfoModel;
import com.likhomanov.model.ReplacedPart;
import com.likhomanov.model.ReplacedPartModel;
import com.likhomanov.web.dto.ExpenseInfoDto;
import com.likhomanov.web.dto.NewReplacedPartDto;
import com.likhomanov.web.dto.ReplacedPartDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.Arrays;
import java.util.Collections;

import static com.likhomanov.enums.Currency.UAH;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ReplacedPartMapperTest {

    @Mock
    private ExpenseInfoMapper infoMapper;
    @InjectMocks
    private final ReplacedPartMapper partMapper = new ReplacedPartMapperImpl();
    private final ExpenseInfoDto infoDto = new ExpenseInfoDto();
    private final ExpenseInfoModel infoModel = new ExpenseInfoModel();
    private final ExpenseInfoEntity infoEntity = new ExpenseInfoEntity();
    private final CarModel carModel = new CarModel();
    private final ReplacedPartModel partModel = new ReplacedPartModel();

    @BeforeEach
    public void init() {
        infoDto.setSum(100.0);
        infoDto.setCurrency(UAH);
        infoDto.setCompanyName("Name");
        infoDto.setCompanyAddress("Address");
        infoDto.setDescription("Description");

        infoModel.setId(1L);
        infoModel.setSum(100.0);
        infoModel.setCurrency(UAH);
        infoModel.setCompanyName("Name");
        infoModel.setCompanyAddress("Address");
        infoModel.setDescription("Description");

        infoEntity.setId(1L);
        infoEntity.setSum(100.0);
        infoEntity.setCurrency(UAH);
        infoEntity.setCompanyName("Name");
        infoEntity.setCompanyAddress("Address");
        infoEntity.setDescription("Description");

        carModel.setId(1L);

        partModel.setExpenseInfo(infoModel);
        partModel.setCar(carModel);
        partModel.setSerialNumber("123456");
        partModel.setName("Some part");
    }

    @Test
    public void expenseInfoDtoWillBeConvertedIntoModel() {
        infoModel.setId(null);

        doReturn(infoModel).when(infoMapper).dtoToModel(infoDto);

        ExpenseInfoModel result = partMapper.infoDtoToModel(infoDto);

        verify(infoMapper).dtoToModel(infoDto);

        assertSame(infoModel, result);
        assertNull(result.getId());
        assertEquals(100.0, result.getSum());
        assertEquals(UAH, result.getCurrency());
        assertEquals("Name", result.getCompanyName());
        assertEquals("Address", result.getCompanyAddress());
        assertEquals("Description", result.getDescription());
    }

    @Test
    public void expenseInfoWillBeConvertedIntoEntity() {
        doReturn(infoEntity).when(infoMapper).modelToEntity(infoModel);

        ExpenseInfoEntity result = partMapper.infoModelToEntity(infoModel);

        verify(infoMapper).modelToEntity(infoModel);

        assertSame(infoEntity, result);
        assertEquals(1L, result.getId());
        assertEquals(100.0, result.getSum());
        assertEquals(UAH, result.getCurrency());
        assertEquals("Name", result.getCompanyName());
        assertEquals("Address", result.getCompanyAddress());
        assertEquals("Description", result.getDescription());
    }

    @Test
    public void expenseInfoWillBeConvertedIntoModel() {
        doReturn(infoModel).when(infoMapper).entityToModel(infoEntity);

        ExpenseInfoModel result = partMapper.infoEntityToModel(infoEntity);

        verify(infoMapper).entityToModel(infoEntity);

        assertSame(infoModel, result);
        assertEquals(1L, result.getId());
        assertEquals(100.0, result.getSum());
        assertEquals(UAH, result.getCurrency());
        assertEquals("Name", result.getCompanyName());
        assertEquals("Address", result.getCompanyAddress());
        assertEquals("Description", result.getDescription());
    }

    @Test
    public void expenseInfoWillBeConvertedIntoDto() {
        doReturn(infoDto).when(infoMapper).modelToDto(infoModel);

        ExpenseInfoDto result = partMapper.infoModelToDto(infoModel);

        verify(infoMapper).modelToDto(infoModel);

        assertSame(infoDto, result);
        assertEquals(100.0, result.getSum());
        assertEquals(UAH, result.getCurrency());
        assertEquals("Name", result.getCompanyName());
        assertEquals("Address", result.getCompanyAddress());
        assertEquals("Description", result.getDescription());
    }

    @Test
    public void pageOfReplacedPartsWillBeConvertedIntoDto() {
        ReplacedPartModel part1 = new ReplacedPartModel();
        ReplacedPartModel part2 = new ReplacedPartModel();
        part1.setId(1L);
        part1.setSerialNumber("12345");
        part1.setName("Part 1");
        part1.setExpenseInfo(infoModel);
        part2.setId(2L);
        part2.setSerialNumber("56789");
        part2.setName("Part 2");
        part2.setExpenseInfo(infoModel);
        Page<? extends ReplacedPart> parts = new PageImpl<>(Arrays.asList(part1, part2));

        doReturn(infoDto).when(infoMapper).modelToDto(infoModel);

        Page<ReplacedPartDto> result = partMapper.pageOfReplacedPartsToDto(parts);

        verify(infoMapper, times(2)).modelToDto(infoModel);

        assertEquals(2, result.getContent().size());
        assertEquals("12345", result.getContent().get(0).getSerialNumber());
        assertEquals("Part 1", result.getContent().get(0).getName());
        assertNotNull(result.getContent().get(0).getReplacedPartInfo());
        assertEquals("56789", result.getContent().get(1).getSerialNumber());
        assertEquals("Part 2", result.getContent().get(1).getName());
        assertNotNull(result.getContent().get(1).getReplacedPartInfo());
    }

    @Test
    public void nullWillBeReturnedWhenGivenNullAsPageOfReplacedParts() {
        Page<ReplacedPartDto> result = partMapper.pageOfReplacedPartsToDto(null);

        verifyNoInteractions(infoMapper);

        assertNull(result);
    }

    @Test
    public void emptyPageWillBeReturnedWhenGivenEmptyPageOfReplacedParts() {
        Page<ReplacedPartDto> result = partMapper.pageOfReplacedPartsToDto(new PageImpl<>(Collections.emptyList()));

        verifyNoInteractions(infoMapper);

        assertTrue(result.getContent().isEmpty());
    }

    @Test
    public void modelWillBeConvertedIntoNewReplacedPartDto() {
        doReturn(infoDto).when(infoMapper).modelToDto(infoModel);

        NewReplacedPartDto result = partMapper.modelToNewDto(partModel);

        verify(infoMapper).modelToDto(infoModel);

        assertEquals("123456", result.getSerialNumber());
        assertEquals("Some part", result.getName());
        assertEquals(1L, result.getCarId());
        assertEquals(100.0, result.getReplacedPartInfo().getSum());
        assertEquals(UAH, result.getReplacedPartInfo().getCurrency());
        assertEquals("Name", result.getReplacedPartInfo().getCompanyName());
        assertEquals("Address", result.getReplacedPartInfo().getCompanyAddress());
        assertEquals("Description", result.getReplacedPartInfo().getDescription());
    }

    @Test
    public void nullWillBeReturnedWhenGivenNullReplacedPartModel() {
        NewReplacedPartDto result = partMapper.modelToNewDto(null);

        verifyNoInteractions(infoMapper);

        assertNull(result);
    }
}