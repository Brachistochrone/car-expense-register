package com.likhomanov.services.mappers;

import com.likhomanov.dao.entities.UserRoleEntity;
import com.likhomanov.enums.Role;
import com.likhomanov.model.UserModel;
import com.likhomanov.model.UserRole;
import com.likhomanov.model.UserRoleModel;
import com.likhomanov.web.dto.AuthenticatedUserDto;
import com.likhomanov.web.dto.NewUserDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static com.likhomanov.enums.Role.ROLE_USER;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserMapperTest {

    @Mock
    private BCryptPasswordEncoder passwordEncoder;
    @Mock
    private UserRoleMapper roleMapper;
    @InjectMocks
    private final UserMapper userMapper = new UserMapperImpl();
    private final NewUserDto newUserDto = new NewUserDto();
    private final UserRoleModel roleModel = new UserRoleModel();
    private final UserRoleEntity roleEntity = new UserRoleEntity();

    @BeforeEach
    public void init() {
        newUserDto.setEmail("email");
        newUserDto.setPassword("password");

        roleModel.setId(1L);
        roleModel.setRole(ROLE_USER);

        roleEntity.setId(1L);
        roleEntity.setRole(ROLE_USER);
    }

    @Test
    public void newUserDtoWillBeConvertedIntoModel() {
        doReturn("encoded password").when(passwordEncoder).encode("password");

        UserModel result = userMapper.dtoToModel(newUserDto);

        verify(passwordEncoder).encode("password");
        verifyNoInteractions(roleMapper);

        assertNotNull(result);
        assertEquals("email", result.getEmail());
        assertEquals("encoded password", result.getPassword());
    }

    @Test
    public void nullWillBeReturnedWhenGivenNullNewUserDto() {
        UserModel result = userMapper.dtoToModel(null);

        verifyNoInteractions(passwordEncoder);
        verifyNoInteractions(roleMapper);

        assertNull(result);
    }

    @Test
    public void setOfUserRolesWillBeConvertedIntoEntities() {
        Set<? extends UserRole> roles = Collections.singleton(roleModel);
        Set<UserRoleEntity> entities = Collections.singleton(roleEntity);

        doReturn(entities).when(roleMapper).rolesToEntities(roles);

        Set<UserRoleEntity> result = userMapper.rolesToEntities(roles);

        verify(roleMapper).rolesToEntities(roles);

        assertSame(entities, result);
        assertEquals(1, result.size());
        assertTrue(result.contains(roleEntity));
    }

    @Test
    public void setOfUserRolesWillBeConvertedIntoModels() {
        Set<? extends UserRole> roles = Collections.singleton(roleEntity);
        Set<UserRoleModel> models = Collections.singleton(roleModel);

        doReturn(models).when(roleMapper).rolesToModels(roles);

        Set<UserRoleModel> result = userMapper.rolesToModels(roles);

        verify(roleMapper).rolesToModels(roles);

        assertSame(models, result);
        assertEquals(1, result.size());
        assertTrue(result.contains(roleModel));
    }

    @Test
    public void springUserWillBeConvertedIntoAuthenticatedUserDto() {
        Collection<GrantedAuthority> authorities = Collections.emptyList();
        Set<Role> roles = new HashSet<>();
        roles.add(ROLE_USER);
        User user = new User("email", "password", authorities);

        doReturn(roles).when(roleMapper).authoritiesToRoles(anyCollection());

        AuthenticatedUserDto result = userMapper.springUserToAuthenticatedUserDto(user);

        verify(roleMapper).authoritiesToRoles(anyCollection());

        assertNotNull(result);
        assertEquals("email", result.getEmail());
        assertEquals("password", result.getPassword());
        assertEquals(1, result.getRoles().size());
        assertTrue(result.getRoles().contains(ROLE_USER));
    }

    @Test
    public void nullWillBeReturnedWhenGivenNullSpringUser() {
        AuthenticatedUserDto result = userMapper.springUserToAuthenticatedUserDto(null);

        verifyNoInteractions(roleMapper);

        assertNull(result);
    }
}