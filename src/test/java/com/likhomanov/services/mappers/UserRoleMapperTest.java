package com.likhomanov.services.mappers;

import com.likhomanov.dao.entities.UserRoleEntity;
import com.likhomanov.enums.Role;
import com.likhomanov.model.UserRole;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import static com.likhomanov.enums.Role.ROLE_USER;
import static org.junit.jupiter.api.Assertions.assertEquals;

class UserRoleMapperTest {

    private final UserRoleMapper roleMapper = new UserRoleMapperImpl();
    private final UserRoleEntity role = new UserRoleEntity();
    private final GrantedAuthority authority = new SimpleGrantedAuthority(ROLE_USER.name());

    @BeforeEach
    public void init() {
        role.setRole(ROLE_USER);
    }

    @Test
    public void userRolesWillBeConvertedIntoGrantedAuthorities() {
        Set<? extends UserRole> roles = Collections.singleton(role);

        Set<? extends GrantedAuthority> result = roleMapper.userRolesToAuthorities(roles);

        assertEquals(1, result.size());
        assertEquals(role.getRole().name(), result.stream().findFirst().get().getAuthority());
    }

    @Test
    public void userRoleWillBeConvertedIntoGrantedAuthority() {
        GrantedAuthority result = roleMapper.userRoleToAuthority(role);

        assertEquals(role.getRole().name(), result.getAuthority());
    }

    @Test
    public void grantedAuthoritiesWillBeConvertedIntoRoles() {
        Collection<GrantedAuthority> authorities = Collections.singleton(authority);

        Set<Role> result = roleMapper.authoritiesToRoles(authorities);

        assertEquals(1, result.size());
        assertEquals(authority.getAuthority(), result.stream().findFirst().get().name());
    }

    @Test
    public void rolesWillBeConvertedIntoGrantedAuthorities() {
        Set<Role> roles = Collections.singleton(ROLE_USER);

        Set<? extends GrantedAuthority> result = roleMapper.rolesToAuthorities(roles);

        assertEquals(1, result.size());
        assertEquals(ROLE_USER.name(), result.stream().findFirst().get().getAuthority());
    }

    @Test
    public void roleWillBeConvertedIntoGrantedAuthority() {
        GrantedAuthority result = roleMapper.roleToAuthority(ROLE_USER);

        assertEquals(ROLE_USER.name(), result.getAuthority());
    }
}