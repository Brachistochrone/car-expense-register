package com.likhomanov.services.validators;

import com.likhomanov.exceptions.ValidationException;
import com.likhomanov.web.dto.AuthenticatedUserDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashSet;

import static com.likhomanov.enums.Role.ROLE_USER;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AuthenticatedUserValidatorTest {

    private final AuthenticatedUserValidator validator = new AuthenticatedUserValidator();
    private final AuthenticatedUserDto userDto = new AuthenticatedUserDto();

    @BeforeEach
    public void init() {
        userDto.setEmail("some_email@gmail.com");
        userDto.setRoles(Collections.singleton(ROLE_USER));
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenAuthenticatedUserDtoWithValidData() {
        assertDoesNotThrow(() -> validator.validate(userDto));
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenAuthenticatedUserDtoWithBlankPassword() {
        userDto.setPassword("");

        assertDoesNotThrow(() -> validator.validate(userDto));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenAuthenticatedUserDtoWithNullEmail() {
        userDto.setEmail(null);

        assertThrows(ValidationException.class, () -> validator.validate(userDto));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenAuthenticatedUserDtoWithBlankEmail() {
        userDto.setEmail("");

        assertThrows(ValidationException.class, () -> validator.validate(userDto));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenAuthenticatedUserDtoWithInvalidFormatEmail() {
        userDto.setEmail("some_email.com");

        assertThrows(ValidationException.class, () -> validator.validate(userDto));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenAuthenticatedUserDtoWithPassword() {
        userDto.setPassword("qwerty");

        assertThrows(ValidationException.class, () -> validator.validate(userDto));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenAuthenticatedUserDtoWithNullRoles() {
        userDto.setRoles(null);

        assertThrows(ValidationException.class, () -> validator.validate(userDto));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenAuthenticatedUserDtoWithEmptyRoles() {
        userDto.setRoles(new HashSet<>());

        assertThrows(ValidationException.class, () -> validator.validate(userDto));
    }
}