package com.likhomanov.services.validators;

import com.likhomanov.exceptions.ValidationException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CarIdValidatorTest {

    private final CarIdValidator carIdValidator = new CarIdValidator();

    @Test
    public void validationExceptionWillBeThrownWhenGivenNullCarId() {
        assertThrows(ValidationException.class, () -> carIdValidator.validate(null));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarIdLessThanOne() {
        assertThrows(ValidationException.class, () -> carIdValidator.validate(0L));
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenValidCarId() {
        assertDoesNotThrow(() -> carIdValidator.validate(1L));
        assertDoesNotThrow(() -> carIdValidator.validate(2L));
    }
}