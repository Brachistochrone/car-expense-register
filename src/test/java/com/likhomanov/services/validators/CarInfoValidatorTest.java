package com.likhomanov.services.validators;

import com.likhomanov.dao.repositories.CarInfoDao;
import com.likhomanov.exceptions.NotUniqueEntityException;
import com.likhomanov.exceptions.ValidationException;
import com.likhomanov.web.dto.CarInfoDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CarInfoValidatorTest {

    @Mock
    private CarInfoDao carInfoDao;
    @InjectMocks
    private CarInfoValidator carInfoValidator;
    private final CarInfoDto carInfo = new CarInfoDto();

    @BeforeEach
    public void init() {
        carInfo.setBody("Sport coupe");
        carInfo.setColor("Rotten chery");
        carInfo.setModel("Badass model 3000");
        carInfo.setVinCode("A1234");
        carInfo.setYear("2020");
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenValidCarInfo() {
        doReturn(false).when(carInfoDao).existsByVinCode(carInfo.getVinCode());

        assertDoesNotThrow(() -> carInfoValidator.validate(carInfo));

        verify(carInfoDao).existsByVinCode(carInfo.getVinCode());
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithBlankVinCode() {
        carInfo.setVinCode("");

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        verifyNoInteractions(carInfoDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithNullVinCode() {
        carInfo.setVinCode(null);

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        verifyNoInteractions(carInfoDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithBlankModel() {
        carInfo.setModel("");

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        verifyNoInteractions(carInfoDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithNullModel() {
        carInfo.setModel(null);

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        verifyNoInteractions(carInfoDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithBlankColor() {
        carInfo.setColor("");

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        verifyNoInteractions(carInfoDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithNullColor() {
        carInfo.setColor(null);

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        verifyNoInteractions(carInfoDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithBlankBody() {
        carInfo.setBody("");

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        verifyNoInteractions(carInfoDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithNullBody() {
        carInfo.setBody(null);

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        verifyNoInteractions(carInfoDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithBlankYear() {
        carInfo.setYear("");

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        verifyNoInteractions(carInfoDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithNullYear() {
        carInfo.setYear(null);

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        verifyNoInteractions(carInfoDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithVinCodeShorterThanFour() {
        carInfo.setVinCode("123");

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        verifyNoInteractions(carInfoDao);
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenCarInfoWithVinCodeOfFourCharacters() {
        carInfo.setVinCode("1234");

        doReturn(false).when(carInfoDao).existsByVinCode(carInfo.getVinCode());

        assertDoesNotThrow(() -> carInfoValidator.validate(carInfo));

        verify(carInfoDao).existsByVinCode(carInfo.getVinCode());
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithVinCodeOfTwoWords() {
        carInfo.setVinCode("A 1234");

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        verifyNoInteractions(carInfoDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithVinCodeWithUnsupportedCharacters() {
        carInfo.setVinCode("A1234$");

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        verifyNoInteractions(carInfoDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithModelShorterThanThree() {
        carInfo.setModel("GT");

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        verifyNoInteractions(carInfoDao);
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenCarInfoWithModelOfThreeCharacters() {
        carInfo.setModel("Rio");

        doReturn(false).when(carInfoDao).existsByVinCode(carInfo.getVinCode());

        assertDoesNotThrow(() -> carInfoValidator.validate(carInfo));

        verify(carInfoDao).existsByVinCode(carInfo.getVinCode());
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithModelWithUnsupportedCharacters() {
        carInfo.setModel("Rio%");

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        verifyNoInteractions(carInfoDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithColorShorterThanThree() {
        carInfo.setColor("Re");

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        verifyNoInteractions(carInfoDao);
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenCarInfoWithColorOfThreeCharacters() {
        carInfo.setColor("Red");

        doReturn(false).when(carInfoDao).existsByVinCode(carInfo.getVinCode());

        assertDoesNotThrow(() -> carInfoValidator.validate(carInfo));

        verify(carInfoDao).existsByVinCode(carInfo.getVinCode());
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithColorWithUnsupportedCharacters() {
        carInfo.setColor("Red%");

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        verifyNoInteractions(carInfoDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithBodyShorterThanFour() {
        carInfo.setBody("Sed");

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        verifyNoInteractions(carInfoDao);
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenCarInfoWithBodyOfFourCharacters() {
        carInfo.setBody("Mini");

        doReturn(false).when(carInfoDao).existsByVinCode(carInfo.getVinCode());

        assertDoesNotThrow(() -> carInfoValidator.validate(carInfo));

        verify(carInfoDao).existsByVinCode(carInfo.getVinCode());
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithBodyWithUnsupportedCharacters() {
        carInfo.setBody("Sedan%");

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        carInfo.setBody("Sedan5");

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        verifyNoInteractions(carInfoDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithYearShorterThanFour() {
        carInfo.setYear("20");

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        verifyNoInteractions(carInfoDao);
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenCarInfoWithYearOfFourCharacters() {
        carInfo.setYear("2020");

        doReturn(false).when(carInfoDao).existsByVinCode(carInfo.getVinCode());

        assertDoesNotThrow(() -> carInfoValidator.validate(carInfo));

        verify(carInfoDao).existsByVinCode(carInfo.getVinCode());
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithYearWithUnsupportedCharacters() {
        carInfo.setYear("2020-01-01");

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        carInfo.setYear("2020y");

        assertThrows(ValidationException.class, () -> carInfoValidator.validate(carInfo));

        verifyNoInteractions(carInfoDao);
    }

    @Test
    public void notUniqueEntityExceptionWillBeThrownWhenGivenCarInfoWithNotUniqueVinCode() {
        doReturn(true).when(carInfoDao).existsByVinCode(carInfo.getVinCode());

        assertThrows(NotUniqueEntityException.class, () -> carInfoValidator.validate(carInfo));

        verify(carInfoDao).existsByVinCode(carInfo.getVinCode());
    }
}