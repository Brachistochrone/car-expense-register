package com.likhomanov.services.validators;

import com.likhomanov.exceptions.ValidationException;
import com.likhomanov.web.dto.CarDto;
import com.likhomanov.web.dto.CarInfoDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CarValidatorTest {

    @Mock
    private CarInfoValidator infoValidator;
    private final CarDto carDto = new CarDto();
    private final CarInfoDto carInfoDto = new CarInfoDto();
    @InjectMocks
    private CarValidator carValidator;

    @Test
    public void validationWillBeSuccessfulWhenGivenCarDtoWithCarInfoDto() {
        carDto.setCarInfo(carInfoDto);

        doNothing().when(infoValidator).validate(carDto.getCarInfo());

        assertDoesNotThrow(() -> carValidator.validate(carDto));

        verify(infoValidator).validate(carInfoDto);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarDtoWithNullCarInfoDto() {
        carDto.setCarInfo(null);

        assertThrows(ValidationException.class, () -> carValidator.validate(carDto));

        verifyNoInteractions(infoValidator);
    }
}