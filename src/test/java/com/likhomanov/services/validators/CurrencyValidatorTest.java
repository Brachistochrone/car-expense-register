package com.likhomanov.services.validators;

import com.likhomanov.exceptions.ValidationException;
import org.junit.jupiter.api.Test;

import static com.likhomanov.enums.Currency.UAH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CurrencyValidatorTest {

    private final CurrencyValidator validator = new CurrencyValidator();

    @Test
    public void validationWillBeSuccessfulWhenGivenValidCurrency() {
        assertDoesNotThrow(() -> validator.validate(UAH));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNullCurrency() {
        assertThrows(ValidationException.class, () -> validator.validate(null));
    }
}