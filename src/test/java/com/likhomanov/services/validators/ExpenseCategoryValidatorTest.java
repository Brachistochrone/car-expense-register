package com.likhomanov.services.validators;

import com.likhomanov.exceptions.ValidationException;
import com.likhomanov.web.dto.ExpenseCategoryDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class ExpenseCategoryValidatorTest {

    @InjectMocks
    private ExpenseCategoryValidator categoryValidator;
    private final ExpenseCategoryDto category = new ExpenseCategoryDto();

    @Test
    public void validationExceptionWillBeThrownWhenGivenExpenseCategoryDtoWithNullExpenseType() {
        category.setExpenseType(null);

        assertThrows(ValidationException.class, () -> categoryValidator.validate(category));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenExpenseCategoryDtoWithEmptyExpenseType() {
        category.setExpenseType("");

        assertThrows(ValidationException.class, () -> categoryValidator.validate(category));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenExpenseCategoryDtoWithTooShortExpenseType() {
        category.setExpenseType("Ex");

        assertThrows(ValidationException.class, () -> categoryValidator.validate(category));
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenExpenseCategoryDtoWithExpenseTypeOfThreeCharacter() {
        category.setExpenseType("Exp");

        assertDoesNotThrow(() -> categoryValidator.validate(category));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenExpenseCategoryDtoWithExpenseTypeOfInvalidFormat() {
        category.setExpenseType("Repair2");

        assertThrows(ValidationException.class, () -> categoryValidator.validate(category));

        category.setExpenseType("Car repair");

        assertThrows(ValidationException.class, () -> categoryValidator.validate(category));

        category.setExpenseType("Car-repair");

        assertThrows(ValidationException.class, () -> categoryValidator.validate(category));
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenExpenseCategoryDtoWithValidExpenseType() {
        category.setExpenseType("Refuel");

        assertDoesNotThrow(() -> categoryValidator.validate(category));
    }
}