package com.likhomanov.services.validators;

import com.likhomanov.exceptions.ValidationException;
import com.likhomanov.web.dto.ExpenseCategoryDto;
import com.likhomanov.web.dto.ExpenseInfoDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static com.likhomanov.enums.Currency.UAH;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ExpenseInfoValidatorTest {

    @Mock
    private ExpenseCategoryValidator categoryValidator;
    private final ExpenseInfoDto infoDto = new ExpenseInfoDto();
    private final ExpenseCategoryDto categoryDto = new ExpenseCategoryDto();
    @InjectMocks
    private ExpenseInfoValidator infoValidator;

    @BeforeEach
    public void init() {
        infoDto.setExpenseCategory(categoryDto);
        infoDto.setDate(new Date());
        infoDto.setSum(0.01);
        infoDto.setCurrency(UAH);
        infoDto.setCompanyName("Some company");
        infoDto.setCompanyAddress("Some address");
        infoDto.setDescription("Some description");
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenExpenseInfoDtoWithValidData() {
        doNothing().when(categoryValidator).validate(categoryDto);

        infoValidator.validate(infoDto);

        verify(categoryValidator).validate(categoryDto);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenExpenseInfoDtoWithNullDate() {
        infoDto.setDate(null);

        assertThrows(ValidationException.class, () -> infoValidator.validate(infoDto));

        verifyNoInteractions(categoryValidator);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenExpenseInfoDtoWithNullSum() {
        infoDto.setSum(null);

        assertThrows(ValidationException.class, () -> infoValidator.validate(infoDto));

        verifyNoInteractions(categoryValidator);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenExpenseInfoDtoWithZeroSum() {
        infoDto.setSum(0.0);

        assertThrows(ValidationException.class, () -> infoValidator.validate(infoDto));

        verifyNoInteractions(categoryValidator);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenExpenseInfoDtoWithNullCurrency() {
        infoDto.setCurrency(null);

        assertThrows(ValidationException.class, () -> infoValidator.validate(infoDto));

        verifyNoInteractions(categoryValidator);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenExpenseInfoDtoWithNullCompanyName() {
        infoDto.setCompanyName(null);

        assertThrows(ValidationException.class, () -> infoValidator.validate(infoDto));

        verifyNoInteractions(categoryValidator);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenExpenseInfoDtoWithBlankCompanyName() {
        infoDto.setCompanyName("");

        assertThrows(ValidationException.class, () -> infoValidator.validate(infoDto));

        verifyNoInteractions(categoryValidator);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenExpenseInfoDtoWithNullCompanyAddress() {
        infoDto.setCompanyAddress(null);

        assertThrows(ValidationException.class, () -> infoValidator.validate(infoDto));

        verifyNoInteractions(categoryValidator);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenExpenseInfoDtoWithBlankCompanyAddress() {
        infoDto.setCompanyAddress("");

        assertThrows(ValidationException.class, () -> infoValidator.validate(infoDto));

        verifyNoInteractions(categoryValidator);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenExpenseInfoDtoWithNullDescription() {
        infoDto.setDescription(null);

        assertThrows(ValidationException.class, () -> infoValidator.validate(infoDto));

        verifyNoInteractions(categoryValidator);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenExpenseInfoDtoWithBlankDescription() {
        infoDto.setDescription("");

        assertThrows(ValidationException.class, () -> infoValidator.validate(infoDto));

        verifyNoInteractions(categoryValidator);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenExpenseInfoDtoWithNullExpenseCategory() {
        infoDto.setExpenseCategory(null);

        assertThrows(ValidationException.class, () -> infoValidator.validate(infoDto));

        verifyNoInteractions(categoryValidator);
    }
}