package com.likhomanov.services.validators;

import com.likhomanov.dao.repositories.ExpenseCategoryDao;
import com.likhomanov.exceptions.NotUniqueEntityException;
import com.likhomanov.web.dto.ExpenseCategoryDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class NewExpenseCategoryValidatorTest {

    @Mock
    private ExpenseCategoryValidator categoryValidator;
    @Mock
    private ExpenseCategoryDao categoryDao;
    @InjectMocks
    private NewExpenseCategoryValidator newCategoryValidator;
    private final ExpenseCategoryDto category = new ExpenseCategoryDto();

    @Test
    public void notUniqueEntityExceptionWillBeThrownWhenGivenExpenseCategoryDtoWithNotUniqueExpenseType() {
        category.setExpenseType("Refuel");

        doNothing().when(categoryValidator).validate(category);
        doReturn(true).when(categoryDao).existsByExpenseType(category.getExpenseType());

        assertThrows(NotUniqueEntityException.class, () -> newCategoryValidator.validate(category));

        verify(categoryValidator).validate(category);
        verify(categoryDao).existsByExpenseType(category.getExpenseType());
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenExpenseCategoryDtoWithValidExpenseType() {
        category.setExpenseType("Parking");

        doNothing().when(categoryValidator).validate(category);
        doReturn(false).when(categoryDao).existsByExpenseType(category.getExpenseType());

        assertDoesNotThrow(() -> newCategoryValidator.validate(category));

        verify(categoryValidator).validate(category);
        verify(categoryDao).existsByExpenseType(category.getExpenseType());
    }
}