package com.likhomanov.services.validators;

import com.likhomanov.dao.repositories.ReplacedPartDao;
import com.likhomanov.exceptions.NotUniqueEntityException;
import com.likhomanov.exceptions.ValidationException;
import com.likhomanov.web.dto.ExpenseInfoDto;
import com.likhomanov.web.dto.NewReplacedPartDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class NewReplacedPartValidatorTest {

    @Mock
    private CarIdValidator carIdValidator;
    @Mock
    private ExpenseInfoValidator infoValidator;
    @Mock
    private ReplacedPartDao partDao;
    private final NewReplacedPartDto part = new NewReplacedPartDto();
    private final ExpenseInfoDto partInfo = new ExpenseInfoDto();
    @InjectMocks
    private NewReplacedPartValidator partValidator;

    @BeforeEach
    public void init() {
        part.setCarId(1L);
        part.setName("Some spare part");
        part.setSerialNumber("W123");
        part.setReplacedPartInfo(partInfo);
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenNewReplacedPartDtoWithValidData() {
        doNothing().when(carIdValidator).validate(part.getCarId());
        doNothing().when(infoValidator).validate(part.getReplacedPartInfo());
        doReturn(false).when(partDao).existsBySerialNumber(part.getSerialNumber());

        assertDoesNotThrow(() -> partValidator.validate(part));

        verify(carIdValidator).validate(part.getCarId());
        verify(infoValidator).validate(part.getReplacedPartInfo());
        verify(partDao).existsBySerialNumber(part.getSerialNumber());
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNewReplacedPartDtoWithNullSerialNumber() {
        part.setSerialNumber(null);

        assertThrows(ValidationException.class, () -> partValidator.validate(part));

        verifyNoInteractions(carIdValidator);
        verifyNoInteractions(infoValidator);
        verifyNoInteractions(partDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNewReplacedPartDtoWithBlankSerialNumber() {
        part.setSerialNumber("");

        assertThrows(ValidationException.class, () -> partValidator.validate(part));

        verifyNoInteractions(carIdValidator);
        verifyNoInteractions(infoValidator);
        verifyNoInteractions(partDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNewReplacedPartDtoWithNullName() {
        part.setName(null);

        assertThrows(ValidationException.class, () -> partValidator.validate(part));

        verifyNoInteractions(carIdValidator);
        verifyNoInteractions(infoValidator);
        verifyNoInteractions(partDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNewReplacedPartDtoWithBlankName() {
        part.setName("");

        assertThrows(ValidationException.class, () -> partValidator.validate(part));

        verifyNoInteractions(carIdValidator);
        verifyNoInteractions(infoValidator);
        verifyNoInteractions(partDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNewReplacedPartDtoWithNullPartInfo() {
        part.setReplacedPartInfo(null);

        assertThrows(ValidationException.class, () -> partValidator.validate(part));

        verifyNoInteractions(carIdValidator);
        verifyNoInteractions(infoValidator);
        verifyNoInteractions(partDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNewReplacedPartDtoWithTooShortSerialNumber() {
        part.setSerialNumber("123");

        assertThrows(ValidationException.class, () -> partValidator.validate(part));

        verifyNoInteractions(carIdValidator);
        verifyNoInteractions(infoValidator);
        verifyNoInteractions(partDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNewReplacedPartDtoWithSerialNumberWithUnsupportedCharacters() {
        part.setSerialNumber("W124$");

        assertThrows(ValidationException.class, () -> partValidator.validate(part));

        verifyNoInteractions(carIdValidator);
        verifyNoInteractions(infoValidator);
        verifyNoInteractions(partDao);
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenNewReplacedPartDtoWithSerialNumberConsistingOfSeveralWords() {
        part.setSerialNumber("W 123");

        doNothing().when(carIdValidator).validate(part.getCarId());
        doNothing().when(infoValidator).validate(part.getReplacedPartInfo());
        doReturn(false).when(partDao).existsBySerialNumber(part.getSerialNumber());

        assertDoesNotThrow(() -> partValidator.validate(part));

        verify(carIdValidator).validate(part.getCarId());
        verify(infoValidator).validate(part.getReplacedPartInfo());
        verify(partDao).existsBySerialNumber(part.getSerialNumber());
    }

    @Test
    public void notUniqueEntityExceptionWillBeThrownWhenGivenNewReplacedPartDtoWithNotUniqueSerialNumber() {
        doNothing().when(carIdValidator).validate(part.getCarId());
        doNothing().when(infoValidator).validate(part.getReplacedPartInfo());
        doReturn(true).when(partDao).existsBySerialNumber(part.getSerialNumber());

        assertThrows(NotUniqueEntityException.class, () -> partValidator.validate(part));

        verify(carIdValidator).validate(part.getCarId());
        verify(infoValidator).validate(part.getReplacedPartInfo());
        verify(partDao).existsBySerialNumber(part.getSerialNumber());
    }
}