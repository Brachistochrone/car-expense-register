package com.likhomanov.services.validators;

import com.likhomanov.dao.repositories.UserDao;
import com.likhomanov.exceptions.NotUniqueEntityException;
import com.likhomanov.exceptions.ValidationException;
import com.likhomanov.web.dto.NewUserDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class NewUserValidatorTest {

    @Mock
    private UserDao userDao;
    @InjectMocks
    private NewUserValidator userValidator;
    private final NewUserDto userDto = new NewUserDto();

    @BeforeEach
    public void init() {
        userDto.setEmail("test_email@gmail.com");
        userDto.setPassword("qwe123");
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenNewUserDtoWithValidData() {
        doReturn(false).when(userDao).existsByEmail(userDto.getEmail());

        assertDoesNotThrow(() -> userValidator.validate(userDto));

        verify(userDao).existsByEmail(userDto.getEmail());
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNewUserDtoWithNullEmail() {
        userDto.setEmail(null);

        assertThrows(ValidationException.class, () -> userValidator.validate(userDto));

        verifyNoInteractions(userDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNewUserDtoWithBlankEmail() {
        userDto.setEmail("");

        assertThrows(ValidationException.class, () -> userValidator.validate(userDto));

        verifyNoInteractions(userDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNewUserDtoWithInvalidFormatEmail() {
        userDto.setEmail("bad_email.com");

        assertThrows(ValidationException.class, () -> userValidator.validate(userDto));

        verifyNoInteractions(userDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNewUserDtoWithNullPassword() {
        userDto.setPassword(null);

        assertThrows(ValidationException.class, () -> userValidator.validate(userDto));

        verifyNoInteractions(userDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNewUserDtoWithBlankPassword() {
        userDto.setPassword("");

        assertThrows(ValidationException.class, () -> userValidator.validate(userDto));

        verifyNoInteractions(userDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNewUserDtoWithTooShortPassword() {
        userDto.setPassword("12345");

        assertThrows(ValidationException.class, () -> userValidator.validate(userDto));

        verifyNoInteractions(userDao);
    }

    @Test
    public void notUniqueEntityExceptionWillBeThrownWhenGivenNewUserDtoWithNotUniqueEmail() {
        doReturn(true).when(userDao).existsByEmail(userDto.getEmail());

        assertThrows(NotUniqueEntityException.class, () -> userValidator.validate(userDto));

        verify(userDao).existsByEmail(userDto.getEmail());
    }
}