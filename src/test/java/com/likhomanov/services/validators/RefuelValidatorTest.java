package com.likhomanov.services.validators;

import com.likhomanov.exceptions.ValidationException;
import com.likhomanov.web.dto.ExpenseInfoDto;
import com.likhomanov.web.dto.RefuelDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RefuelValidatorTest {

    @Mock
    private ExpenseInfoValidator infoValidator;
    @Mock
    private CarIdValidator carIdValidator;
    @InjectMocks
    private RefuelValidator refuelValidator;
    private final RefuelDto refuelDto = new RefuelDto();
    private final ExpenseInfoDto refuelInfo = new ExpenseInfoDto();

    @BeforeEach
    public void init() {
        refuelDto.setCarId(1L);
        refuelDto.setVolume(0.01);
        refuelDto.setRefuelInfo(refuelInfo);
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenRefuelDtoWithValidData() {
        doNothing().when(infoValidator).validate(refuelDto.getRefuelInfo());
        doNothing().when(carIdValidator).validate(refuelDto.getCarId());

        assertDoesNotThrow(() -> refuelValidator.validate(refuelDto));

        verify(infoValidator).validate(refuelDto.getRefuelInfo());
        verify(carIdValidator).validate(refuelDto.getCarId());
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenRefuelDtoWithNullVolume() {
        refuelDto.setVolume(null);

        assertThrows(ValidationException.class, () -> refuelValidator.validate(refuelDto));

        verifyNoInteractions(infoValidator);
        verifyNoInteractions(carIdValidator);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenRefuelDtoWithZeroVolume() {
        refuelDto.setVolume(0.0);

        assertThrows(ValidationException.class, () -> refuelValidator.validate(refuelDto));

        verifyNoInteractions(infoValidator);
        verifyNoInteractions(carIdValidator);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenRefuelDtoWithNullRefuelInfo() {
        refuelDto.setRefuelInfo(null);

        assertThrows(ValidationException.class, () -> refuelValidator.validate(refuelDto));

        verifyNoInteractions(infoValidator);
        verifyNoInteractions(carIdValidator);
    }
}