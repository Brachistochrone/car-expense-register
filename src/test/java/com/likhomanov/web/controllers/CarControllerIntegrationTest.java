package com.likhomanov.web.controllers;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.likhomanov.dao.entities.CarEntity;
import com.likhomanov.dao.entities.CarInfoEntity;
import com.likhomanov.dao.repositories.CarDao;
import com.likhomanov.dao.repositories.CarInfoDao;
import com.likhomanov.model.Car;
import com.likhomanov.web.dto.CarDto;
import com.likhomanov.web.dto.CarInfoDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CarControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CarDao carDao;
    @MockBean
    private CarInfoDao carInfoDao;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final CarDto newCar = new CarDto();
    private final CarInfoDto carInfo = new CarInfoDto();
    private final List<Car> cars = new ArrayList<>();
    private final CarEntity savedCar = new CarEntity();
    private final CarInfoEntity savedCarInfo = new CarInfoEntity();

    @BeforeEach
    public void init() {
        carInfo.setVinCode("123456");
        carInfo.setModel("Some model 3000");
        carInfo.setBody("Sedan");
        carInfo.setColor("Red");
        carInfo.setYear("2010");

        newCar.setCarInfo(carInfo);

        savedCarInfo.setVinCode("123456");
        savedCarInfo.setModel("Some model 3000");
        savedCarInfo.setBody("Sedan");
        savedCarInfo.setColor("Red");
        savedCarInfo.setYear("2010");

        savedCar.setId(1L);
        savedCar.setCarInfo(savedCarInfo);

        if (cars.isEmpty()) {
            cars.add(new CarEntity());
            cars.add(new CarEntity());
            cars.add(new CarEntity());
            cars.add(new CarEntity());
        }
    }

    @Test
    @WithMockUser
    public void newCarWillBeSavedAndDtoWithIdWillBeReturnedWhenGivenCarDtoWithValidDataByAuthorizedUser() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newCar);

        doReturn(false).when(carInfoDao).existsByVinCode(carInfo.getVinCode());
        doReturn(savedCar).when(carDao).save(any(CarEntity.class));

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        CarDto responseDto = objectMapper.readValue(response.getContentAsString(), CarDto.class);

        verify(carInfoDao).existsByVinCode(carInfo.getVinCode());
        verify(carDao).save(any(CarEntity.class));

        assertEquals(1L, responseDto.getId());
        assertEquals("123456", responseDto.getCarInfo().getVinCode());
        assertEquals("Some model 3000", responseDto.getCarInfo().getModel());
        assertEquals("Red", responseDto.getCarInfo().getColor());
        assertEquals("Sedan", responseDto.getCarInfo().getBody());
        assertEquals("2010", responseDto.getCarInfo().getYear());
    }

    @Test
    public void accessWillBeDeniedWhenGivenCarDtoWithValidDataByUnauthorizedUser() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newCar);

        mockMvc.perform(post("/cars")
                                .contentType(APPLICATION_JSON)
                                .content(requestBody))
               .andExpect(status().isForbidden());

        verifyNoInteractions(carDao);
        verifyNoInteractions(carInfoDao);
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithNullCarInfo() throws Exception {
        newCar.setCarInfo(null);
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);
        verifyNoInteractions(carInfoDao);

        assertEquals("Car info is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithNullVinCode() throws Exception {
        newCar.getCarInfo().setVinCode(null);
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);
        verifyNoInteractions(carInfoDao);

        assertEquals("Car's vin code is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithBlankVinCode() throws Exception {
        newCar.getCarInfo().setVinCode("");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);
        verifyNoInteractions(carInfoDao);

        assertEquals("Car's vin code is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithTooShortVinCode() throws Exception {
        newCar.getCarInfo().setVinCode("123");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);
        verifyNoInteractions(carInfoDao);

        assertEquals("Invalid car's vin code format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithInvalidFormatVinCode() throws Exception {
        newCar.getCarInfo().setVinCode("# QS4586");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);
        verifyNoInteractions(carInfoDao);

        assertEquals("Invalid car's vin code format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithNullModel() throws Exception {
        newCar.getCarInfo().setModel(null);
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);
        verifyNoInteractions(carInfoDao);

        assertEquals("Car's model is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithBlankModel() throws Exception {
        newCar.getCarInfo().setModel("");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);
        verifyNoInteractions(carInfoDao);

        assertEquals("Car's model is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithTooShortModel() throws Exception {
        newCar.getCarInfo().setModel("UV");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);
        verifyNoInteractions(carInfoDao);

        assertEquals("Invalid car's model format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithInvalidFormatModel() throws Exception {
        newCar.getCarInfo().setModel("Badass_car_model");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);
        verifyNoInteractions(carInfoDao);

        assertEquals("Invalid car's model format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithNullColor() throws Exception {
        newCar.getCarInfo().setColor(null);
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);
        verifyNoInteractions(carInfoDao);

        assertEquals("Car's color is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithBlankColor() throws Exception {
        newCar.getCarInfo().setColor("");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);
        verifyNoInteractions(carInfoDao);

        assertEquals("Car's color is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithTooShortColor() throws Exception {
        newCar.getCarInfo().setColor("Re");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);
        verifyNoInteractions(carInfoDao);

        assertEquals("Invalid car's color format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithInvalidFormatColor() throws Exception {
        newCar.getCarInfo().setColor("Wet asphalt 18");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);
        verifyNoInteractions(carInfoDao);

        assertEquals("Invalid car's color format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithNullBody() throws Exception {
        newCar.getCarInfo().setBody(null);
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);
        verifyNoInteractions(carInfoDao);

        assertEquals("Car's body type is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithBlankBody() throws Exception {
        newCar.getCarInfo().setBody("");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);
        verifyNoInteractions(carInfoDao);

        assertEquals("Car's body type is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithTooShortBody() throws Exception {
        newCar.getCarInfo().setBody("Sed");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);
        verifyNoInteractions(carInfoDao);

        assertEquals("Invalid car's body type format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithInvalidFormatBody() throws Exception {
        newCar.getCarInfo().setBody("Sedan 2");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);
        verifyNoInteractions(carInfoDao);

        assertEquals("Invalid car's body type format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithNullYear() throws Exception {
        newCar.getCarInfo().setYear(null);
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);
        verifyNoInteractions(carInfoDao);

        assertEquals("Car's production year is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithBlankYear() throws Exception {
        newCar.getCarInfo().setYear("");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);
        verifyNoInteractions(carInfoDao);

        assertEquals("Car's production year is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithTooShortYear() throws Exception {
        newCar.getCarInfo().setYear("20");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);
        verifyNoInteractions(carInfoDao);

        assertEquals("Invalid car's year format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithInvalidFormatYear() throws Exception {
        newCar.getCarInfo().setYear("2020 year");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);
        verifyNoInteractions(carInfoDao);

        assertEquals("Invalid car's year format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithNotUniqueVinCode() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newCar);

        doReturn(true).when(carInfoDao).existsByVinCode(carInfo.getVinCode());

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verify(carInfoDao).existsByVinCode(carInfo.getVinCode());
        verifyNoInteractions(carDao);

        assertEquals("Car with such vin code already exists", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void pageOfCarDtosWillBeReturnedWhenGivenRequestByAuthorizedUser() throws Exception {
        doAnswer(invocationOnMock -> {
            Pageable pageable = invocationOnMock.getArgument(0);
            return new PageImpl<>(cars, pageable, cars.size());
        }).when(carDao).findAll(any(Pageable.class));

        MockHttpServletResponse response = mockMvc.perform(get("/cars")
                                                                   .param("page", "0")
                                                                   .param("size", "4")
                                                                   .param("sort", ""))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        Page<CarDto> carDtos = objectMapper.readValue(response.getContentAsString(),
                                                      new TypeReference<TestPageImpl<CarDto>>() {});

        verify(carDao).findAll(any(Pageable.class));
        verifyNoInteractions(carInfoDao);

        assertEquals(4, carDtos.getTotalElements());
        assertEquals(4, carDtos.getContent().size());
        assertEquals("UNSORTED", carDtos.getSort().toString());
        assertEquals(0, carDtos.getPageable().getPageNumber());
        assertEquals(4, carDtos.getPageable().getPageSize());
    }

    @Test
    @WithMockUser
    public void emptyPageOfCarDtosWillBeReturnedWhenThereIsNoCarsInDb() throws Exception {
        cars.clear();

        doAnswer(invocationOnMock -> {
            Pageable pageable = invocationOnMock.getArgument(0);
            return new PageImpl<>(cars, pageable, cars.size());
        }).when(carDao).findAll(any(Pageable.class));

        MockHttpServletResponse response = mockMvc.perform(get("/cars")
                                                                   .param("page", "0")
                                                                   .param("size", "4")
                                                                   .param("sort", ""))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        Page<CarDto> carDtos = objectMapper.readValue(response.getContentAsString(),
                                                      new TypeReference<TestPageImpl<CarDto>>() {});

        verify(carDao).findAll(any(Pageable.class));
        verifyNoInteractions(carInfoDao);

        assertEquals(0, carDtos.getTotalElements());
        assertEquals(0, carDtos.getContent().size());
        assertEquals("UNSORTED", carDtos.getSort().toString());
        assertEquals(0, carDtos.getPageable().getPageNumber());
        assertEquals(4, carDtos.getPageable().getPageSize());
    }

    @Test
    public void accessWillBeDeniedWhenGivenRequestByUnauthorizedUser() throws Exception {
        mockMvc.perform(get("/cars"))
               .andExpect(status().isForbidden());

        verifyNoInteractions(carDao);
    }

    private static final class TestPageImpl<T> extends PageImpl<T> {

        private static final long serialVersionUID = 3248189030448292002L;

        @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
        public TestPageImpl(@JsonProperty("content") List<T> content,
                            @JsonProperty("number") int number,
                            @JsonProperty("size") int size,
                            @JsonProperty("totalElements") Long totalElements,
                            @JsonProperty("pageable") JsonNode pageable,
                            @JsonProperty("last") boolean last,
                            @JsonProperty("totalPages") int totalPages,
                            @JsonProperty("sort") JsonNode sort,
                            @JsonProperty("first") boolean first,
                            @JsonProperty("numberOfElements") int numberOfElements,
                            @JsonProperty("empty") boolean empty) {
            super(content, PageRequest.of(number, size), totalElements);
        }

        public TestPageImpl(List<T> content, Pageable pageable, long total) {
            super(content, pageable, total);
        }

        public TestPageImpl(List<T> content) {
            super(content);
        }

        public TestPageImpl() {
            super(new ArrayList<T>());
        }
    }
}