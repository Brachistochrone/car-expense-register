package com.likhomanov.web.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.likhomanov.dao.entities.ExpenseCategoryEntity;
import com.likhomanov.dao.repositories.ExpenseCategoryDao;
import com.likhomanov.model.ExpenseCategory;
import com.likhomanov.web.dto.ExpenseCategoryDto;
import com.likhomanov.web.dto.ListOfExpenseCategoriesDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ExpenseCategoryControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ExpenseCategoryDao expenseCategoryDao;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final ExpenseCategoryDto newExpenseCategory = new ExpenseCategoryDto();
    private final List<ExpenseCategory> allCategories = new ArrayList<>();
    private final ExpenseCategoryEntity repairCategory = new ExpenseCategoryEntity();
    private final ExpenseCategoryEntity refuelCategory = new ExpenseCategoryEntity();

    @BeforeEach
    public void init() {
        newExpenseCategory.setExpenseType("PARKING");

        if (allCategories.isEmpty()) {
            repairCategory.setExpenseType("REPAIR");
            refuelCategory.setExpenseType("REFUEL");
            allCategories.add(repairCategory);
            allCategories.add(refuelCategory);
        }
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    public void newExpenseCategoryWillBeSavedWhenGivenDtoWithValidExpenseTypeByAuthorizedAdmin() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newExpenseCategory);

        doReturn(false).when(expenseCategoryDao).existsByExpenseType(newExpenseCategory.getExpenseType());
        doAnswer(invocationOnMock -> {
            ExpenseCategoryEntity savedCategory = invocationOnMock.getArgument(0);
            savedCategory.setId(1L);
            return savedCategory;
        }).when(expenseCategoryDao).save(any(ExpenseCategoryEntity.class));

        MockHttpServletResponse response = mockMvc.perform(post("/expense_categories")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        ExpenseCategoryDto responseDto = objectMapper.readValue(response.getContentAsString(),
                                                                ExpenseCategoryDto.class);

        verify(expenseCategoryDao).existsByExpenseType(newExpenseCategory.getExpenseType());
        verify(expenseCategoryDao).save(any(ExpenseCategoryEntity.class));

        assertEquals(1L, responseDto.getId());
        assertEquals("PARKING", responseDto.getExpenseType());
    }

    @Test
    @WithMockUser
    public void accessWillBeDeniedWhenGivenExpenseCategoryDtoWithValidExpenseTypeByAuthorizedUser() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newExpenseCategory);

        mockMvc.perform(post("/expense_categories")
                                .contentType(APPLICATION_JSON)
                                .content(requestBody))
               .andExpect(status().isForbidden());

        verifyNoInteractions(expenseCategoryDao);
    }

    @Test
    public void accessWillBeDeniedWhenGivenExpenseCategoryDtoWithValidExpenseTypeByUnauthorizedUser() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newExpenseCategory);

        mockMvc.perform(post("/expense_categories")
                                .contentType(APPLICATION_JSON)
                                .content(requestBody))
               .andExpect(status().isForbidden());

        verifyNoInteractions(expenseCategoryDao);
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    public void clientErrorWillBeReturnedWhenGivenExpenseCategoryDtoWithNullExpenseType() throws Exception {
        newExpenseCategory.setExpenseType(null);
        String requestBody = objectMapper.writeValueAsString(newExpenseCategory);

        MockHttpServletResponse response = mockMvc.perform(post("/expense_categories")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(expenseCategoryDao);

        assertEquals("Expense type is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    public void clientErrorWillBeReturnedWhenGivenExpenseCategoryDtoWithBlankExpenseType() throws Exception {
        newExpenseCategory.setExpenseType("");
        String requestBody = objectMapper.writeValueAsString(newExpenseCategory);

        MockHttpServletResponse response = mockMvc.perform(post("/expense_categories")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(expenseCategoryDao);

        assertEquals("Expense type is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    public void clientErrorWillBeReturnedWhenGivenExpenseCategoryDtoWithTooShortExpenseType() throws Exception {
        newExpenseCategory.setExpenseType("RA");
        String requestBody = objectMapper.writeValueAsString(newExpenseCategory);

        MockHttpServletResponse response = mockMvc.perform(post("/expense_categories")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(expenseCategoryDao);

        assertEquals("Invalid expense type format", response.getContentAsString());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    public void clientErrorWillBeReturnedWhenGivenExpenseCategoryDtoWithInvalidFormatExpenseType() throws Exception {
        newExpenseCategory.setExpenseType("PARKING #2");
        String requestBody = objectMapper.writeValueAsString(newExpenseCategory);

        MockHttpServletResponse response = mockMvc.perform(post("/expense_categories")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(expenseCategoryDao);

        assertEquals("Invalid expense type format", response.getContentAsString());
    }

    @Test
    @WithMockUser(roles = {"ADMIN"})
    public void clientErrorWillBeReturnedWhenGivenExpenseCategoryDtoWithNotUniqueExpenseType() throws Exception {
        newExpenseCategory.setExpenseType("REFUEL");
        String requestBody = objectMapper.writeValueAsString(newExpenseCategory);

        doReturn(true).when(expenseCategoryDao).existsByExpenseType(newExpenseCategory.getExpenseType());

        MockHttpServletResponse response = mockMvc.perform(post("/expense_categories")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verify(expenseCategoryDao).existsByExpenseType(newExpenseCategory.getExpenseType());

        assertEquals("Such an expense category already exists", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void listOfExpenseCategoriesDtoWillBeReturnedWhenGivenRequestByAuthorizedUser() throws Exception {
        doReturn(allCategories).when(expenseCategoryDao).findAll();

        MockHttpServletResponse response = mockMvc.perform(get("/expense_categories"))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        ListOfExpenseCategoriesDto allCategoriesDto = objectMapper.readValue(response.getContentAsString(),
                                                                             ListOfExpenseCategoriesDto.class);

        verify(expenseCategoryDao).findAll();

        assertEquals(2, allCategoriesDto.getExpenseCategories().size());
        assertEquals("REPAIR", allCategoriesDto.getExpenseCategories().get(0).getExpenseType());
        assertEquals("REFUEL", allCategoriesDto.getExpenseCategories().get(1).getExpenseType());
    }

    @Test
    public void accessWillBeDeniedWhenGivenRequestByUnauthorizedUser() throws Exception {
        mockMvc.perform(get("/expense_categories"))
               .andExpect(status().isForbidden());

        verifyNoInteractions(expenseCategoryDao);
    }

    @Test
    @WithMockUser
    public void emptyListOfExpenseCategoriesWillBeReturnedWhenNoCategoriesInDb() throws Exception {
        allCategories.clear();

        doReturn(allCategories).when(expenseCategoryDao).findAll();

        MockHttpServletResponse response = mockMvc.perform(get("/expense_categories"))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        ListOfExpenseCategoriesDto allCategoriesDto = objectMapper.readValue(response.getContentAsString(),
                                                                             ListOfExpenseCategoriesDto.class);

        verify(expenseCategoryDao).findAll();

        assertTrue(allCategoriesDto.getExpenseCategories().isEmpty());
    }
}