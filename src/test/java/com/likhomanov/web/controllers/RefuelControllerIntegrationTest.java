package com.likhomanov.web.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.likhomanov.dao.entities.CarEntity;
import com.likhomanov.dao.entities.RefuelEntity;
import com.likhomanov.dao.repositories.CarDao;
import com.likhomanov.dao.repositories.ExpenseCategoryDao;
import com.likhomanov.dao.repositories.RefuelDao;
import com.likhomanov.exceptions.EntityNotFoundException;
import com.likhomanov.web.dto.ConsumedGasVolumeDto;
import com.likhomanov.web.dto.ExpenseCategoryDto;
import com.likhomanov.web.dto.ExpenseInfoDto;
import com.likhomanov.web.dto.RefuelDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;
import java.util.Optional;

import static com.likhomanov.enums.Currency.UAH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class RefuelControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private RefuelDao refuelDao;
    @MockBean
    private CarDao carDao;
    @MockBean
    private ExpenseCategoryDao categoryDao;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final RefuelDto newRefuel = new RefuelDto();
    private final ExpenseInfoDto expenseInfo = new ExpenseInfoDto();
    private final ExpenseCategoryDto expenseCategory = new ExpenseCategoryDto();

    @BeforeEach
    public void init() {
        expenseCategory.setExpenseType("REFUEL");

        expenseInfo.setDate(new Date());
        expenseInfo.setSum(100.0);
        expenseInfo.setCurrency(UAH);
        expenseInfo.setCompanyName("Some company");
        expenseInfo.setCompanyAddress("Some company address");
        expenseInfo.setDescription("Something happened");
        expenseInfo.setExpenseCategory(expenseCategory);

        newRefuel.setCarId(1L);
        newRefuel.setVolume(100.0);
        newRefuel.setRefuelInfo(expenseInfo);
    }

    @Test
    @WithMockUser
    public void newRefuelWillBeSavedWhenGivenRefuelDtoWithValidDataByAuthorizedUser() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        doReturn(Optional.of(2L)).when(categoryDao).findIdByExpenseType("REFUEL");
        doAnswer(invocationOnMock -> {
            RefuelEntity refuelToSave = invocationOnMock.getArgument(1);
            CarEntity carFromDb = new CarEntity();
            carFromDb.setId(1L);
            carFromDb.addRefuel(refuelToSave);
            return refuelToSave;
        }).when(carDao).attachRefuelToCarAndSave(eq(1L), any(RefuelEntity.class));

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        RefuelDto responseDto = objectMapper.readValue(response.getContentAsString(), RefuelDto.class);

        verify(categoryDao).findIdByExpenseType("REFUEL");
        verify(carDao).attachRefuelToCarAndSave(eq(1L), any(RefuelEntity.class));

        assertEquals(newRefuel.getCarId(), responseDto.getCarId());
        assertEquals(newRefuel.getVolume(), responseDto.getVolume());
        assertEquals(newRefuel.getRefuelInfo().getSum(), responseDto.getRefuelInfo().getSum());
        assertEquals(newRefuel.getRefuelInfo().getCompanyAddress(), responseDto.getRefuelInfo().getCompanyAddress());
        assertEquals(newRefuel.getRefuelInfo().getCompanyName(), responseDto.getRefuelInfo().getCompanyName());
        assertEquals(newRefuel.getRefuelInfo().getCurrency(), responseDto.getRefuelInfo().getCurrency());
        assertEquals(newRefuel.getRefuelInfo().getDate(), responseDto.getRefuelInfo().getDate());
        assertEquals(newRefuel.getRefuelInfo().getDescription(), responseDto.getRefuelInfo().getDescription());
        assertEquals(newRefuel.getRefuelInfo().getExpenseCategory().getExpenseType(),
                     responseDto.getRefuelInfo().getExpenseCategory().getExpenseType());
    }

    @Test
    public void accessWillBeDeniedWhenGivenRefuelDtoWithValidDataByUnauthorizedUser() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        mockMvc.perform(post("/refuels")
                                .contentType(APPLICATION_JSON)
                                .content(requestBody))
               .andExpect(status().isForbidden());

        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRefuelDtoWithNullVolume() throws Exception {
        newRefuel.setVolume(null);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Refuel volume is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRefuelDtoWithZeroVolume() throws Exception {
        newRefuel.setVolume(0.0);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Refuel volume is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRefuelDtoWithNullInfo() throws Exception {
        newRefuel.setRefuelInfo(null);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Refuel info is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRefuelDtoWithNullCarId() throws Exception {
        newRefuel.setCarId(null);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Car id is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRefuelDtoWithZeroCarId() throws Exception {
        newRefuel.setCarId(0L);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Car id is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRefuelDtoWithNullDate() throws Exception {
        newRefuel.getRefuelInfo().setDate(null);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Expense date is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRefuelDtoWithNullSum() throws Exception {
        newRefuel.getRefuelInfo().setSum(null);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Expense sum is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRefuelDtoWithZeroSum() throws Exception {
        newRefuel.getRefuelInfo().setSum(0.0);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Expense sum is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRefuelDtoWithNullCurrency() throws Exception {
        newRefuel.getRefuelInfo().setCurrency(null);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Currency is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRefuelDtoWithNullCompanyName() throws Exception {
        newRefuel.getRefuelInfo().setCompanyName(null);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Company name is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRefuelDtoWithBlankCompanyName() throws Exception {
        newRefuel.getRefuelInfo().setCompanyName("");
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Company name is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRefuelDtoWithNullCompanyAddress() throws Exception {
        newRefuel.getRefuelInfo().setCompanyAddress(null);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Company address is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRefuelDtoWithBlankCompanyAddress() throws Exception {
        newRefuel.getRefuelInfo().setCompanyAddress("");
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Company address is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRefuelDtoWithNullDescription() throws Exception {
        newRefuel.getRefuelInfo().setDescription(null);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Expense description is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRefuelDtoWithBlankDescription() throws Exception {
        newRefuel.getRefuelInfo().setDescription("");
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Expense description is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRefuelDtoWithNullExpenseCategory() throws Exception {
        newRefuel.getRefuelInfo().setExpenseCategory(null);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Expense category is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRefuelDtoWithNullExpenseType() throws Exception {
        newRefuel.getRefuelInfo().getExpenseCategory().setExpenseType(null);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Expense type is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRefuelDtoWithBlankExpenseType() throws Exception {
        newRefuel.getRefuelInfo().getExpenseCategory().setExpenseType("");
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Expense type is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRefuelDtoWithTooShortExpenseType() throws Exception {
        newRefuel.getRefuelInfo().getExpenseCategory().setExpenseType("RE");
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Invalid expense type format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRefuelDtoWithInvalidFormatExpenseType() throws Exception {
        newRefuel.getRefuelInfo().getExpenseCategory().setExpenseType("REFUEL #1");
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Invalid expense type format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRefuelDtoWithUnknownExpenseCategory() throws Exception {
        newRefuel.getRefuelInfo().getExpenseCategory().setExpenseType("BRIBE");
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        doReturn(Optional.empty()).when(categoryDao).findIdByExpenseType("BRIBE");

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verify(categoryDao).findIdByExpenseType("BRIBE");
        verifyNoInteractions(carDao);

        assertEquals("Expense category not found", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRefuelDtoWithUnknownCarId() throws Exception {
        newRefuel.setCarId(5L);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        doReturn(Optional.of(2L)).when(categoryDao).findIdByExpenseType("REFUEL");
        doThrow(new EntityNotFoundException("Car with id " +
                                            newRefuel.getCarId() +
                                            " does not exist")).when(carDao)
                                                               .attachRefuelToCarAndSave(eq(5L),
                                                                                         any(RefuelEntity.class));

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verify(categoryDao).findIdByExpenseType("REFUEL");
        verify(carDao).attachRefuelToCarAndSave(eq(5L), any(RefuelEntity.class));

        assertEquals("Car with id " +
                             newRefuel.getCarId() +
                             " does not exist", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void consumedGasVolumeWillBeReturnedWhenGivenValidCarIdStartDateAndEndDateByAuthorizedUser() throws Exception {
        doReturn(Optional.of(100.0)).when(refuelDao).findGuzzledGasForSomePeriodByCarId(eq(1L),
                                                                                        any(Date.class),
                                                                                        any(Date.class));

        MockHttpServletResponse response = mockMvc.perform(get("/refuels")
                                                                   .param("car_id", "1")
                                                                   .param("start_date", "2020-07-01")
                                                                   .param("end_date", "2020-07-31"))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        ConsumedGasVolumeDto consumedGas = objectMapper.readValue(response.getContentAsString(),
                                                                  ConsumedGasVolumeDto.class);

        verify(refuelDao).findGuzzledGasForSomePeriodByCarId(eq(1L), any(Date.class), any(Date.class));

        assertEquals(100.0, consumedGas.getVolume());
    }

    @Test
    public void accessWillBeDeniedWhenGivenValidCarIdStartDateAndEndDateByUnauthorizedUser() throws Exception {
        mockMvc.perform(get("/refuels")
                                .param("car_id", "1")
                                .param("start_date", "2020-07-01")
                                .param("end_date", "2020-07-31"))
               .andExpect(status().isForbidden());

        verifyNoInteractions(refuelDao);
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenEmptyCarId() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/refuels")
                                                                   .param("car_id", "")
                                                                   .param("start_date", "2020-07-01")
                                                                   .param("end_date", "2020-07-31"))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(refuelDao);

        assertEquals("Car id is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenZeroCarId() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/refuels")
                                                                   .param("car_id", "0")
                                                                   .param("start_date", "2020-07-01")
                                                                   .param("end_date", "2020-07-31"))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(refuelDao);

        assertEquals("Car id is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRequestWithoutCarIdParam() throws Exception {
        mockMvc.perform(get("/refuels")
                                .param("start_date", "2020-07-01")
                                .param("end_date", "2020-07-31"))
               .andExpect(status().isBadRequest());

        verifyNoInteractions(refuelDao);
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenEmptyStartDate() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/refuels")
                                                                   .param("car_id", "1")
                                                                   .param("start_date", "")
                                                                   .param("end_date", "2020-07-31"))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(refuelDao);

        assertEquals("Date is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenInvalidFormatStartDate() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/refuels")
                                                                   .param("car_id", "1")
                                                                   .param("start_date", "01-07-2020")
                                                                   .param("end_date", "2020-07-31"))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(refuelDao);

        assertEquals("Invalid date format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRequestWithoutStartDateParam() throws Exception {
        mockMvc.perform(get("/refuels")
                                .param("car_id", "1")
                                .param("end_date", "2020-07-31"))
               .andExpect(status().isBadRequest());

        verifyNoInteractions(refuelDao);
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenEmptyEndDate() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/refuels")
                                                                   .param("car_id", "1")
                                                                   .param("start_date", "2020-07-01")
                                                                   .param("end_date", ""))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(refuelDao);

        assertEquals("Date is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenInvalidFormatEndDate() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/refuels")
                                                                   .param("car_id", "1")
                                                                   .param("start_date", "2020-07-01")
                                                                   .param("end_date", "31-07-2020"))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(refuelDao);

        assertEquals("Invalid date format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRequestWithoutEndDateParam() throws Exception {
        mockMvc.perform(get("/refuels")
                                .param("car_id", "1")
                                .param("start_date", "2020-07-31"))
               .andExpect(status().isBadRequest());

        verifyNoInteractions(refuelDao);
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenUnknownCarIdOrDate() throws Exception {
        doReturn(Optional.empty()).when(refuelDao).findGuzzledGasForSomePeriodByCarId(eq(5L),
                                                                                      any(Date.class),
                                                                                      any(Date.class));

        MockHttpServletResponse response = mockMvc.perform(get("/refuels")
                                                                   .param("car_id", "5")
                                                                   .param("start_date", "2000-07-01")
                                                                   .param("end_date", "2000-07-31"))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verify(refuelDao).findGuzzledGasForSomePeriodByCarId(eq(5L), any(Date.class), any(Date.class));

        assertEquals("No data for specified car id or period was found", response.getContentAsString());
    }
}