package com.likhomanov.web.controllers;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.likhomanov.dao.entities.CarEntity;
import com.likhomanov.dao.entities.ReplacedPartEntity;
import com.likhomanov.dao.queryresults.MoneyByCurrencyResult;
import com.likhomanov.dao.repositories.CarDao;
import com.likhomanov.dao.repositories.ExpenseCategoryDao;
import com.likhomanov.dao.repositories.ReplacedPartDao;
import com.likhomanov.enums.Currency;
import com.likhomanov.exceptions.EntityNotFoundException;
import com.likhomanov.exceptions.HttpRequestException;
import com.likhomanov.model.CurrencyExchangeRate;
import com.likhomanov.model.ReplacedPart;
import com.likhomanov.services.CurrencyExchangeRateFetcher;
import com.likhomanov.web.dto.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static com.likhomanov.enums.Currency.*;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ReplacedPartControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ReplacedPartDao replacedPartDao;
    @MockBean
    private ExpenseCategoryDao categoryDao;
    @MockBean
    private CarDao carDao;
    @MockBean
    private CurrencyExchangeRateFetcher exchangeRateFetcher;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final NewReplacedPartDto newReplacedPart = new NewReplacedPartDto();
    private final ExpenseInfoDto expenseInfo = new ExpenseInfoDto();
    private final ExpenseCategoryDto expenseCategory = new ExpenseCategoryDto();
    private final CurrencyExchangeRate rateUSD = new CurrencyExchangeRate();
    private final CurrencyExchangeRate rateEUR = new CurrencyExchangeRate();
    private final CurrencyExchangeRate rateRUB = new CurrencyExchangeRate();
    private final Map<Currency, CurrencyExchangeRate> exchangeRates = new HashMap<>();
    private final ReplacedPart replacedPart = new ReplacedPartEntity();
    private MoneyByCurrencyResult moneyResult;

    @BeforeEach
    public void init() {
        expenseCategory.setExpenseType("REPAIR");

        expenseInfo.setDate(new Date());
        expenseInfo.setSum(100.0);
        expenseInfo.setCurrency(UAH);
        expenseInfo.setCompanyName("Some company");
        expenseInfo.setCompanyAddress("Some company address");
        expenseInfo.setDescription("Something happened");
        expenseInfo.setExpenseCategory(expenseCategory);

        newReplacedPart.setCarId(1L);
        newReplacedPart.setSerialNumber("WT123456");
        newReplacedPart.setName("Some spare part");
        newReplacedPart.setReplacedPartInfo(expenseInfo);

        rateUSD.setCurrency(USD);
        rateUSD.setBaseCurrency(UAH);
        rateUSD.setBuyRate(27.0);
        rateUSD.setSaleRate(28.0);

        rateEUR.setCurrency(EUR);
        rateEUR.setBaseCurrency(UAH);
        rateEUR.setBuyRate(32.0);
        rateEUR.setSaleRate(33.0);

        rateRUB.setCurrency(RUB);
        rateRUB.setBaseCurrency(UAH);
        rateRUB.setBuyRate(0.36);
        rateRUB.setSaleRate(0.38);

        if (exchangeRates.isEmpty()) {
            exchangeRates.put(USD, rateUSD);
            exchangeRates.put(EUR, rateEUR);
            exchangeRates.put(RUB, rateRUB);
        }

        if (moneyResult == null) {
            moneyResult = new MoneyByCurrencyResult() {
                @Override
                public Currency getCurrency() {
                    return UAH;
                }

                @Override
                public Double getDough() {
                    return 100.0;
                }
            };
        }
    }

    @Test
    @WithMockUser
    public void newReplacedPartWillBeSavedWhenGivenNewReplacedPartDtoWithValidDataByAuthorizedUser() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        doReturn(false).when(replacedPartDao).existsBySerialNumber(newReplacedPart.getSerialNumber());
        doReturn(Optional.of(1L)).when(categoryDao).findIdByExpenseType("REPAIR");
        doAnswer(invocationOnMock -> {
            ReplacedPartEntity partToSave = invocationOnMock.getArgument(1);
            CarEntity carFromDb = new CarEntity();
            carFromDb.setId(1L);
            carFromDb.addReplacedPart(partToSave);
            return partToSave;
        }).when(carDao).attachReplacedPartToCarAndSave(eq(1L), any(ReplacedPartEntity.class));

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();

        verify(replacedPartDao).existsBySerialNumber(newReplacedPart.getSerialNumber());
        verify(categoryDao).findIdByExpenseType("REPAIR");
        verify(carDao).attachReplacedPartToCarAndSave(eq(1L), any(ReplacedPartEntity.class));

        NewReplacedPartDto savedReplacedPart = objectMapper.readValue(response.getContentAsString(),
                                                                      NewReplacedPartDto.class);

        assertEquals(newReplacedPart.getCarId(),
                     savedReplacedPart.getCarId());
        assertEquals(newReplacedPart.getName(),
                     savedReplacedPart.getName());
        assertEquals(newReplacedPart.getSerialNumber(),
                     savedReplacedPart.getSerialNumber());
        assertEquals(newReplacedPart.getReplacedPartInfo().getSum(),
                     savedReplacedPart.getReplacedPartInfo().getSum());
        assertEquals(newReplacedPart.getReplacedPartInfo().getCompanyAddress(),
                     savedReplacedPart.getReplacedPartInfo().getCompanyAddress());
        assertEquals(newReplacedPart.getReplacedPartInfo().getCompanyName(),
                     savedReplacedPart.getReplacedPartInfo().getCompanyName());
        assertEquals(newReplacedPart.getReplacedPartInfo().getCurrency(),
                     savedReplacedPart.getReplacedPartInfo().getCurrency());
        assertEquals(newReplacedPart.getReplacedPartInfo().getDate(),
                     savedReplacedPart.getReplacedPartInfo().getDate());
        assertEquals(newReplacedPart.getReplacedPartInfo().getDescription(),
                     savedReplacedPart.getReplacedPartInfo().getDescription());
        assertEquals(newReplacedPart.getReplacedPartInfo().getExpenseCategory().getExpenseType(),
                     savedReplacedPart.getReplacedPartInfo().getExpenseCategory().getExpenseType());
    }

    @Test
    public void accessWillBeDeniedWhenGivenNewReplacedPartDtoByUnauthorizedUser() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        mockMvc.perform(post("/replaced_parts")
                                .contentType(APPLICATION_JSON)
                                .content(requestBody))
               .andExpect(status().isForbidden());

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithNullSerialNumber() throws Exception {
        newReplacedPart.setSerialNumber(null);
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Replaced part's serial number is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithBlankSerialNumber() throws Exception {
        newReplacedPart.setSerialNumber("");
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Replaced part's serial number is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithTooShortSerialNumber() throws Exception {
        newReplacedPart.setSerialNumber("123");
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Invalid serial number format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithInvalidFormatSerialNumber() throws Exception {
        newReplacedPart.setSerialNumber("#JT 45632");
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Invalid serial number format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithNullName() throws Exception {
        newReplacedPart.setName(null);
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Replaced part's name is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithBlankName() throws Exception {
        newReplacedPart.setName("");
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Replaced part's name is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithNullInfo() throws Exception {
        newReplacedPart.setReplacedPartInfo(null);
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Replaced part's info is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithNullCarId() throws Exception {
        newReplacedPart.setCarId(null);
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Car id is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithZeroCarId() throws Exception {
        newReplacedPart.setCarId(0L);
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Car id is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithNullDate() throws Exception {
        newReplacedPart.getReplacedPartInfo().setDate(null);
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Expense date is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithNullSum() throws Exception {
        newReplacedPart.getReplacedPartInfo().setSum(null);
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Expense sum is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithZeroSum() throws Exception {
        newReplacedPart.getReplacedPartInfo().setSum(0.0);
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Expense sum is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithNullCurrency() throws Exception {
        newReplacedPart.getReplacedPartInfo().setCurrency(null);
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Currency is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithNullCompanyName() throws Exception {
        newReplacedPart.getReplacedPartInfo().setCompanyName(null);
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Company name is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithBlankCompanyName() throws Exception {
        newReplacedPart.getReplacedPartInfo().setCompanyName("");
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Company name is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithNullCompanyAddress() throws Exception {
        newReplacedPart.getReplacedPartInfo().setCompanyAddress(null);
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Company address is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithBlankCompanyAddress() throws Exception {
        newReplacedPart.getReplacedPartInfo().setCompanyAddress("");
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Company address is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithNullDescription() throws Exception {
        newReplacedPart.getReplacedPartInfo().setDescription(null);
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Expense description is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithBlankDescription() throws Exception {
        newReplacedPart.getReplacedPartInfo().setDescription("");
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Expense description is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithNullExpenseCategory() throws Exception {
        newReplacedPart.getReplacedPartInfo().setExpenseCategory(null);
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Expense category is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithNullExpenseType() throws Exception {
        newReplacedPart.getReplacedPartInfo().getExpenseCategory().setExpenseType(null);
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Expense type is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithBlankExpenseType() throws Exception {
        newReplacedPart.getReplacedPartInfo().getExpenseCategory().setExpenseType("");
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Expense type is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithTooShortExpenseType() throws Exception {
        newReplacedPart.getReplacedPartInfo().getExpenseCategory().setExpenseType("RP");
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Invalid expense type format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithInvalidFormatExpenseType() throws Exception {
        newReplacedPart.getReplacedPartInfo().getExpenseCategory().setExpenseType("FIRST REPAIR");
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Invalid expense type format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithUnknownExpenseCategory() throws Exception {
        newReplacedPart.getReplacedPartInfo().getExpenseCategory().setExpenseType("BRIBE");
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        doReturn(false).when(replacedPartDao).existsBySerialNumber(newReplacedPart.getSerialNumber());
        doReturn(Optional.empty()).when(categoryDao).findIdByExpenseType("BRIBE");

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verify(categoryDao).findIdByExpenseType("BRIBE");
        verify(replacedPartDao).existsBySerialNumber(newReplacedPart.getSerialNumber());
        verifyNoInteractions(carDao);

        assertEquals("Expense category not found", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNewReplacedPartDtoWithUnknownCarId() throws Exception {
        newReplacedPart.setCarId(5L);
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        doReturn(false).when(replacedPartDao).existsBySerialNumber(newReplacedPart.getSerialNumber());
        doReturn(Optional.of(1L)).when(categoryDao).findIdByExpenseType("REPAIR");
        doThrow(new EntityNotFoundException("Car with id " +
                                            newReplacedPart.getCarId() +
                                            " does not exist")).when(carDao)
                                                               .attachReplacedPartToCarAndSave(eq(5L),
                                                                                               any(ReplacedPartEntity.class));

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verify(replacedPartDao).existsBySerialNumber(newReplacedPart.getSerialNumber());
        verify(categoryDao).findIdByExpenseType("REPAIR");
        verify(carDao).attachReplacedPartToCarAndSave(eq(5L), any(ReplacedPartEntity.class));

        assertEquals("Car with id " +
                             newReplacedPart.getCarId() +
                             " does not exist", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void notUniqueEntityExceptionWillBeThrownWhenGivenNewReplacedPartDtoWithNotUniqueSerialNumber() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newReplacedPart);

        doReturn(true).when(replacedPartDao).existsBySerialNumber(newReplacedPart.getSerialNumber());

        MockHttpServletResponse response = mockMvc.perform(post("/replaced_parts")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verify(replacedPartDao).existsBySerialNumber(newReplacedPart.getSerialNumber());
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals("Replaced part with such a serial number already exists", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void spentMoneyInUahWillBeReturnedWhenGivenValidCarIdYearAndCurrencyByAuthorizedUser() throws Exception {
        doReturn(singletonList(moneyResult)).when(replacedPartDao)
                                            .findDoughSpentOnSparePartsForSomePeriodByCarId(eq(1L),
                                                                                            any(Date.class),
                                                                                            any(Date.class));
        doReturn(exchangeRates).when(exchangeRateFetcher).obtainCurrencyExchangeRates();

        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/spent_money")
                                                                   .param("car_id", "1")
                                                                   .param("year", "2020")
                                                                   .param("currency", "UAH"))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        SpentMoneyDto spentMoneyDto = objectMapper.readValue(response.getContentAsString(), SpentMoneyDto.class);

        verify(replacedPartDao).findDoughSpentOnSparePartsForSomePeriodByCarId(eq(1L),
                                                                               any(Date.class),
                                                                               any(Date.class));
        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();

        assertEquals(100.0, spentMoneyDto.getSpentMoney());
        assertEquals(UAH, spentMoneyDto.getCurrency());
    }

    @Test
    @WithMockUser
    public void spentMoneyInUsdWillBeReturnedWhenGivenValidCarIdYearAndCurrencyByAuthorizedUser() throws Exception {
        doReturn(singletonList(moneyResult)).when(replacedPartDao)
                                            .findDoughSpentOnSparePartsForSomePeriodByCarId(eq(1L),
                                                                                            any(Date.class),
                                                                                            any(Date.class));
        doReturn(exchangeRates).when(exchangeRateFetcher).obtainCurrencyExchangeRates();

        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/spent_money")
                                                                   .param("car_id", "1")
                                                                   .param("year", "2020")
                                                                   .param("currency", "USD"))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        SpentMoneyDto spentMoneyDto = objectMapper.readValue(response.getContentAsString(), SpentMoneyDto.class);

        verify(replacedPartDao).findDoughSpentOnSparePartsForSomePeriodByCarId(eq(1L),
                                                                               any(Date.class),
                                                                               any(Date.class));
        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();

        assertEquals(3.57, spentMoneyDto.getSpentMoney());
        assertEquals(USD, spentMoneyDto.getCurrency());
    }

    @Test
    @WithMockUser
    public void spentMoneyInEurWillBeReturnedWhenGivenValidCarIdYearAndCurrencyByAuthorizedUser() throws Exception {
        doReturn(singletonList(moneyResult)).when(replacedPartDao)
                                            .findDoughSpentOnSparePartsForSomePeriodByCarId(eq(1L),
                                                                                            any(Date.class),
                                                                                            any(Date.class));
        doReturn(exchangeRates).when(exchangeRateFetcher).obtainCurrencyExchangeRates();

        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/spent_money")
                                                                   .param("car_id", "1")
                                                                   .param("year", "2020")
                                                                   .param("currency", "EUR"))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        SpentMoneyDto spentMoneyDto = objectMapper.readValue(response.getContentAsString(), SpentMoneyDto.class);

        verify(replacedPartDao).findDoughSpentOnSparePartsForSomePeriodByCarId(eq(1L),
                                                                               any(Date.class),
                                                                               any(Date.class));
        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();

        assertEquals(3.03, spentMoneyDto.getSpentMoney());
        assertEquals(EUR, spentMoneyDto.getCurrency());
    }

    @Test
    @WithMockUser
    public void spentMoneyInRubWillBeReturnedWhenGivenValidCarIdYearAndCurrencyByAuthorizedUser() throws Exception {
        doReturn(singletonList(moneyResult)).when(replacedPartDao)
                                            .findDoughSpentOnSparePartsForSomePeriodByCarId(eq(1L),
                                                                                            any(Date.class),
                                                                                            any(Date.class));
        doReturn(exchangeRates).when(exchangeRateFetcher).obtainCurrencyExchangeRates();

        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/spent_money")
                                                                   .param("car_id", "1")
                                                                   .param("year", "2020")
                                                                   .param("currency", "RUB"))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        SpentMoneyDto spentMoneyDto = objectMapper.readValue(response.getContentAsString(), SpentMoneyDto.class);

        verify(replacedPartDao).findDoughSpentOnSparePartsForSomePeriodByCarId(eq(1L),
                                                                               any(Date.class),
                                                                               any(Date.class));
        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();

        assertEquals(263.16, spentMoneyDto.getSpentMoney());
        assertEquals(RUB, spentMoneyDto.getCurrency());
    }

    @Test
    public void accessWillBeDeniedWhenGivenValidCarIdYearAndCurrencyByUnauthorizedUser() throws Exception {
        mockMvc.perform(get("/replaced_parts/spent_money")
                                .param("car_id", "1")
                                .param("year", "2020")
                                .param("currency", "UAH"))
               .andExpect(status().isForbidden());

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(exchangeRateFetcher);
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenBlankCarId() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/spent_money")
                                                                   .param("car_id", "")
                                                                   .param("year", "2020")
                                                                   .param("currency", "UAH"))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(exchangeRateFetcher);

        assertEquals("Car id is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenYearCurrencyAndZeroCarId() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/spent_money")
                                                                   .param("car_id", "0")
                                                                   .param("year", "2020")
                                                                   .param("currency", "UAH"))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(exchangeRateFetcher);

        assertEquals("Car id is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRequestWithoutCarIdParam() throws Exception {
        mockMvc.perform(get("/replaced_parts/spent_money")
                                .param("year", "2020")
                                .param("currency", "UAH"))
               .andExpect(status().isBadRequest());

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(exchangeRateFetcher);
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRequestWithoutYearParam() throws Exception {
        mockMvc.perform(get("/replaced_parts/spent_money")
                                .param("car_id", "1")
                                .param("currency", "UAH"))
               .andExpect(status().isBadRequest());

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(exchangeRateFetcher);
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenTooOldYear() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/spent_money")
                                                                   .param("car_id", "1")
                                                                   .param("year", "1899")
                                                                   .param("currency", "UAH"))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(exchangeRateFetcher);

        assertEquals("Year is out of range", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenYearFromTooFarFuture() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/spent_money")
                                                                   .param("car_id", "1")
                                                                   .param("year", "2101")
                                                                   .param("currency", "UAH"))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(exchangeRateFetcher);

        assertEquals("Year is out of range", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenBlankCurrency() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/spent_money")
                                                                   .param("car_id", "1")
                                                                   .param("year", "2020")
                                                                   .param("currency", ""))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(exchangeRateFetcher);

        assertEquals("Currency is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenUnknownCurrency() throws Exception {
        mockMvc.perform(get("/replaced_parts/spent_money")
                                .param("car_id", "1")
                                .param("year", "2020")
                                .param("currency", "AUD"))
               .andExpect(status().isBadRequest());

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(exchangeRateFetcher);
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRequestWithoutCurrencyParam() throws Exception {
        mockMvc.perform(get("/replaced_parts/spent_money")
                                .param("car_id", "1")
                                .param("year", "2020"))
               .andExpect(status().isBadRequest());

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(exchangeRateFetcher);
    }

    @Test
    @WithMockUser
    public void serviceUnavailableWillBeReturnedWhenCurrencyExchangeRateSourceCannotBeReached() throws Exception {
        doReturn(singletonList(moneyResult)).when(replacedPartDao)
                                            .findDoughSpentOnSparePartsForSomePeriodByCarId(eq(1L),
                                                                                            any(Date.class),
                                                                                            any(Date.class));
        doThrow(new HttpRequestException("Currency exchange rate source not available")).when(exchangeRateFetcher)
                                                                                        .obtainCurrencyExchangeRates();

        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/spent_money")
                                                                   .param("car_id", "1")
                                                                   .param("year", "2020")
                                                                   .param("currency", "UAH"))
                                                  .andExpect(status().isServiceUnavailable())
                                                  .andReturn()
                                                  .getResponse();

        verify(replacedPartDao).findDoughSpentOnSparePartsForSomePeriodByCarId(eq(1L),
                                                                               any(Date.class),
                                                                               any(Date.class));
        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();

        assertEquals("Currency exchange rate source not available", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void emptySpentMoneyDtoWillBeReturnedWhenGivenUnknownCarId() throws Exception {
        doReturn(new ArrayList<>()).when(replacedPartDao)
                                   .findDoughSpentOnSparePartsForSomePeriodByCarId(eq(5L),
                                                                                   any(Date.class),
                                                                                   any(Date.class));
        doReturn(exchangeRates).when(exchangeRateFetcher).obtainCurrencyExchangeRates();

        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/spent_money")
                                                                   .param("car_id", "5")
                                                                   .param("year", "2020")
                                                                   .param("currency", "UAH"))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        SpentMoneyDto spentMoneyDto = objectMapper.readValue(response.getContentAsString(), SpentMoneyDto.class);

        verify(replacedPartDao).findDoughSpentOnSparePartsForSomePeriodByCarId(eq(5L),
                                                                               any(Date.class),
                                                                               any(Date.class));
        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();

        assertEquals(0.0, spentMoneyDto.getSpentMoney());
        assertEquals(UAH, spentMoneyDto.getCurrency());
    }

    @Test
    @WithMockUser
    public void emptySpentMoneyDtoWillBeReturnedWhenGivenUnknownYear() throws Exception {
        doReturn(new ArrayList<>()).when(replacedPartDao)
                                   .findDoughSpentOnSparePartsForSomePeriodByCarId(eq(1L),
                                                                                   any(Date.class),
                                                                                   any(Date.class));
        doReturn(exchangeRates).when(exchangeRateFetcher).obtainCurrencyExchangeRates();

        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/spent_money")
                                                                   .param("car_id", "1")
                                                                   .param("year", "1984")
                                                                   .param("currency", "UAH"))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        SpentMoneyDto spentMoneyDto = objectMapper.readValue(response.getContentAsString(), SpentMoneyDto.class);

        verify(replacedPartDao).findDoughSpentOnSparePartsForSomePeriodByCarId(eq(1L),
                                                                               any(Date.class),
                                                                               any(Date.class));
        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();

        assertEquals(0.0, spentMoneyDto.getSpentMoney());
        assertEquals(UAH, spentMoneyDto.getCurrency());
    }

    @Test
    @WithMockUser
    public void spentMoneyInUahWillBeReturnedWhenGivenValidCarIdAndCurrencyByAuthorizedUser() throws Exception {
        doReturn(singletonList(moneyResult)).when(replacedPartDao).findDoughSpentOnSparePartsByCarId(1L);
        doReturn(exchangeRates).when(exchangeRateFetcher).obtainCurrencyExchangeRates();

        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/spent_money")
                                                                   .param("car_id", "1")
                                                                   .param("year", "")
                                                                   .param("currency", "UAH"))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        SpentMoneyDto spentMoneyDto = objectMapper.readValue(response.getContentAsString(), SpentMoneyDto.class);

        verify(replacedPartDao).findDoughSpentOnSparePartsByCarId(1L);
        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();

        assertEquals(100.0, spentMoneyDto.getSpentMoney());
        assertEquals(UAH, spentMoneyDto.getCurrency());
    }

    @Test
    @WithMockUser
    public void spentMoneyInUsdWillBeReturnedWhenGivenValidCarIdAndCurrencyByAuthorizedUser() throws Exception {
        doReturn(singletonList(moneyResult)).when(replacedPartDao).findDoughSpentOnSparePartsByCarId(1L);
        doReturn(exchangeRates).when(exchangeRateFetcher).obtainCurrencyExchangeRates();

        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/spent_money")
                                                                   .param("car_id", "1")
                                                                   .param("year", "")
                                                                   .param("currency", "USD"))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        SpentMoneyDto spentMoneyDto = objectMapper.readValue(response.getContentAsString(), SpentMoneyDto.class);

        verify(replacedPartDao).findDoughSpentOnSparePartsByCarId(1L);
        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();

        assertEquals(3.57, spentMoneyDto.getSpentMoney());
        assertEquals(USD, spentMoneyDto.getCurrency());
    }

    @Test
    @WithMockUser
    public void spentMoneyInEurWillBeReturnedWhenGivenValidCarIdAndCurrencyByAuthorizedUser() throws Exception {
        doReturn(singletonList(moneyResult)).when(replacedPartDao).findDoughSpentOnSparePartsByCarId(1L);
        doReturn(exchangeRates).when(exchangeRateFetcher).obtainCurrencyExchangeRates();

        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/spent_money")
                                                                   .param("car_id", "1")
                                                                   .param("year", "")
                                                                   .param("currency", "EUR"))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        SpentMoneyDto spentMoneyDto = objectMapper.readValue(response.getContentAsString(), SpentMoneyDto.class);

        verify(replacedPartDao).findDoughSpentOnSparePartsByCarId(1L);
        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();

        assertEquals(3.03, spentMoneyDto.getSpentMoney());
        assertEquals(EUR, spentMoneyDto.getCurrency());
    }

    @Test
    @WithMockUser
    public void spentMoneyInRubWillBeReturnedWhenGivenValidCarIdAndCurrencyByAuthorizedUser() throws Exception {
        doReturn(singletonList(moneyResult)).when(replacedPartDao).findDoughSpentOnSparePartsByCarId(1L);
        doReturn(exchangeRates).when(exchangeRateFetcher).obtainCurrencyExchangeRates();

        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/spent_money")
                                                                   .param("car_id", "1")
                                                                   .param("year", "")
                                                                   .param("currency", "RUB"))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        SpentMoneyDto spentMoneyDto = objectMapper.readValue(response.getContentAsString(), SpentMoneyDto.class);

        verify(replacedPartDao).findDoughSpentOnSparePartsByCarId(1L);
        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();

        assertEquals(263.16, spentMoneyDto.getSpentMoney());
        assertEquals(RUB, spentMoneyDto.getCurrency());
    }

    @Test
    public void accessWillBeDeniedWhenGivenValidCarIdAndCurrencyByUnauthorizedUser() throws Exception {
        mockMvc.perform(get("/replaced_parts/spent_money")
                                .param("car_id", "1")
                                .param("year", "")
                                .param("currency", "UAH"))
               .andExpect(status().isForbidden());

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(exchangeRateFetcher);
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCurrencyAndBlankCarId() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/spent_money")
                                                                   .param("car_id", "")
                                                                   .param("year", "")
                                                                   .param("currency", "UAH"))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(exchangeRateFetcher);

        assertEquals("Car id is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCurrencyAndZeroCarId() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/spent_money")
                                                                   .param("car_id", "0")
                                                                   .param("year", "")
                                                                   .param("currency", "UAH"))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(exchangeRateFetcher);

        assertEquals("Car id is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRequestWithCurrencyAndWithoutCarIdParam() throws Exception {
        mockMvc.perform(get("/replaced_parts/spent_money")
                                .param("year", "")
                                .param("currency", "UAH"))
               .andExpect(status().isBadRequest());

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(exchangeRateFetcher);
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarIdAndBlankCurrency() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/spent_money")
                                                                   .param("car_id", "1")
                                                                   .param("year", "")
                                                                   .param("currency", ""))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(exchangeRateFetcher);

        assertEquals("Currency is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarIdAndUnknownCurrency() throws Exception {
        mockMvc.perform(get("/replaced_parts/spent_money")
                                .param("car_id", "1")
                                .param("year", "")
                                .param("currency", "AUD"))
               .andExpect(status().isBadRequest());

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(exchangeRateFetcher);
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRequestWithCarIdAndWithoutCurrencyParam() throws Exception {
        mockMvc.perform(get("/replaced_parts/spent_money")
                                .param("car_id", "1")
                                .param("year", ""))
               .andExpect(status().isBadRequest());

        verifyNoInteractions(replacedPartDao);
        verifyNoInteractions(exchangeRateFetcher);
    }

    @Test
    @WithMockUser
    public void serviceUnavailableWillBeReturnedWhenExchangeRateSourceCannotBeReached() throws Exception {
        doReturn(singletonList(moneyResult)).when(replacedPartDao).findDoughSpentOnSparePartsByCarId(1L);
        doThrow(new HttpRequestException("Currency exchange rate source not available")).when(exchangeRateFetcher)
                                                                                        .obtainCurrencyExchangeRates();

        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/spent_money")
                                                                   .param("car_id", "1")
                                                                   .param("year", "")
                                                                   .param("currency", "UAH"))
                                                  .andExpect(status().isServiceUnavailable())
                                                  .andReturn()
                                                  .getResponse();

        verify(replacedPartDao).findDoughSpentOnSparePartsByCarId(1L);
        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();

        assertEquals("Currency exchange rate source not available", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void emptySpentMoneyDtoWillBeReturnedWhenGivenCurrencyAndUnknownCarId() throws Exception {
        doReturn(new ArrayList<>()).when(replacedPartDao).findDoughSpentOnSparePartsByCarId(5L);
        doReturn(exchangeRates).when(exchangeRateFetcher).obtainCurrencyExchangeRates();

        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/spent_money")
                                                                   .param("car_id", "5")
                                                                   .param("year", "")
                                                                   .param("currency", "UAH"))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        SpentMoneyDto spentMoneyDto = objectMapper.readValue(response.getContentAsString(), SpentMoneyDto.class);

        verify(replacedPartDao).findDoughSpentOnSparePartsByCarId(5L);
        verify(exchangeRateFetcher).obtainCurrencyExchangeRates();

        assertEquals(0.0, spentMoneyDto.getSpentMoney());
        assertEquals(UAH, spentMoneyDto.getCurrency());
    }

    @Test
    @WithMockUser
    public void pageOfReplacedPartsWillBeReturnedWhenGivenValidCarIdAndPageParametersByAuthorizedUser() throws Exception {
        doAnswer(invocationOnMock -> {
            Pageable pageable = invocationOnMock.getArgument(1);
            return new PageImpl<>(Collections.singletonList(replacedPart), pageable, 1);
        }).when(replacedPartDao).findByCarId(eq(1L), any(Pageable.class));

        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/1")
                                                                   .param("page", "0")
                                                                   .param("size", "4")
                                                                   .param("sort", ""))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        Page<ReplacedPartDto> partDtos = objectMapper.readValue(response.getContentAsString(),
                                                                new TypeReference<TestPageImpl<ReplacedPartDto>>() {});

        verify(replacedPartDao).findByCarId(eq(1L), any(Pageable.class));
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals(1, partDtos.getTotalElements());
        assertEquals(1, partDtos.getContent().size());
        assertEquals("UNSORTED", partDtos.getSort().toString());
        assertEquals(0, partDtos.getPageable().getPageNumber());
        assertEquals(4, partDtos.getPageable().getPageSize());
    }

    @Test
    @WithMockUser
    public void emptyPageOfReplacedPartsWillBeReturnedWhenGivenUnknownCarId() throws Exception {
        doAnswer(invocationOnMock -> {
            Pageable pageable = invocationOnMock.getArgument(1);
            return new PageImpl<>(Collections.emptyList(), pageable, 0);
        }).when(replacedPartDao).findByCarId(eq(5L), any(Pageable.class));

        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/5")
                                                                   .param("page", "0")
                                                                   .param("size", "4")
                                                                   .param("sort", ""))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        Page<ReplacedPartDto> partDtos = objectMapper.readValue(response.getContentAsString(),
                                                                new TypeReference<TestPageImpl<ReplacedPartDto>>() {});

        verify(replacedPartDao).findByCarId(eq(5L), any(Pageable.class));
        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);

        assertEquals(0, partDtos.getTotalElements());
        assertEquals(0, partDtos.getContent().size());
        assertEquals("UNSORTED", partDtos.getSort().toString());
        assertEquals(0, partDtos.getPageable().getPageNumber());
        assertEquals(4, partDtos.getPageable().getPageSize());
    }

    @Test
    public void accessWillBeDeniedWhenGivenValidCarIdByUnauthorizedUser() throws Exception {
        mockMvc.perform(get("/replaced_parts/1")
                                .param("page", "0")
                                .param("size", "4")
                                .param("sort", ""))
               .andExpect(status().isForbidden());

        verifyNoInteractions(replacedPartDao);
    }

    @Test
    @WithMockUser
    public void methodNotAllowedWillBeReturnedWhenGivenNullCarId() throws Exception {
        mockMvc.perform(get("/replaced_parts/")
                                .param("page", "0")
                                .param("size", "4")
                                .param("sort", ""))
               .andExpect(status().isMethodNotAllowed())
               .andReturn()
               .getResponse();

        verifyNoInteractions(replacedPartDao);
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenZeroCarId() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/replaced_parts/0")
                                                                   .param("page", "0")
                                                                   .param("size", "4")
                                                                   .param("sort", ""))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(replacedPartDao);

        assertEquals("Car id is missing", response.getContentAsString());
    }

    private static final class TestPageImpl<T> extends PageImpl<T> {

        private static final long serialVersionUID = 3248189030448292002L;

        @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
        public TestPageImpl(@JsonProperty("content") List<T> content,
                            @JsonProperty("number") int number,
                            @JsonProperty("size") int size,
                            @JsonProperty("totalElements") Long totalElements,
                            @JsonProperty("pageable") JsonNode pageable,
                            @JsonProperty("last") boolean last,
                            @JsonProperty("totalPages") int totalPages,
                            @JsonProperty("sort") JsonNode sort,
                            @JsonProperty("first") boolean first,
                            @JsonProperty("numberOfElements") int numberOfElements,
                            @JsonProperty("empty") boolean empty) {
            super(content, PageRequest.of(number, size), totalElements);
        }

        public TestPageImpl(List<T> content, Pageable pageable, long total) {
            super(content, pageable, total);
        }

        public TestPageImpl(List<T> content) {
            super(content);
        }

        public TestPageImpl() {
            super(new ArrayList<T>());
        }
    }
}