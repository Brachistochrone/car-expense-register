package com.likhomanov.web.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.likhomanov.dao.entities.UserEntity;
import com.likhomanov.dao.repositories.UserDao;
import com.likhomanov.dao.repositories.UserRoleDao;
import com.likhomanov.enums.Role;
import com.likhomanov.web.dto.NewUserDto;
import com.likhomanov.web.dto.UserDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UserDao userDao;
    @MockBean
    private UserRoleDao roleDao;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final NewUserDto newUser = new NewUserDto();

    @BeforeEach
    public void init() {
        newUser.setEmail("some_email@gmail.com");
        newUser.setPassword("qwerty");
    }

    @Test
    public void newUserWillBeCreatedAndUserDtoWillBeReturnedWhenGivenNewUserCredentials() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newUser);

        doReturn(Optional.of(1L)).when(roleDao).findRoleIdByRoleName(any(Role.class));
        doReturn(false).when(userDao).existsByEmail(newUser.getEmail());
        doAnswer(invocationOnMock -> {
            UserEntity userEntity = invocationOnMock.getArgument(0);
            userEntity.setId(1L);
            return userEntity;
        }).when(userDao).saveUserWithRole(any(UserEntity.class));

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        UserDto returnedUser = objectMapper.readValue(response.getContentAsString(), UserDto.class);

        verify(roleDao).findRoleIdByRoleName(any(Role.class));
        verify(userDao).existsByEmail(newUser.getEmail());
        verify(userDao).saveUserWithRole(any(UserEntity.class));

        assertEquals(1L, returnedUser.getId());
        assertEquals("some_email@gmail.com", returnedUser.getEmail());
    }

    @Test
    public void clientErrorWillBeReturnedWhenGivenNewUserWithNullEmail() throws Exception {
        newUser.setEmail(null);
        String requestBody = objectMapper.writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(roleDao);
        verifyNoInteractions(userDao);

        assertEquals("Email is missing", response.getContentAsString());
    }

    @Test
    public void clientErrorWillBeReturnedWhenGivenNewUserWithBlankEmail() throws Exception {
        newUser.setEmail("");
        String requestBody = objectMapper.writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(roleDao);
        verifyNoInteractions(userDao);

        assertEquals("Email is missing", response.getContentAsString());
    }

    @Test
    public void clientErrorWillBeReturnedWhenGivenNewUserWithInvalidFormatEmail() throws Exception {
        newUser.setEmail("some_email.com");
        String requestBody = objectMapper.writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(roleDao);
        verifyNoInteractions(userDao);

        assertEquals("Invalid email format", response.getContentAsString());
    }

    @Test
    public void clientErrorWillBeReturnedWhenGivenNewUserWithNullPassword() throws Exception {
        newUser.setPassword(null);
        String requestBody = objectMapper.writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(roleDao);
        verifyNoInteractions(userDao);

        assertEquals("Password is missing", response.getContentAsString());
    }

    @Test
    public void clientErrorWillBeReturnedWhenGivenNewUserWithBlankPassword() throws Exception {
        newUser.setPassword("");
        String requestBody = objectMapper.writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(roleDao);
        verifyNoInteractions(userDao);

        assertEquals("Password is missing", response.getContentAsString());
    }

    @Test
    public void clientErrorWillBeReturnedWhenGivenNewUserWithTooShortPassword() throws Exception {
        newUser.setPassword("qwert");
        String requestBody = objectMapper.writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(roleDao);
        verifyNoInteractions(userDao);

        assertEquals("Too short password", response.getContentAsString());
    }

    @Test
    public void internalServerErrorWillBeReturnedWhenRoleCannotBeFoundInDb() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newUser);

        doReturn(false).when(userDao).existsByEmail(newUser.getEmail());
        doReturn(Optional.empty()).when(roleDao).findRoleIdByRoleName(any(Role.class));

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isInternalServerError())
                                                  .andReturn()
                                                  .getResponse();

        verify(roleDao).findRoleIdByRoleName(any(Role.class));
        verify(userDao).existsByEmail(newUser.getEmail());
        verifyNoMoreInteractions(userDao);

        assertEquals("Role does not exist", response.getContentAsString());
    }

    @Test
    public void clientErrorWillBeReturnedWhenGivenNewUserWithNotUniqueEmail() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newUser);

        doReturn(true).when(userDao).existsByEmail(newUser.getEmail());

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(roleDao);
        verify(userDao).existsByEmail(newUser.getEmail());
        verifyNoMoreInteractions(userDao);

        assertEquals("User with such email already exists", response.getContentAsString());
    }
}